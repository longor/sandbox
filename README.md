# L-Box Sandbox #

This is a sandbox game in its prototype-phase. If you want to help, feel free to create a fork and create pull requests.
Please mind that this is a hobby-project, so there will be a lot of weird/random/insane code in the repository.

## Download ##

Here is a downloadable version. Mind that it only contains the LWJGL-natives for windows, although adding the natives for your own system is pretty easy.
Also, the file is about 18 megabyte large.
[Download](https://www.mediafire.com/?yl5j7a93j5dom0d)

## Dependencies ##

* Java 8

**External Libraries**

* [Jython](http://www.jython.org/downloads.html) 2.5.4 rc1 (standalone)
* [Artemis](http://gamadu.com/artemis/download.html)
* EventBUS 1.4 (by Bushe)
* [Gson](https://code.google.com/p/google-gson/) 2.2.2 (by Google)
* [LWJGL](http://lwjgl.org/) 2.9.0
* [JBullet](http://jbullet.advel.cz/) 2.72
* * ASM
* * vecmath
* * stack-alloc

**L19 Libraries**

* [BinaryDataTree](https://bitbucket.org/longor1996/binarydatatree)
* [UtilCore](https://bitbucket.org/longor1996/utilcore)
* [UtilIO](https://bitbucket.org/longor1996/utilio)
* [UtilMath](https://bitbucket.org/longor1996/utilmath)