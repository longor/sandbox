/*
 *
 */
package de.longor.gcore;

// TODO: Auto-generated Javadoc
/**
 * The Interface TimedLoopHandler.
 */
public interface TimedLoopHandler
{
	// These are events that could happen.
	/**
	 * On loop start.
	 */
	public default void onLoopStart(){}
	
	/**
	 * On loop stop.
	 */
	public default void onLoopStop(){}
	
	/**
	 * On loop pause.
	 */
	public default void onLoopPause(){}
	
	/**
	 * On loop unpause.
	 */
	public default void onLoopUnpause(){}
	
	// These get called as long as the loop is running, and is not paused.
	/**
	 * Tick.
	 */
	public default void tick(){}
	
	/**
	 * Second.
	 */
	public default void second(){}
	
	/**
	 * Loop.
	 *
	 * @param variableDelta the variable delta
	 */
	public default void loop(float variableDelta){}
	
	/**
	 * Frame.
	 *
	 * @param interpolation the interpolation
	 */
	public default void frame(float interpolation){}
	
	// These get called as long as the loop is running, regardless if it is paused.
	/**
	 * Inf_tick.
	 */
	public default void inf_tick(){}
	
	/**
	 * Inf_second.
	 */
	public default void inf_second(){}
	
	/**
	 * Inf_loop.
	 *
	 * @param variableDelta the variable delta
	 */
	public default void inf_loop(float variableDelta){}
	
	/**
	 * Inf_frame.
	 *
	 * @param interpolation the interpolation
	 */
	public default void inf_frame(float interpolation){}
	
	/**
	 * Frame start.
	 */
	public default void frameStart(){}
	
	/**
	 * Frame stop.
	 */
	public default void frameStop(){}
	
}
