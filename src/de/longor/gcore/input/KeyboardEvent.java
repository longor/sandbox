/*
 * 
 */
package de.longor.gcore.input;

import de.longor.eventor.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class KeyboardEvent.
 */
public class KeyboardEvent extends Event
{
	
	/** The key code. */
	public int keyCode;
	
	/** The key char. */
	public char keyChar;
	
	/** The key time. */
	public long keyTime;
	
	/** The key state. */
	public boolean keyState;
}
