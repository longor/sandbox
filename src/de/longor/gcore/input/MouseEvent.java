/*
 *
 */
package de.longor.gcore.input;

import de.longor.eventor.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class MouseEvent.
 */
public class MouseEvent extends Event
{

	/**
	 * The Enum MouseEventType.
	 */
	public static enum MouseEventType
	{

		/** The button. */
		BUTTON,

		/** The moved. */
		MOVED,

		/** The wheelmoved. */
		WHEELMOVED
	}

	/** The action. */
	public MouseEventType action;

	/** The mouse button. */
	public int mouseButton;

	/** The mouse button state. */
	public boolean mouseButtonState;

	/** The d_wheel. */
	public int d_wheel;

	/** The d_x. */
	public int d_x;

	/** The d_y. */
	public int d_y;

	/** The time. */
	public long time;

	/** The mouse x. */
	public int mouseX;

	/** The mouse y. */
	public int mouseY;

	/**
	 * Determine type.
	 */
	public void determineType()
	{
		if(mouseButton == -1)
		{
			// Can only be MOVED or WHEELMOVED
			if(d_wheel != 0)
			{
				action = MouseEventType.WHEELMOVED;
			}
			else
			{
				action = MouseEventType.MOVED;
			}
		}
		else
		{
			// Button
			action = MouseEventType.BUTTON;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return
				"MouseEvent:{" +
				"mouseButton: " + mouseButton + ", " +
				"mouseButtonState: " + mouseButtonState + ", " +
				"d_wheel: " + d_wheel + ", " +
				"d_x: " + d_x + ", " +
				"d_y: " + d_y + ", " +
				"time: " + time + ", " +
				"mouseX: " + mouseX + ", " +
				"mouseY: " + mouseY +
				"}"
		;
	}

	/**
	 * Checks if is a click.
	 *
	 * @return true, if is a click
	 */
	public boolean isClick()
	{
		if(action == MouseEventType.BUTTON)
		{
			return !mouseButtonState;
		}
		return false;
	}

	/**
	 * Checks if is left click.
	 *
	 * @return true, if is left click
	 */
	public boolean isLeftClick()
	{
		if(action == MouseEventType.BUTTON && mouseButton == 0)
		{
			return !mouseButtonState;
		}
		return false;
	}

	/**
	 * Checks if is right click.
	 *
	 * @return true, if is right click
	 */
	public boolean isRightClick()
	{
		if(action == MouseEventType.BUTTON && mouseButton == 1)
		{
			return !mouseButtonState;
		}
		return false;
	}

	/**
	 * Checks if is middle click.
	 *
	 * @return true, if is middle click
	 */
	public boolean isMiddleClick()
	{
		if(action == MouseEventType.BUTTON && mouseButton == 2)
		{
			return !mouseButtonState;
		}
		return false;
	}


}
