/*
 * 
 */
package de.longor.gcore.input;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import de.longor.eventor.Eventor;
import de.longor.gcore.graphics.IGLScreen;

// TODO: Auto-generated Javadoc
/**
 * The Class InputManager.
 */
public class InputManager
{
	
	/** The eventor. */
	Eventor eventor;
	
	/** The screen. */
	IGLScreen screen;
	
	/** The first mouse grab. */
	boolean firstMouseGrab;
	
	/**
	 * Instantiates a new input manager.
	 *
	 * @param eventor the eventor
	 * @param screen the screen
	 */
	public InputManager(Eventor eventor, IGLScreen screen)
	{
		this.eventor = eventor;
		this.screen = screen;
		firstMouseGrab = true;
	}
	
	/**
	 * Inits the.
	 *
	 * @throws LWJGLException the LWJGL exception
	 */
	public void init() throws LWJGLException
	{
		// Create Mouse and Keyboard
		Keyboard.create();
		Mouse.create();
		Mouse.setClipMouseCoordinatesToWindow(false);
	}
	
	/**
	 * Tick update.
	 */
	public void tickUpdate()
	{
		{
			KeyboardEvent event = new KeyboardEvent();
			while(Keyboard.next())
			{
				event.keyChar = Keyboard.getEventCharacter();
				event.keyCode = Keyboard.getEventKey();
				event.keyState = Keyboard.getEventKeyState();
				event.keyTime = Keyboard.getEventNanoseconds();
				eventor.post(event);
			}
		}
		
		{
			MouseEvent event = new MouseEvent();
			while(Mouse.next())
			{
				event.mouseButton = Mouse.getEventButton();
				event.mouseButtonState = Mouse.getEventButtonState();
				event.d_wheel = Mouse.getEventDWheel();
				event.d_x = Mouse.getEventDX();
				event.d_y = Mouse.getEventDY();
				event.time = Mouse.getEventNanoseconds();
				event.mouseX = Mouse.getEventX();
				event.mouseY = (int) (screen.getPhysicalHeight() - Mouse.getEventY());
				event.determineType();
				eventor.post(event);
			}
		}
		
		if(!isMouseGrabbed())
		{
			// fetch the values to prevent 'mouse grab jump'.
			Mouse.getDWheel();
			Mouse.getDX();
			Mouse.getDY();
		}
	}

	/**
	 * Checks if is key down.
	 *
	 * @param keyW the key w
	 * @return true, if is key down
	 */
	public boolean isKeyDown(int keyW)
	{
		// TODO: Deferred Input System
		return Keyboard.isKeyDown(keyW);
	}

	/**
	 * Checks if is mouse grabbed.
	 *
	 * @return true, if is mouse grabbed
	 */
	public boolean isMouseGrabbed()
	{
		return Mouse.isGrabbed();
	}

	/**
	 * Sets the mouse grabbed.
	 *
	 * @param b the new mouse grabbed
	 */
	public void setMouseGrabbed(boolean b)
	{
		Mouse.setGrabbed(b);
		
		if(!b && !firstMouseGrab)
		{
			Mouse.setCursorPosition((int) (screen.getPhysicalWidth()/2f),(int) (screen.getPhysicalHeight()/2f));
		}
		
		firstMouseGrab = false;
	}

	/**
	 * Gets the mouse x.
	 *
	 * @return the mouse x
	 */
	public float getMouseX() {
		return Mouse.getX();
	}
	
	/**
	 * Gets the mouse y.
	 *
	 * @return the mouse y
	 */
	public float getMouseY() {
		return screen.getPhysicalHeight()-Mouse.getY();
	}
	
	/**
	 * Gets the mouse y_ un inv.
	 *
	 * @return the mouse y_ un inv
	 */
	public float getMouseY_UnInv() {
		return Mouse.getY();
	}
	
}
