/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.opengl.GL11;

// TODO: Auto-generated Javadoc
/**
 * The Class VertexDrawerIM.
 */
public class VertexDrawerIM implements IVertexDrawer
{
	
	/** The drawer. */
	public static IVertexDrawer drawer = new VertexDrawerIM();
	
	/** The offx. */
	float offx = 0;
	
	/** The offy. */
	float offy = 0;
	
	/** The offz. */
	float offz = 0;
	
	/** The v count. */
	int vCount = 0;
	
	/** The is drawing. */
	boolean isDrawing = false;
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setOriginTranslation(float, float, float)
	 */
	@Override
	public void setOriginTranslation(float x, float y, float z) {
		this.offx = x;
		this.offy = y;
		this.offz = z;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexColor(float, float, float, float)
	 */
	@Override
	public void setVertexColor(float r, float g, float b, float a) {
		GL11.glColor4f(r, g, b, a);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexNormal(float, float, float)
	 */
	@Override
	public void setVertexNormal(float x, float y, float z) {
		GL11.glNormal3f(x, y, z);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexUV(int, float, float)
	 */
	@Override
	public void setVertexUV(int texUnit, float s, float t) {
		GL11.glTexCoord2f(s, t);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#vertex(float, float, float)
	 */
	@Override
	public void vertex(float x, float y, float z) {
		GL11.glVertex3f(this.offx+x, this.offy+y, this.offz+z);
		vCount++;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#startDrawing(int, boolean, boolean, boolean)
	 */
	@Override
	public void startDrawing(int mode, boolean color, boolean texture, boolean normals) {
		GL11.glBegin(mode);
		this.offx = 0;
		this.offy = 0;
		this.offz = 0;
		vCount = 0;
		isDrawing = true;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#stopDrawing()
	 */
	@Override
	public void stopDrawing() {
		GL11.glEnd();
		isDrawing = false;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#isDrawing()
	 */
	@Override
	public boolean isDrawing() {
		return isDrawing;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#hasDrawenAnyVertices()
	 */
	@Override
	public boolean hasDrawenAnyVertices() {
		return vCount > 0;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#getCurrentVertexCount()
	 */
	@Override
	public int getCurrentVertexCount() {
		return vCount;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#remainingUntilFailure()
	 */
	@Override
	public int remainingUntilFailure() {
		return Integer.MAX_VALUE / 2;
	}

}
