/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.opengl.GL11;

import de.longor1996.util.math.Color;
import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.Vector2F;
import de.longor1996.util.math.geom2D.RectangleF;

// TODO: Auto-generated Javadoc
/**
 * The Class BasicGeometry2D.
 */
public class BasicGeometry2D
{
	
	/** The va. */
	private static IVertexDrawer va;
	
	/** The off. */
	private static Vector2F off = new Vector2F(0, 0);
	
	/** The pc_r. */
	private static float pc_r = 1;
	
	/** The pc_g. */
	private static float pc_g = 1;
	
	/** The pc_b. */
	private static float pc_b = 1;
	
	/** The pc_a. */
	private static float pc_a = 1;
	
	/** The sc_r. */
	private static float sc_r = 0;
	
	/** The sc_g. */
	private static float sc_g = 0;
	
	/** The sc_b. */
	private static float sc_b = 0;
	
	/** The sc_a. */
	private static float sc_a = 1;
	
	/**
	 * Sets the drawer.
	 *
	 * @param va the new drawer
	 */
	public static void setDrawer(IVertexDrawer va)
	{
		BasicGeometry2D.va = va;
	}
	
	/**
	 * Sets the line width.
	 *
	 * @param width the new line width
	 */
	public static void setLineWidth(float width)
	{
		GL11.glLineWidth(MathUtility.clampFloat(0.25f, 4.00f, width));
	}
	
	/**
	 * Sets the primary color.
	 *
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public static void setPrimaryColor(float r, float g, float b, float a) {
		pc_r = r;
		pc_g = g;
		pc_b = b;
		pc_a = a;
	}
	
	/**
	 * Sets the primary color.
	 *
	 * @param color the new primary color
	 */
	public static void setPrimaryColor(Color color)
	{
		pc_r = color.r;
		pc_g = color.g;
		pc_b = color.b;
		pc_a = color.a;
	}
	
	/**
	 * Sets the secondary color.
	 *
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public static void setSecondaryColor(float r, float g, float b, float a) {
		sc_r = r;
		sc_g = g;
		sc_b = b;
		sc_a = a;
	}
	
	/**
	 * Sets the secondary color.
	 *
	 * @param color the new secondary color
	 */
	public static void setSecondaryColor(Color color)
	{
		sc_r = color.r;
		sc_g = color.g;
		sc_b = color.b;
		sc_a = color.a;
	}
	
	/**
	 * Draw flat untextured rectangle pd.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public static void drawFlatUntexturedRectanglePD(float x, float y, float w, float h)
	{
		va.startDrawing(GL11.GL_QUADS, true, false, false);
		va.setOriginTranslation(off.x + x, off.y + y, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a);
		va.vertex(0, 0, 0);
		va.vertex(w, 0, 0);
		va.vertex(w, h, 0);
		va.vertex(0, h, 0);
		va.stopDrawing();
	}
	
	/**
	 * Draw flat untextured rectangle pd.
	 *
	 * @param bounds the bounds
	 */
	public static void drawFlatUntexturedRectanglePD(RectangleF bounds)
	{
		drawFlatUntexturedRectanglePD(bounds.x, bounds.y, bounds.width, bounds.height);
	}
	
	/**
	 * Draw flat untextured rectangle pd.
	 *
	 * @param size the size
	 */
	public static void drawFlatUntexturedRectanglePD(Vector2F size)
	{
		drawFlatUntexturedRectanglePD(0, 0, size.x, size.y);
	}
	
	/**
	 * Draw rectangle border.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public static void drawRectangleBorder(float x, float y, float w, float h)
	{
		va.startDrawing(GL11.GL_LINE_LOOP, true, false, false);
		va.setOriginTranslation(off.x + x, off.y + y, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a);
		va.vertex(0, 0, 0);
		va.vertex(w, 0, 0);
		va.vertex(w, h, 0);
		va.vertex(0, h, 0);
		va.vertex(0, 0, 0);
		va.stopDrawing();
	}
	
	/**
	 * Draw rectangle border.
	 *
	 * @param bounds the bounds
	 */
	public static void drawRectangleBorder(RectangleF bounds)
	{
		drawRectangleBorder(bounds.x, bounds.y, bounds.width, bounds.height);
	}
	
	/**
	 * Draw rectangle border.
	 *
	 * @param size the size
	 */
	public static void drawRectangleBorder(Vector2F size)
	{
		drawRectangleBorder(0, 0, size.x, size.y);
	}
	
	/**
	 * Sets the offset.
	 *
	 * @param frameStackSum the new offset
	 */
	public static void setOffset(Vector2F frameStackSum)
	{
		off.setXY(frameStackSum);
	}
	
	/**
	 * Draw vertical gradient.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public static void drawVerticalGradient(float x, float y, float w, float h)
	{
		va.startDrawing(GL11.GL_QUADS, true, false, false);
		va.setOriginTranslation(off.x + x, off.y + y, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a); va.vertex(0, 0, 0);
		va.setVertexColor(sc_r, sc_g, sc_b, sc_a); va.vertex(w, 0, 0);
		va.setVertexColor(sc_r, sc_g, sc_b, sc_a); va.vertex(w, h, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a); va.vertex(0, h, 0);
		va.stopDrawing();
	}
	
	/**
	 * Draw horizontal gradient.
	 *
	 * @param x the x
	 * @param y the y
	 * @param w the w
	 * @param h the h
	 */
	public static void drawHorizontalGradient(float x, float y, float w, float h)
	{
		va.startDrawing(GL11.GL_QUADS, true, false, false);
		va.setOriginTranslation(off.x + x, off.y + y, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a); va.vertex(0, 0, 0);
		va.setVertexColor(sc_r, sc_g, sc_b, sc_a); va.vertex(w, 0, 0);
		va.setVertexColor(sc_r, sc_g, sc_b, sc_a); va.vertex(w, h, 0);
		va.setVertexColor(pc_r, pc_g, pc_b, pc_a); va.vertex(0, h, 0);
		va.stopDrawing();
	}
	
	/**
	 * Draw vertical gradient.
	 *
	 * @param size the size
	 */
	public static void drawVerticalGradient(Vector2F size)
	{
		drawVerticalGradient(0, 0, size.x, size.y);
	}
	
	/**
	 * Draw horizontal gradient.
	 *
	 * @param size the size
	 */
	public static void drawHorizontalGradient(Vector2F size)
	{
		drawHorizontalGradient(0, 0, size.x, size.y);
	}
	
}
