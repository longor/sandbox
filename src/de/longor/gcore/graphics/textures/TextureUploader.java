/*
 * 
 */
package de.longor.gcore.graphics.textures;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import de.longor.gcore.graphics.GLProfile;

// TODO: Auto-generated Javadoc
/**
 * The Class TextureUploader.
 */
public class TextureUploader
{
	// The Image
	/** The image width. */
	int imageWidth;
	
	/** The image height. */
	int imageHeight;
	
	/** The image data. */
	ByteBuffer imageData;
	
	// Misc
	/** The uploaded. */
	boolean uploaded;
	
	/** The debug. */
	boolean debug;
	
	// 'Upload Info'
	/** The texture name. */
	String textureName = null;
	
	/** The texture linear filter. */
	boolean textureLinearFilter = false;
	
	/** The texture internal format. */
	int textureInternalFormat = GL11.GL_RGBA;
	
	/** The texture wrapping. */
	int textureWrapping = GL11.GL_REPEAT;
	
	/** The texture mip map level. */
	int textureMipMapLevel = 0;
	
	/** The texture anisotropy. */
	int textureAnisotropy = 0;
	
	/** The instance type. */
	int instanceType;
	
	/**
	 * Instantiates a new texture uploader.
	 */
	public TextureUploader()
	{
		this.textureName = "<null>";
		this.instanceType = 0;
		this.imageData = null;
		this.imageWidth = -1;
		this.imageHeight = -1;
		this.uploaded = false;
		this.debug = false;
	}
	
	/**
	 * Instantiates a new texture uploader.
	 *
	 * @param bufImg the buf img
	 */
	public TextureUploader(BufferedImage bufImg)
	{
		this();
		this.imageData = TextureHelper.convertBufferedImageToByteBuffer(bufImg, null);
		this.imageWidth = bufImg.getWidth();
		this.imageHeight = bufImg.getHeight();
	}
	
	/**
	 * Instantiates a new texture uploader.
	 *
	 * @param width the width
	 * @param height the height
	 * @param data the data
	 */
	public TextureUploader(int width, int height, ByteBuffer data)
	{
		this();
		this.imageData = data;
		this.imageWidth = width;
		this.imageHeight = height;
	}
	
	/**
	 * Sets the image.
	 *
	 * @param image the new image
	 */
	public void setImage(BufferedImage image)
	{
		this.imageData = TextureHelper.convertBufferedImageToByteBuffer(image, null);
		this.imageWidth = image.getWidth();
		this.imageHeight = image.getHeight();
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name)
	{
		this.textureName = name;
	}

	/**
	 * Sets the type.
	 *
	 * @param i the new type
	 */
	public void setType(int i)
	{
		this.instanceType = i;
	}
	
	/**
	 * Sets the debug.
	 */
	public void setDebug()
	{
		this.debug = true;
	}
	
	/**
	 * Sets the wrap repeat.
	 */
	public void setWrapREPEAT()
	{
		this.textureWrapping = GL11.GL_REPEAT;
	}
	
	/**
	 * Sets the wrap clamp.
	 */
	public void setWrapCLAMP()
	{
		this.textureWrapping = GL11.GL_CLAMP;
	}
	
	/**
	 * Sets the wrap clampborder.
	 */
	public void setWrapCLAMPBORDER()
	{
		this.textureWrapping = GLProfile.GL13 ? GL13.GL_CLAMP_TO_BORDER : GL11.GL_CLAMP;
	}
	
	/**
	 * Sets the linear filter.
	 *
	 * @param linearFiltering the new linear filter
	 */
	public void setLinearFilter(boolean linearFiltering)
	{
		this.textureLinearFilter = linearFiltering;
	}
	
	/**
	 * Sets the anisotropy.
	 *
	 * @param val the new anisotropy
	 */
	public void setAnisotropy(int val)
	{
		if(val < 0)
		{
			this.textureAnisotropy = 0;
			return;
		}
		
		if(val > GLProfile.maxAnisotropy)
		{
			this.textureAnisotropy = GLProfile.maxAnisotropy;
			return;
		}
		
		this.textureAnisotropy = val;
	}
	
	/**
	 * Sets the mip map.
	 *
	 * @param level the new mip map
	 */
	public void setMipMap(int level)
	{
		if(level < 0)
		{
			this.textureMipMapLevel = 0;
			return;
		}
		
		if(level > 16)
		{
			this.textureMipMapLevel = 16;
			return;
		}
		
		this.textureMipMapLevel = level;
	}
	
	/**
	 * Upload.
	 *
	 * @return the i texture
	 */
	public ITexture upload()
	{
		if(this.uploaded)
		{
			throw new IllegalStateException("A TextureUploader cannot upload again after uploading a texture! Please create a new TextureUploader.");
		}
		
		if(this.debug) {
			System.out.println("[Info#] [TextureUploader] Creating Texture-Handle...");
		}
		
		// generate texture handle
		int id = GL11.glGenTextures();
		
		if(this.debug) {
			System.out.println("[Info#] [TextureUploader] Enabling&Binding Texture...");
		}
		
		// enable textures
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		
		if(this.debug) {
			System.out.println("[Info#] [TextureUploader] Configuring Texture-Parameters...");
		}
		
		// parameters
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, this.textureLinearFilter ? GL11.GL_LINEAR : GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, this.textureLinearFilter ? GL11.GL_LINEAR : GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, this.textureWrapping);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, this.textureWrapping);
		
		if(this.textureAnisotropy > 0)
		{
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, this.textureAnisotropy);
		}
		
		if(this.debug) {
			System.out.println("[Info#] [TextureUploader] Uploading Texture...");
		}
		
		// Upload Texture-Data.
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, this.textureInternalFormat, this.imageWidth, this.imageHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, this.imageData);
		
		// Upload MipMaps if asked for
		if(this.textureMipMapLevel > 0)
		{
			
			if(this.debug) {
				System.out.println("[Info#] [TextureUploader] Uploading MipMaps...");
			}
			
			// XXX: Bug: If the texture gets uploaded with MipMaps enabled, the data in the ByteBuffer gets 'corrupted'.
			
			for (int l = 1; l <= this.textureMipMapLevel; l++)
			{
				if(this.debug) {
					System.out.println("[Info#] [TextureUploader] Uploading MipMaps ... " + l + " of " + this.textureMipMapLevel);
				}
				
				// Shift the Bits
				int imgWm1 = this.imageWidth >> (l - 1);
				int imgW = this.imageWidth >> l;
				int imgH = this.imageHeight >> l;
				
				for (int x = 0; x < imgW; x++)
				{
					for (int y = 0; y < imgH; y++)
					{
						// Get the samples from the buffer.
						int l3 = this.imageData.getInt(((x * 2) + 0 + (((y * 2) + 0) * imgWm1)) * 4);
						int j4 = this.imageData.getInt(((x * 2) + 1 + (((y * 2) + 0) * imgWm1)) * 4);
						int k4 = this.imageData.getInt(((x * 2) + 1 + (((y * 2) + 1) * imgWm1)) * 4);
						int l4 = this.imageData.getInt(((x * 2) + 0 + (((y * 2) + 1) * imgWm1)) * 4);
						
						// Blend between the given samples using alphaBlend(A,B)
						int i5 = alphaBlend(alphaBlend(l3, j4), alphaBlend(k4, l4));
						
						// Put sampled pixel into buffer for upload
						this.imageData.putInt((x + (y * imgW)) * 4, i5);
					}
				}
				
				// upload
				GL11.glTexImage2D(GL11.GL_TEXTURE_2D, l, GL11.GL_RGBA, imgW, imgH, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, this.imageData);
			}
		}
		
		// unbind
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
		// change flag
		this.uploaded = true;
		
		if(this.debug) {
			System.out.println("[Info#] [TextureUploader] Texture Uploaded.");
		}
		
		switch(this.instanceType)
		{
			// case 1: return new TextureAtlas(id, this.imageWidth, this.imageHeight).setName(this.textureName);
			default: return new HandlerTexture(id, this.imageWidth, this.imageHeight);
		}
		
	}

	/**
	 *  Blends the Alpha Value between the Two given Integer Hex-Colors. *
	 *
	 * @param par1 the par1
	 * @param par2 the par2
	 * @return the int
	 */
	private static int alphaBlend(int par1, int par2) {
		int i = ((par1 & 0xff000000) >> 24) & 0xff;
		int j = ((par2 & 0xff000000) >> 24) & 0xff;
		char c = '\377';

		if ((i + j) < 255) {
			c = '\0';
			i = 1;
			j = 1;
		} else if (i > j) {
			i = 255;
			j = 1;
		} else {
			i = 1;
			j = 255;
		}

		int k = ((par1 >> 16) & 0xff) * i;
		int l = ((par1 >> 8) & 0xff) * i;
		int i1 = (par1 & 0xff) * i;
		int j1 = ((par2 >> 16) & 0xff) * j;
		int k1 = ((par2 >> 8) & 0xff) * j;
		int l1 = (par2 & 0xff) * j;
		int i2 = (k + j1) / (i + j);
		int j2 = (l + k1) / (i + j);
		int k2 = (i1 + l1) / (i + j);
		return (c << 24) | (i2 << 16) | (j2 << 8) | k2;
	}
	
}
