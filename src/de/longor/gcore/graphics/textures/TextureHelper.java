/*
 * 
 */
package de.longor.gcore.graphics.textures;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import de.longor1996.util.ByteBufferUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class TextureHelper.
 */
public class TextureHelper
{
	
	/** The Constant DEBUGPIXELS. */
	private static final BufferedImage DEBUGPIXELS;
	
	static
	{
		int A = 0xFF000000, B = 0xFFFE00FE;
		DEBUGPIXELS = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
		DEBUGPIXELS.setRGB(0, 0, A);
		DEBUGPIXELS.setRGB(1, 1, A);
		DEBUGPIXELS.setRGB(0, 1, B);
		DEBUGPIXELS.setRGB(1, 0, B);
	}
	
	/**
	 * Upload buffered image to open gl.
	 *
	 * @param image the image
	 * @return the int
	 */
	public static final int uploadBufferedImageToOpenGL(BufferedImage image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		ByteBuffer imageData = convertBufferedImageToByteBuffer(image, null);
		
		// generate texture handle
		int id = GL11.glGenTextures();
		
		// enable textures
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		
		// parameters
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
		
		// upload
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageData);
		
		// unbind
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
		return id;
	}
	
	/**
	 * Upload buffered image to open gl.
	 *
	 * @param imageData the image data
	 * @param width the width
	 * @param height the height
	 * @return the int
	 */
	public static final int uploadBufferImageToOpenGL(ByteBuffer imageData, int width, int height)
	{
		// generate texture handle
		int id = GL11.glGenTextures();
		
		// enable textures
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		
		// parameters
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
		
		// upload
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageData);
		
		// unbind
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
		return id;
	}
	
	/**
	 * Load image from classpath.
	 *
	 * @param path the path
	 * @return the buffered image
	 */
	public static final BufferedImage loadImageFromClasspath(String path)
	{
		try
		{
			InputStream inputStream = ClassLoader.getSystemResourceAsStream(path);
			
			if(inputStream == null)
			{
				throw new FileNotFoundException("Unable to open stream to file: " + path);
			}
			
			return ImageIO.read(inputStream);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return DEBUGPIXELS;
		}
	}
	
	/**
	 * Convert buffered image to byte buffer.
	 *
	 * @param image the image
	 * @param buffer the buffer
	 * @return the byte buffer
	 */
	public static final ByteBuffer convertBufferedImageToByteBuffer(BufferedImage image, ByteBuffer buffer)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		int surface = width * height;
		int bytes = surface * 4;
		
		if(buffer == null)
		{
			buffer = ByteBufferUtil.offheapNativeOrdered(bytes);
		}
		
		int[] argb = new int[surface];
		byte[] rgba = new byte[bytes];
		image.getRGB(0, 0, width, height, argb, 0, width);
		
		for(int i = 0, k = 0, pixel = 0; i < surface; i++)
		{
			pixel = argb[i];
			
			// to RGBA
			rgba[k++] = (byte) ((pixel >> 16) & 0xFF);
			rgba[k++] = (byte) ((pixel >> 8) & 0xFF);
			rgba[k++] = (byte) ((pixel) & 0xFF);
			rgba[k++] = (byte) ((pixel >> 24) & 0xFF);
		}
		
		buffer.clear();
		buffer.put(rgba);
		buffer.flip();
		
		return buffer;
	}
	
	/**
	 * Bind.
	 *
	 * @param unit the unit
	 * @param handle the handle
	 */
	public static void bind(int unit, int handle)
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, handle);
	}
	
	/**
	 * Bind.
	 *
	 * @param unit the unit
	 * @param handle the handle
	 * @param target the target
	 */
	public static void bind(int unit, int handle, int target)
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
		GL11.glBindTexture(target, handle);
	}

	/**
	 * Bind and enable.
	 *
	 * @param handle the handle
	 */
	public static void bindAndEnable(int handle)
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, handle);
	}
	
	/**
	 * Creates the null image.
	 *
	 * @return the buffered image
	 */
	public static BufferedImage createNullImage()
	{
		BufferedImage image = new BufferedImage(2,2,BufferedImage.TYPE_INT_ARGB);
		int A = 0xFF050505, B = 0xFFFF00FF;
		image.setRGB(0, 0, A);
		image.setRGB(1, 1, A);
		image.setRGB(1, 0, B);
		image.setRGB(0, 1, B);
		image.flush();
		return image;
	}
	
}
