/*
 * 
 */
package de.longor.gcore.graphics.textures;

// TODO: Auto-generated Javadoc
/**
 * The Interface ITexture.
 */
public interface ITexture
{
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getID();
	
	/**
	 * Gets the width.
	 *
	 * @return the width
	 */
	public int getWidth();
	
	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight();
	
	/**
	 * Bind.
	 */
	public void bind();
	
	/**
	 * Unbind.
	 */
	public void unbind();
}
