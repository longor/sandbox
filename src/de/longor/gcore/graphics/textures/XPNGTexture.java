/*
 * 
 */
package de.longor.gcore.graphics.textures;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.zip.GZIPInputStream;

import org.lwjgl.opengl.GL11;

import de.longor1996.util.ByteBufferUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class XPNGTexture.
 */
public class XPNGTexture implements ITexture
{
	
	/**
	 * The Class XPNGDataLayer.
	 */
	public class XPNGDataLayer
	{
		
		/** The width. */
		int width;
		
		/** The height. */
		int height;
		
		/** The surface. */
		int surface;
		
		/** The image data. */
		ByteBuffer imageData;
		
		/**
		 * Read.
		 *
		 * @param in the in
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		public void read(DataInputStream in) throws IOException
		{
			// out.writeInt(image.getWidth());
			// out.writeInt(image.getHeight());
			
			width = in.readInt();
			height = in.readInt();
			surface = width * height;
			
			// out.writeByte(?); // encoding
			int encoding = in.readByte();
			//System.out.println("Reading XPNGImgLayer with size " + width + "x" + height + " and encoding " + encoding + "!");
			
			if(encoding == 0)
			{
				readRawImg(in);
			}
			else if(encoding == 1)
			{
				readGZipdImg(in);
			}
			else
			{
				throw new IOException("Failed to load layer: Unknown encoding '"+encoding+"'!");
			}
		}
		
		/**
		 * Read g zipd img.
		 *
		 * @param in the in
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		private void readGZipdImg(DataInputStream in) throws IOException
		{
			// out.writeInt(img.length); // real size
			// out.writeInt(imgC.length); // compressed size
			// out.write(imgC); // compressed image
			
			int realSize = in.readInt();
			int compressedSize = in.readInt();
			//System.out.println("Reading compressed data: rs = " + realSize + ", cs = " + compressedSize + "!");
			
			byte[] compressed = new byte[compressedSize];
			in.readFully(compressed);
			
			GZIPInputStream gin = new GZIPInputStream(new ByteArrayInputStream(compressed), compressedSize+1);
	        int n = 0;
	        byte[] data = new byte[realSize];
	        while (n < realSize) {
	            int count = gin.read(data, n, realSize - n);
	            if (count < 0)
	                throw new EOFException();
	            n += count;
	        }
	        gin.close();
	        
			imageData = ByteBufferUtil.offheapNativeOrdered(realSize);
			imageData.put(data);
			imageData.flip();
		}
		
		/**
		 * Read raw img.
		 *
		 * @param in the in
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		private void readRawImg(DataInputStream in) throws IOException
		{
			// out.writeInt(img.length); // real size
			// out.write(img); // real image
			
			int realSize = in.readInt();
			//System.out.println("Reading raw data: rs = " + realSize + "!");
			
			imageData = ByteBufferUtil.offheapNativeOrdered(realSize);
			
			byte[] data = new byte[realSize];
			in.readFully(data);
			imageData.put(data);
			imageData.flip();
		}
	}
	
	/** The image. */
	XPNGDataLayer image;
	
	/** The mipmaps. */
	XPNGDataLayer[] mipmaps;
	
	/** The glid. */
	int glid = -1;
	
	/** The uploadable. */
	boolean uploadable = false;
	
	/** The linear filtering. */
	boolean linearFiltering;
	
	/**
	 * Instantiates a new XPNG texture.
	 *
	 * @param linearFiltering the linear filtering
	 * @param input the input
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public XPNGTexture(boolean linearFiltering, InputStream input) throws IOException
	{
		this.linearFiltering = linearFiltering;
		
		DataInputStream in = new DataInputStream(input);
		in.skipBytes(4);
		int v = in.readByte();
		
		if(v != 1)
		{
			throw new IOException("Can't read XPNG-file: Unknown version '"+Integer.toHexString(v)+"'!");
		}
		
		image = new XPNGDataLayer();
		image.read(in);
		
		int mipmapCount = in.readByte();
		mipmaps = new XPNGDataLayer[mipmapCount];
		for(int i = 0; i < mipmapCount; i++)
		{
			mipmaps[i] = new XPNGDataLayer();
			mipmaps[i].read(in);
		}
		
		in.close();
		uploadable = true;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getID()
	 */
	@Override
	public int getID() {
		return glid;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getWidth()
	 */
	@Override
	public int getWidth() {
		return image.width;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getHeight()
	 */
	@Override
	public int getHeight() {
		return image.height;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#bind()
	 */
	@Override
	public void bind()
	{
		if(glid == -1)
		{
			if(!uploadable)
			{
				return;
			}
			
			glid = GL11.glGenTextures();
			
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, glid);
			
			if(mipmaps.length > 0)
			{
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
				
				if(linearFiltering)
				{
					GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
				}
				else
				{
					GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
				}
			}
			else
			{
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			}
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			
			/*
			if(anisotropy > 0 && GLProfile.anisotropyEnabled)
			{
				float A = anisotropy > GLProfile.maxAnisotropy ? GLProfile.maxAnisotropy : anisotropy;
				GL11.glTexParameterf(GL11.GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, A);
			}
			//*/
			
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, image.width, image.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, image.imageData);
			image.imageData = null;
			
			if(mipmaps.length > 0)
			{
				for(int i = 0; i < mipmaps.length; i++)
				{
					XPNGDataLayer l = mipmaps[i];
					GL11.glTexImage2D(GL11.GL_TEXTURE_2D, i, GL11.GL_RGBA, l.width, l.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, l.imageData);
					l.imageData = null;
				}
			}
			
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		}
		else
		{
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, glid);
		}
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#unbind()
	 */
	@Override
	public void unbind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}
	
}
