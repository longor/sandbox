/*
 * 
 */
package de.longor.gcore.graphics.textures;

import org.lwjgl.opengl.GL11;

// TODO: Auto-generated Javadoc
/**
 * The Class BasicTexture2.
 */
public class HandlerTexture implements ITexture
{
	
	/** The glid. */
	int glid;
	
	/** The width. */
	int width;
	
	/** The height. */
	int height;
	
	/**
	 * Instantiates a new basic texture2.
	 *
	 * @param glid the glid
	 * @param w the w
	 * @param h the h
	 */
	public HandlerTexture(int glid, int w, int h)
	{
		this.glid = glid;
		width = w;
		height = h;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getID()
	 */
	@Override
	public int getID() {
		return glid;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getWidth()
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getHeight()
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#bind()
	 */
	@Override
	public void bind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, glid);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#unbind()
	 */
	@Override
	public void unbind() {
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}

}
