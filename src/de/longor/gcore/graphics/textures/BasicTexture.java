/*
 * 
 */
package de.longor.gcore.graphics.textures;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL11;

import de.longor.gcore.graphics.GLProfile;
import de.longor1996.util.ByteBufferUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class BasicTexture.
 */
public class BasicTexture implements ITexture
{
	
	/** The Constant WHITEPIXEL. */
	public static final BasicTexture WHITEPIXEL;
	
	/** The Constant BLACKPIXEL. */
	public static final BasicTexture BLACKPIXEL;
	
	/** The Constant CHECKERBOARD. */
	public static final BasicTexture CHECKERBOARD;
	
	static
	{
		BufferedImage wp = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
		{
			int D = 0xFFFFFFFF;
			wp.setRGB(0, 0, D);
			wp.setRGB(1, 0, D);
			wp.setRGB(1, 1, D);
			wp.setRGB(0, 1, D);
		}
		BufferedImage bp = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
		{
			int D = 0xFF000000;
			bp.setRGB(0, 0, D);
			bp.setRGB(1, 0, D);
			bp.setRGB(1, 1, D);
			bp.setRGB(0, 1, D);
		}
		BufferedImage cp = new BufferedImage(2, 2, BufferedImage.TYPE_INT_ARGB);
		{
			int D = 0xFFFFFFFF;
			int O = 0xFFFF44FF;
			cp.setRGB(0, 0, D);
			cp.setRGB(1, 0, O);
			cp.setRGB(1, 1, D);
			cp.setRGB(0, 1, O);
		}
		
		WHITEPIXEL = new BasicTexture(wp);
		BLACKPIXEL = new BasicTexture(bp);
		CHECKERBOARD = new BasicTexture(cp);
	}
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The surface. */
	private int surface;
	
	/** The bpp. */
	private int bpp;
	
	/** The bytes. */
	private int bytes;
	
	/** The image. */
	private BufferedImage image;
	
	/** The image data. */
	private ByteBuffer imageData;
	
	/** The glid. */
	private int glid;
	
	/** The anisotropy. */
	public int anisotropy = 0;
	
	/** The mip mapping. */
	public int mipMapping = 0;
	
	/**
	 * Instantiates a new basic texture.
	 *
	 * @param input the input
	 */
	public BasicTexture(InputStream input)
	{
		try {
			image = ImageIO.read(input);
		} catch (IOException e) {
			e.printStackTrace();
			assert false : "Failed to load image.";
		}
		
		glid = -1;
		bpp = 4;
		image(image);
		unpack();
	}
	
	/**
	 * Instantiates a new basic texture.
	 *
	 * @param img the img
	 */
	public BasicTexture(BufferedImage img)
	{
		glid = -1;
		bpp = 4;
		
		image(img);
		unpack();
	}
	
	/**
	 * Image.
	 *
	 * @param img the img
	 */
	public void image(BufferedImage img)
	{
		image = img;
		width = image.getWidth();
		height = image.getHeight();
		surface = width * height;
		bytes = bpp * surface;
	}
	
	/**
	 * Unpack.
	 */
	public void unpack()
	{
		imageData = ByteBufferUtil.offheapNativeOrdered(bytes);
		
		// Setup more Buffers
		int rawPictureBuffer[] = new int[surface];
		byte outputPictureBuffer[] = new byte[bytes];
		
		// Put the Picture into the Raw Picture Buffer
		image.getRGB(0, 0, width, height, rawPictureBuffer, 0, width);
		
		//
		for (int k = 0; k < rawPictureBuffer.length; k++) {
			// Get the Color Values
			int i1 = (rawPictureBuffer[k] >> 24) & 0xff;
			int k1 = (rawPictureBuffer[k] >> 16) & 0xff;
			int i2 = (rawPictureBuffer[k] >> 8) & 0xff;
			int k2 = rawPictureBuffer[k] & 0xff;
			
			// Put into Output Buffer
			outputPictureBuffer[(k * 4) + 0] = (byte) k1;
			outputPictureBuffer[(k * 4) + 1] = (byte) i2;
			outputPictureBuffer[(k * 4) + 2] = (byte) k2;
			outputPictureBuffer[(k * 4) + 3] = (byte) i1;
		}
		
		// Put the Output Buffer into the ByteBuffer
		imageData.clear();
		imageData.put(outputPictureBuffer);
		imageData.position(0).limit(outputPictureBuffer.length);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#bind()
	 */
	public void bind()
	{
		if(glid == -1)
		{
			upload();
		}
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, glid);
	}
	
	/**
	 * Upload.
	 */
	public void upload()
	{
		if(glid != -1)
			throw new IllegalStateException("Texture already uploaded! " + glid);
		
		glid = GL11.glGenTextures();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, glid);
		
		if(mipMapping > 0)
		{
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		}
		
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		
		if(anisotropy > 0 && GLProfile.anisotropyEnabled)
		{
			float A = anisotropy > GLProfile.maxAnisotropy ? GLProfile.maxAnisotropy : anisotropy;
			GL11.glTexParameterf(GL11.GL_TEXTURE_2D, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, A);
		}
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageData);
		
		if(mipMapping > 0)
		{
			for (int l = 1; l <= mipMapping; l++)
			{
				// Shift the Bits
				int j1 = width >> (l - 1);
				int l1 = width >> l;
				int j2 = height >> l;
				
				if(l1 <= 0 || j2 <= 0)
				{
					break;
				}
				
				for (int l2 = 0; l2 < l1; l2++)
				{
					for (int j3 = 0; j3 < j2; j3++) {
						// Blend the Color Values
						int l3 = imageData.getInt(((l2 * 2) + 0 + (((j3 * 2) + 0) * j1)) * 4);
						int j4 = imageData.getInt(((l2 * 2) + 1 + (((j3 * 2) + 0) * j1)) * 4);
						int k4 = imageData.getInt(((l2 * 2) + 1 + (((j3 * 2) + 1) * j1)) * 4);
						int l4 = imageData.getInt(((l2 * 2) + 0 + (((j3 * 2) + 1) * j1)) * 4);
						
						// Blend Alpha
						int i5 = alphaBlend(alphaBlend(l3, j4), alphaBlend(k4, l4));
						
						// Put into MipMap Texture Buffer
						imageData.putInt((l2 + (j3 * l1)) * 4, i5);
					}
				}
				
				// Send to OpenGL
				GL11.glTexImage2D(GL11.GL_TEXTURE_2D, l, GL11.GL_RGBA, l1, j2, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, imageData);
			}
		}
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		
		// clean up the imageData, we dont need it anymore!
		imageData = null;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#unbind()
	 */
	public void unbind()
	{
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
	}

	/**
	 *  Blends the Alpha Value between the Two given Integer Hex-Colors. *
	 *
	 * @param a the a
	 * @param b the b
	 * @return the int
	 */
	private static final int alphaBlend(int a, int b)
	{
		int i = ((a & 0xff000000) >> 24) & 0xff;
		int j = ((b & 0xff000000) >> 24) & 0xff;
		char c = '\377';
		
		if ((i + j) < 255) {
			c = '\0';
			i = 1;
			j = 1;
		} else if (i > j) {
			i = 255;
			j = 1;
		} else {
			i = 1;
			j = 255;
		}
		
		int k = ((a >> 16) & 0xff) * i;
		int l = ((a >> 8) & 0xff) * i;
		int i1 = (a & 0xff) * i;
		int j1 = ((b >> 16) & 0xff) * j;
		int k1 = ((b >> 8) & 0xff) * j;
		int l1 = (b & 0xff) * j;
		int i2 = (k + j1) / (i + j);
		int j2 = (l + k1) / (i + j);
		int k2 = (i1 + l1) / (i + j);
		return (c << 24) | (i2 << 16) | (j2 << 8) | k2;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getID()
	 */
	@Override
	public int getID() {
		return glid;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getWidth()
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.textures.ITexture#getHeight()
	 */
	@Override
	public int getHeight() {
		return height;
	}
	
}
