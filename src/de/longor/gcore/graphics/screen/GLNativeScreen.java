/*
 * 
 */
package de.longor.gcore.graphics.screen;

import org.bushe.swing.event.annotation.EventSubscriber;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import de.longor.gcore.crashreport.CrashReportBuildEvent;
import de.longor.gcore.graphics.GLProfile;
import de.longor.gcore.graphics.IGLScreen;

// TODO: Auto-generated Javadoc
/**
 * The Class GLNativeScreen.
 */
public class GLNativeScreen implements IGLScreen
{
	
	/** The title. */
	private String title;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The virtual width. */
	private int virtualWidth;
	
	/** The virtual height. */
	private int virtualHeight;
	
	/** The virtual width f. */
	private float virtualWidthF;
	
	/** The virtual height f. */
	private float virtualHeightF;
	
	/** The force resize. */
	private boolean forceResize;
	
	/** The scale factor. */
	private int scaleFactor;
	
	/** The scale. */
	private int scale;
	
	/** The aspect. */
	private float aspect;
    
    /** The Minimum virtual screen width. */
    private int MinimumVirtualScreenWidth;
    
    /** The Minimum virtual screen height. */
    private int MinimumVirtualScreenHeight;
    
    /** The framerate cap. */
    private int framerateCap;
	
	/**
	 * Instantiates a new GL native screen.
	 */
	public GLNativeScreen()
	{
		// initial state (reduces the chance of errors at startup)
		title = "Game?";
		width = 16;
		height = 9;
		aspect = 16F / 9F;
		virtualWidth = 16;
		virtualHeight = 9;
		virtualWidthF = 16F;
		virtualHeightF = 9F;
		scaleFactor = 1;
		forceResize = true;
		
		// default settings
		setVirtualMinimumScreenSize(320, 240);
		setVirtualScale(0);
		setFramerateCap(-1);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#show()
	 */
	public void show() throws LWJGLException
	{
		Display.setDisplayMode(new DisplayMode(800, 500));
		Display.create();
		
		Display.setResizable(true);
		Display.setTitle(title);
		GLProfile.loadProfile();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#pre_update()
	 */
	public boolean pre_update()
	{
		int nw = Display.getWidth();
		int nh = Display.getHeight();
		boolean resized = false;
		
		if(nw != width || nh != height || forceResize)
		{
			forceResize = false;
			width = nw;
			height = nh;
			aspect = (float)width / (float) height;
			recalculateScaledResolution();
			GL11.glViewport(0, 0, nw, nh);
			resized = true;
		}
		
		return resized;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#post_update()
	 */
	public void post_update()
	{
		Display.update();
		Display.sync(framerateCap);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#cleanup()
	 */
	public void cleanup()
	{
		Display.destroy();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#isCloseRequested()
	 */
	public boolean isCloseRequested()
	{
		return Display.isCloseRequested();
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getVirtualWidth()
	 */
	public float getVirtualWidth()
	{
		return virtualWidth;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getVirtualHeight()
	 */
	public float getVirtualHeight()
	{
		return virtualHeight;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getPhysicalWidth()
	 */
	public float getPhysicalWidth()
	{
		return width;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getPhysicalHeight()
	 */
	public float getPhysicalHeight()
	{
		return height;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getAspectRatio()
	 */
	public float getAspectRatio()
	{
		return aspect;
	}
	
	/**
	 * Recalculates the scaled ('virtual') resolution.
	 **/
	public void recalculateScaledResolution()
	{
        final int scale = this.scale == 0 ? 1000 : this.scale;
        int scaleFactor = 1;
        
        this.virtualWidth = this.width;
        this.virtualHeight = this.height;
        
		// Calculate the scaling value. This loop will run only a couple of times.
        while ((scaleFactor < scale) &&
        		((this.virtualWidth  / (scaleFactor + 1)) >= MinimumVirtualScreenWidth) &&
				((this.virtualHeight / (scaleFactor + 1)) >= MinimumVirtualScreenHeight))
        {
            ++scaleFactor;
        }
		
        // ...?
        this.scaleFactor = scaleFactor;
        
		// Calculate the scaled screen dimension.
        this.virtualWidthF = (float)this.virtualWidth / (float)this.scaleFactor;
        this.virtualHeightF = (float)this.virtualHeight / (float)this.scaleFactor;
        
        // Round(upwards) the floating-point dimensions and cast them into integers.
        this.virtualWidth = (int) Math.ceil(this.virtualWidthF);
        this.virtualHeight = (int) Math.ceil(this.virtualHeightF);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#clear(boolean, boolean, boolean)
	 */
	public void clear(boolean color, boolean depth, boolean stencil)
	{
		int mask = 0;
		
		mask |= color ? GL11.GL_COLOR_BUFFER_BIT : 0;
		mask |= depth ? GL11.GL_DEPTH_BUFFER_BIT : 0;
		mask |= stencil ? GL11.GL_STENCIL_BUFFER_BIT : 0;
		
		GL11.glClear(mask);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setTitle(java.lang.String)
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setVirtualScale(int)
	 */
	public void setVirtualScale(int scale)
	{
		if(scale <= 0)
		{
			scale = 1000;
		}
		
		this.scale = scale;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setFramerateCap(int)
	 */
	public void setFramerateCap(int maximumFramerate)
	{
		this.framerateCap = maximumFramerate;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setVirtualMinimumScreenSize(int, int)
	 */
	public void setVirtualMinimumScreenSize(int vw, int vh)
	{
		this.MinimumVirtualScreenWidth = vw;
		this.MinimumVirtualScreenHeight = vh;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setPhysicalMinimumScreenSize(int, int)
	 */
	@Override
	public void setPhysicalMinimumScreenSize(int vw, int vh)
	{
		System.err.println("Changing the minimum physical screen dimensions is not supported by GLNativeScreen.");
	}
	
	/**
	 * Crash report build event.
	 *
	 * @param event the event
	 */
	@EventSubscriber
	public void crashReportBuildEvent(CrashReportBuildEvent event)
	{
		event.report.addObject(this);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return	"Screen:{" +
					"aspect: " + aspect + ", " + 
					"width: " + width + ", " + 
					"height: " + height + ", " + 
					"virtual-width: " + virtualWidth + ", " + 
					"virtual-height: " + virtualHeight + ", " + 
					"virtual-scale: " + scale + ", " + 
					"virtual-scalefactor: " + scaleFactor +  
				"}";
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setBorderless(boolean)
	 */
	@Override
	public void setBorderless(boolean isBorderless)
	{
		System.err.println("GLNativeScreen cannot be borderless.");
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setExpandWindow(boolean)
	 */
	@Override
	public void setExpandWindow(boolean expand)
	{
		System.err.println("GLNativeScreen cannot be expanded.");
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setResized()
	 */
	@Override
	public void setResized() {
		forceResize = true;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#isPhysicalSizeReal()
	 */
	@Override
	public boolean isPhysicalSizeReal() {
		if(getPhysicalWidth() < 1)
			return false;
		if(getPhysicalHeight() < 1)
			return false;
		
		return true;
	}
	
}
