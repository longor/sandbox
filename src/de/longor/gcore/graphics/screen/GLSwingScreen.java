/*
 * 
 */
package de.longor.gcore.graphics.screen;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import de.longor.gcore.graphics.GLProfile;
import de.longor.gcore.graphics.IGLScreen;

// TODO: Auto-generated Javadoc
/**
 * The Class GLSwingScreen.
 */
public class GLSwingScreen implements IGLScreen
{
	
	/** The window. */
	private JFrame window;
	
	/** The canvas. */
	private Canvas canvas;
	
	/** The is close requested. */
	private boolean isCloseRequested;
	
	/** The force resize. */
	private boolean forceResize;
	
	/** The title. */
	private String title;
	
	/** The borderless. */
	private boolean borderless;
	
	/** The expand. */
	private boolean expand;
	
	/** The width. */
	private int width;
	
	/** The height. */
	private int height;
	
	/** The virtual width. */
	private int virtualWidth;
	
	/** The virtual height. */
	private int virtualHeight;
	
	/** The virtual width f. */
	private float virtualWidthF;
	
	/** The virtual height f. */
	private float virtualHeightF;
	
	/** The scale factor. */
	private int scaleFactor;
	
	/** The scale. */
	private int scale;
	
	/** The aspect. */
	private float aspect;
    
    /** The Minimum virtual screen width. */
    private int MinimumVirtualScreenWidth;
    
    /** The Minimum virtual screen height. */
    private int MinimumVirtualScreenHeight;
    
    /** The Minimum physical screen width. */
    private int MinimumPhysicalScreenWidth;
    
    /** The Minimum physical screen height. */
    private int MinimumPhysicalScreenHeight;
    
    /** The framerate cap. */
    private int framerateCap;
    
	/**
	 * Instantiates a new GL swing screen.
	 */
	public GLSwingScreen()
	{
		// initial state (reduces the chance of errors at startup)
		title = "Game?";
		width = 16;
		height = 9;
		aspect = 16F / 9F;
		virtualWidth = 16;
		virtualHeight = 9;
		virtualWidthF = 16F;
		virtualHeightF = 9F;
		scaleFactor = 1;
		isCloseRequested = false;
		forceResize = true;
		
		// default settings
		setVirtualMinimumScreenSize(320, 240);
		setVirtualScale(0);
		setFramerateCap(-1);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#show()
	 */
	@Override
	public void show() throws LWJGLException {
		window = new JFrame(title);
		window.setSize(800, 500);
		window.setPreferredSize(new Dimension(800, 500));
		window.setLocationRelativeTo(null);
		window.setResizable(true);
		window.setLayout(new BorderLayout());
		window.setUndecorated(borderless);
		window.setMinimumSize(new Dimension(MinimumPhysicalScreenWidth, MinimumPhysicalScreenHeight));
		window.setBackground(Color.BLACK);
		
		if(expand)
		{
			window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
		
		canvas = new Canvas();
		canvas.setIgnoreRepaint(true);
		
		window.add(canvas, BorderLayout.CENTER);
		window.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent event)
			{
				isCloseRequested = true;
			}
		});
		window.setVisible(true);
		
		Display.setParent(canvas);
		
		Display.create();
		Display.setDisplayMode(new DisplayMode(800, 500));
		GLProfile.loadProfile();
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#cleanup()
	 */
	@Override
	public void cleanup()
	{
		Display.update();
		Display.destroy();
		
		window.dispose();
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#pre_update()
	 */
	@Override
	public boolean pre_update()
	{
		int nw = Display.getWidth();
		int nh = Display.getHeight();
		boolean resized = false;
		
		if(nw != width || nh != height || forceResize)
		{
			forceResize = false;
			width = nw;
			height = nh;
			aspect = (float)width / (float) height;
			recalculateScaledResolution();
			GL11.glViewport(0, 0, nw, nh);
			resized = true;
		}
		
		return resized;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#post_update()
	 */
	@Override
	public void post_update()
	{
		Display.update();
		Display.sync(framerateCap);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#clear(boolean, boolean, boolean)
	 */
	@Override
	public void clear(boolean color, boolean depth, boolean stencil) {
		int mask = 0;
		
		mask |= color ? GL11.GL_COLOR_BUFFER_BIT : 0;
		mask |= depth ? GL11.GL_DEPTH_BUFFER_BIT : 0;
		mask |= stencil ? GL11.GL_STENCIL_BUFFER_BIT : 0;
		
		GL11.glClear(mask);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#isCloseRequested()
	 */
	@Override
	public boolean isCloseRequested() {
		return Display.isCloseRequested() || isCloseRequested;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getVirtualWidth()
	 */
	@Override
	public float getVirtualWidth() {
		return virtualWidthF;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getVirtualHeight()
	 */
	@Override
	public float getVirtualHeight() {
		return virtualHeightF;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getPhysicalWidth()
	 */
	@Override
	public float getPhysicalWidth() {
		return width;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getPhysicalHeight()
	 */
	@Override
	public float getPhysicalHeight() {
		return height;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#getAspectRatio()
	 */
	@Override
	public float getAspectRatio() {
		return aspect;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String ttitle) {
		title = ttitle;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setVirtualScale(int)
	 */
	@Override
	public void setVirtualScale(int scale)
	{
		if(scale <= 0)
		{
			scale = 1000;
		}
		
		this.scale = scale;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setFramerateCap(int)
	 */
	@Override
	public void setFramerateCap(int maximumFramerate) {
		framerateCap = maximumFramerate;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setVirtualMinimumScreenSize(int, int)
	 */
	@Override
	public void setVirtualMinimumScreenSize(int vw, int vh)
	{
		this.MinimumVirtualScreenWidth = vw;
		this.MinimumVirtualScreenHeight = vh;
	}
	
	/**
	 * Recalculates the scaled ('virtual') resolution.
	 **/
	public void recalculateScaledResolution()
	{
        final int scale = this.scale == 0 ? 1000 : this.scale;
        int scaleFactor = 1;
        
        this.virtualWidth = this.width;
        this.virtualHeight = this.height;
        
		// Calculate the scaling value. This loop will run only a couple of times.
        while ((scaleFactor < scale) &&
        		((this.virtualWidth  / (scaleFactor + 1)) >= MinimumVirtualScreenWidth) &&
				((this.virtualHeight / (scaleFactor + 1)) >= MinimumVirtualScreenHeight))
        {
            ++scaleFactor;
        }
		
        // ...?
        this.scaleFactor = scaleFactor;
        
		// Calculate the scaled screen dimension.
        this.virtualWidthF = (float)this.virtualWidth / (float)this.scaleFactor;
        this.virtualHeightF = (float)this.virtualHeight / (float)this.scaleFactor;
        
        // Round(upwards) the floating-point dimensions and cast them into integers.
        this.virtualWidth = (int) Math.ceil(this.virtualWidthF);
        this.virtualHeight = (int) Math.ceil(this.virtualHeightF);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setPhysicalMinimumScreenSize(int, int)
	 */
	@Override
	public void setPhysicalMinimumScreenSize(int vw, int vh) {
		MinimumPhysicalScreenWidth = vw;
		MinimumPhysicalScreenHeight = vh;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setBorderless(boolean)
	 */
	@Override
	public void setBorderless(boolean isBorderless) {
		borderless = isBorderless;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setExpandWindow(boolean)
	 */
	@Override
	public void setExpandWindow(boolean expand) {
		this.expand = expand;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#setResized()
	 */
	@Override
	public void setResized() {
		forceResize = true;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IGLScreen#isPhysicalSizeReal()
	 */
	@Override
	public boolean isPhysicalSizeReal() {
		if(getPhysicalWidth() < 1)
			return false;
		if(getPhysicalHeight() < 1)
			return false;
		
		return true;
	}

}
