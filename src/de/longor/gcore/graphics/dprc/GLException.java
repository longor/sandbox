/*
 * 
 */
package de.longor.gcore.graphics.dprc;

// TODO: Auto-generated Javadoc
/**
 * The Class GLException.
 */
public class GLException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8505926408034956437L;

	/**
	 * Instantiates a new GL exception.
	 *
	 * @param string the string
	 */
	public GLException(String string) {
		super(string);
	}
	
}
