/*
 * 
 */
package de.longor.gcore.graphics.dprc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GLContext;

// TODO: Auto-generated Javadoc
/**A full OpenGL GLSL Shader (Vertex and Fragment Shader, Geometry shader not yet supported!).**/
public class GLSLShader {
	
	/** The Program id. */
	private int ProgramID = 0;
	
	/** The FRAG shader id. */
	private int FRAGShaderID = 0;
	
	/** The VERT shader id. */
	private int VERTShaderID = 0;
	
	/** The shader vertex. */
	private StringBuffer SHADER_VERTEX;
	
	/** The shader fragment. */
	private StringBuffer SHADER_FRAGMENT;
	
	/** The is usable. */
	private boolean isUsable = false;
	
	/**
	 * Instantiates a new GLSL shader.
	 *
	 * @param input the input
	 * @throws GLException the GL exception
	 */
	public GLSLShader(InputStream input) throws GLException{
		
		if(!GLContext.getCapabilities().GL_ARB_shader_objects)
			throw new GLException("GL-Incompatible-Error: GL_ARB_shader_objects is not supported! (Update your Graphicscard!)");
		if(!GLContext.getCapabilities().GL_ARB_vertex_shader)
			throw new GLException("GL-Incompatible-Error: GL_ARB_vertex_shader is not supported! (Update your Graphicscard!)");
		if(!GLContext.getCapabilities().GL_ARB_fragment_shader)
			throw new GLException("GL-Incompatible-Error: GL_ARB_fragment_shader is not supported! (Update your Graphicscard!)");
		
		if(input == null)
			throw new IllegalArgumentException("Input cannot be null!");
		
		// LOAD shader
		this.loadFileIntoBuffers(input);
		
		// CREATE objects
		this.ProgramID = GL20.glCreateProgram();
		this.FRAGShaderID = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
		this.VERTShaderID = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		
		// CHECK validity
		if (this.ProgramID == 0)
            throw new GLException("Error creating shader: Failed to Create Program-Object!");
		
		// CHECK validity
		if (this.FRAGShaderID == 0)
            throw new GLException("Error creating shader: Failed to Create FragmentShader-Object!");
		
		// CHECK validity
		if (this.VERTShaderID == 0)
            throw new GLException("Error creating shader: Failed to Create VertexShader-Object!");
		
		System.out.println("[Info.GL] Created new GLSL-Shader from: " + input.toString());
		
	}
	
	/**
	 * Compiles the Sources.*
	 *
	 * @throws GLException the GL exception
	 */
	public void doLoadIntoOpenGL() throws GLException
	{
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			GL20.glShaderSource(this.VERTShaderID,this.SHADER_VERTEX.toString());
			GL20.glCompileShader(this.VERTShaderID);
			
			if (ARBShaderObjects.glGetObjectParameteriARB(this.VERTShaderID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE){
	            this.isUsable = false;
	            throw new GLException(
	            		"Error compiling vertex shader!\n" +
	            		getLogInfo(this.VERTShaderID) + "\n" +
	            		"##### Vertex Shader #####\n" +
	            		this.SHADER_VERTEX.toString() + "\n"+
	            		"##### ------------- #####\n"
	            );
			}
			GL20.glAttachShader(this.ProgramID, this.VERTShaderID);
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			GL20.glShaderSource(this.FRAGShaderID,this.SHADER_FRAGMENT.toString());
			GL20.glCompileShader(this.FRAGShaderID);
			
			
			if (ARBShaderObjects.glGetObjectParameteriARB(this.FRAGShaderID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE){
	            this.isUsable = false;
	            throw new GLException(
	            		"Error compiling fragment shader!\n" +
	            		getLogInfo(this.FRAGShaderID) + "\n" +
	            		"##### Fragment Shader #####\n" +
	            		this.SHADER_FRAGMENT.toString() + "\n"+
	            		"##### --------------- #####"
	            );
			}
			GL20.glAttachShader(this.ProgramID, this.FRAGShaderID);
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//if(!this.isLinked)
		GL20.glLinkProgram(this.ProgramID);
		
		if (ARBShaderObjects.glGetObjectParameteriARB(this.ProgramID, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE){
	           this.isUsable = false;
	           throw new GLException(
	           		"Error linking program!\n" +
	           		getLogInfo(this.ProgramID) + "\n" +
	           		getLogInfo(this.VERTShaderID) + "\n" +
	           		getLogInfo(this.FRAGShaderID) + "\n" +
	           		"##### Vertex Shader #####\n" +
	           		this.SHADER_VERTEX.toString() + "\n"+
	           		"##### Fragment Shader #####\n" +
	           		this.SHADER_FRAGMENT.toString() + "\n"+
	           		"##### ------------- #####\n"
	           );
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		GL20.glValidateProgram(this.ProgramID);
        if (ARBShaderObjects.glGetObjectParameteriARB(this.ProgramID, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
            this.isUsable = false;
        	throw new GLException("Error validating shader: " + getLogInfo(this.ProgramID));
        }
        
        this.isUsable = true;
		
        this.doNotUseProgram();
	}
	
	/**
	 * Gets the uniform index.
	 *
	 * @param label the label
	 * @return the uniform index
	 */
	public int getUniformIndex(String label){
		if(!this.isUsable)
			return 0;
		return GL20.glGetUniformLocation(this.ProgramID, label);
	}
	
	/**
	 * Do use program.
	 */
	public void doUseProgram(){
		if(this.isUsable)
			GL20.glUseProgram(this.ProgramID);
	}
	
	/**
	 * Do not use program.
	 */
	public void doNotUseProgram(){
		GL20.glUseProgram(0);
	}
	
	/**
	 * Sets the shader source.
	 *
	 * @param ShaderType the shader type
	 * @param newCode the new code
	 */
	public void setShaderSource(int ShaderType,String newCode){
		if(ShaderType == GL20.GL_FRAGMENT_SHADER)
			this.SHADER_FRAGMENT = new StringBuffer(newCode);
		else
			if(ShaderType == GL20.GL_VERTEX_SHADER)
				this.SHADER_VERTEX = new StringBuffer(newCode);
	}

	/**
	 * Load file into buffers.
	 *
	 * @param input the input
	 */
	private final void loadFileIntoBuffers(InputStream input) {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(input);
		int SHADER_ = -1;
		
		while(scanner.hasNextLine()){
			String s = scanner.nextLine();
			
			if(s.isEmpty())
				continue;
			
			if(s.startsWith("//"))
				continue;
			
			if(s.startsWith("#SHADER_")){
				if(s.equals("#SHADER_FRAGMENT")){
					SHADER_ = GL20.GL_FRAGMENT_SHADER;
					
					if(this.SHADER_FRAGMENT == null)
						this.SHADER_FRAGMENT = new StringBuffer("");
				}else if(s.equals("#SHADER_VERTEX")){
					SHADER_ = GL20.GL_VERTEX_SHADER;
					
					if(this.SHADER_VERTEX == null)
						this.SHADER_VERTEX = new StringBuffer("");
				}
				continue;
			}
			if(s.startsWith("#IGNORE")){
				SHADER_ = -1;
			}
			
			if((SHADER_ == GL20.GL_FRAGMENT_SHADER) && (this.SHADER_FRAGMENT != null)){
				this.SHADER_FRAGMENT.append(s + "\n");
				continue;
			}
			
			if((SHADER_ == GL20.GL_VERTEX_SHADER) && (this.SHADER_VERTEX != null)){
				this.SHADER_VERTEX.append(s + "\n");
				continue;
			}
			
			if(SHADER_ == -1){
				continue;
			}
			
			throw new IllegalArgumentException("Unknown Shader Type: " + s);
		}
		try {
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(SHADER_VERTEX == null)
			throw new RuntimeException("SHADER_VERTEX could not be loaded.");
		
		if(SHADER_FRAGMENT == null)
			throw new RuntimeException("SHADER_FRAGMENT could not be loaded.");
		
	}
	
    /**
     * Gets the log info.
     *
     * @param obj the obj
     * @return the log info
     */
    public static final String getLogInfo(int obj) {
    	int errorcode = ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB);
        return ARBShaderObjects.glGetInfoLogARB(obj, errorcode) + "\t\t#" + errorcode;
    }
    
    /**
     * Gets the default shader.
     *
     * @return the default shader
     */
    public static InputStream getDefaultShader(){
    	return GLSLShader.class.getResourceAsStream("defaultPipelineShader.shader");
    }

    
    /**
     * Sets the uniform float.
     *
     * @param label the label
     * @param f the f
     */
    public void setUniformFloat(String label, float f) {
		GL20.glUniform1f(this.getUniformIndex(label),f);
	}

	/**
	 * Sets the uniform vector3f.
	 *
	 * @param label the label
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setUniformVector3f(String label, float x, float y, float z) {
		GL20.glUniform3f(this.getUniformIndex(label),x,y,z);
	}
	
	/**
	 * Sets the uniform int.
	 *
	 * @param uniloc the uniloc
	 * @param i the i
	 */
	public void setUniformInt(int uniloc, int i) {
		GL20.glUniform1i(uniloc, i);
	}
	
	/**
	 * Sets the uniform vector3f.
	 *
	 * @param uniloc the uniloc
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setUniformVector3f(int uniloc, float x, float y, float z) {
		GL20.glUniform3f(uniloc, x, y, z);
	}
	
}
