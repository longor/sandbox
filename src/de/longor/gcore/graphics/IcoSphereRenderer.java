/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.opengl.GL11;

import de.longor1996.util.math.geom3D.IcoSphere;

// TODO: Auto-generated Javadoc
/**
 * The Class IcoSphereRenderer.
 */
public class IcoSphereRenderer
{
	
	/**
	 * Render.
	 *
	 * @param sphere the sphere
	 * @param upscale the upscale
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public static final void render(IcoSphere sphere, float upscale, float r, float g, float b, float a)
	{
		render(sphere, upscale, 0, 0, 0, r, g, b, a);
	}
	
	/**
	 * Render.
	 *
	 * @param sphere the sphere
	 * @param upscale the upscale
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public static final void render(IcoSphere sphere, float upscale, float x, float y, float z, float r, float g, float b, float a)
	{
		
		int[][] faces = sphere.faces$;
		float[][] vertices = sphere.vertices$;
		
		IVertexDrawer va = VertexDrawerVA.drawer;
		
		va.startDrawing(GL11.GL_TRIANGLES, true, false, false);
		va.setOriginTranslation(x, y, z);
		va.setVertexColor(r, g, b, a);
		
		for(int face = 0; face < faces.length; face++)
		{
			int[] faceData = faces[face];
			float[] vertex_a = vertices[faceData[0]];
			float[] vertex_b = vertices[faceData[1]];
			float[] vertex_c = vertices[faceData[2]];
			
			va.vertex((float)vertex_a[0]*upscale, (float)vertex_a[1]*upscale, (float)vertex_a[2]*upscale);
			va.vertex((float)vertex_b[0]*upscale, (float)vertex_b[1]*upscale, (float)vertex_b[2]*upscale);
			va.vertex((float)vertex_c[0]*upscale, (float)vertex_c[1]*upscale, (float)vertex_c[2]*upscale);
		}
		va.stopDrawing();
	}
	
	
}
