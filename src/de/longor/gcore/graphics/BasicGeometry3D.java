/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.opengl.GL11;

import de.longor1996.util.math.Vector3F;
import de.longor1996.util.math.Vector4F;
import de.longor1996.util.math.geom3D.IcoSphere;


// TODO: Auto-generated Javadoc
/**
 * This class contains methods to draw basic geometry.
 **/
public class BasicGeometry3D
{
	
	/** The Constant unitSphereOutfacing. */
	private static final IcoSphere unitSphereOutfacing;
	
	/** The Constant unitSphereInfacing. */
	private static final IcoSphere unitSphereInfacing;
	
	/** The tex mul. */
	private static float texMul = 1f;
	static
	{
		unitSphereOutfacing = new IcoSphere(2, true);
		unitSphereInfacing = new IcoSphere(2, false);
	}
	
	/**
	 * Draws a sphere made of TRIANGLES, with the given size and face-facing.
	 * @param va The vertexDrawer to output the mesh to.
	 * @param radius The radius of the sphere.
	 * @param outfacing The direction the triangles are facing in. <i>True = Outward, False = Inward</i>
	 **/
	public static final void drawSphere(IVertexDrawer va, float radius, boolean outfacing)
	{
		final IcoSphere sphere = outfacing ? unitSphereOutfacing : unitSphereInfacing;
		final int[][] faces = sphere.faces$;
		final float[][] vertices = sphere.vertices$;
		
		for(int face = 0; face < faces.length; face++)
		{
			int[] faceData = faces[face];
			float[] vertex_a = vertices[faceData[0]];
			float[] vertex_b = vertices[faceData[1]];
			float[] vertex_c = vertices[faceData[2]];
			
			float[] normal_a = vertices[faceData[0]];
			float[] normal_b = vertices[faceData[1]];
			float[] normal_c = vertices[faceData[2]];
			
			va.setVertexUV(0, 0, 0);
			va.setVertexNormal(normal_a[0], normal_a[1], normal_a[2]);
			va.vertex((float)vertex_a[0]*radius, (float)vertex_a[1]*radius, (float)vertex_a[2]*radius);
			va.setVertexUV(0, 1, 0);
			va.setVertexNormal(normal_b[0], normal_b[1], normal_b[2]);
			va.vertex((float)vertex_b[0]*radius, (float)vertex_b[1]*radius, (float)vertex_b[2]*radius);
			va.setVertexUV(0, 0, 1);
			va.setVertexNormal(normal_c[0], normal_c[1], normal_c[2]);
			va.vertex((float)vertex_c[0]*radius, (float)vertex_c[1]*radius, (float)vertex_c[2]*radius);
		}
	}
	
	/**
	 * Draws a box made of LINES with the given size.
	 * @param va The vertexDrawer to output the mesh to.
	 * @param width The width of the box.
	 * @param height The height of the box.
	 * @param length The length of the box.
	 * @param centered If the box is centered around (0,0,0) or not.
	 **/
	public static final void drawWireframeBox(IVertexDrawer va, float width, float height, float length, boolean centered)
	{
		if(centered)
		{
			width /= 2F;
			height /= 2F;
			length /= 2F;
			drawWireframeBox(va, -width, -height, -length, +width, +height, +length);
		}
		else
		{
			drawWireframeBox(va, 0, 0, 0, width, height, length);
		}
	}
	
	/**
	 * Draws a Axis-Aligned Bounding-Box made of LINES.
	 * @param va The vertexDrawer to output the mesh to.
	 * @param x0 Minimum X
	 * @param y0 Minimum Y
	 * @param z0 Minimum Z
	 * @param x1 Maximum X
	 * @param y1 Maximum Y
	 * @param z1 Maximum Z
	 **/
	public static final void drawWireframeBox(IVertexDrawer va, float x0, float y0, float z0, float x1, float y1, float z1)
	{
		va.vertex(x0, y0, z0);
		va.vertex(x1, y0, z0);
		
		va.vertex(x0, y0, z0);
		va.vertex(x0, y1, z0);
		
		va.vertex(x0, y1, z0);
		va.vertex(x1, y1, z0);
		
		va.vertex(x1, y0, z0);
		va.vertex(x1, y1, z0);
		
		va.vertex(x0, y0, z0);
		va.vertex(x0, y0, z1);
		
		va.vertex(x0, y1, z0);
		va.vertex(x0, y1, z1);
		
		va.vertex(x1, y0, z0);
		va.vertex(x1, y0, z1);
		
		va.vertex(x1, y1, z0);
		va.vertex(x1, y1, z1);
		
		va.vertex(x0, y0, z1);
		va.vertex(x0, y1, z1);
		
		va.vertex(x0, y1, z1);
		va.vertex(x1, y1, z1);
		
		va.vertex(x1, y0, z1);
		va.vertex(x1, y1, z1);
		
		va.vertex(x0, y0, z1);
		va.vertex(x1, y0, z1);
	}
	
	/**
	 * Draws a circle made of LINES centered around (0,0,0). UNTESTED!
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param radius The radius of the ring.
	 * @param segments The amount of segments the circle should consist of.
	 * @param isXY If true, the ring will XY, if false the ring will be XZ.
	 **/
	public static final void drawCircle(IVertexDrawer va, float radius, int segments, boolean isXY)
	{
		float segmentsF = segments;
		float segmentsDivider = 1F / segmentsF;
		float twoPI = 2.0f * 3.1415926f;
		float twoPImulSegDivide = twoPI * segmentsDivider;
		
		float theta = 0;
		float ThetaCos = 0;
		float ThetaSin = 0;
		float x = 0;
		float yz = 0;
		
		for(int ii = 0; ii < segments+1; ii++)
		{
			theta = twoPImulSegDivide * ii;//get the current angle
			
			// could use a lookup-table here...
			ThetaCos = (float) Math.cos(theta);
			ThetaSin = (float) Math.sin(theta);
			
			x = (radius * ThetaCos);//calculate the x component
			yz = (radius * ThetaSin);//calculate the y component
			
			if(isXY)
				va.vertex(x,yz,0);
			else
				va.vertex(x,0,yz);
		}
	}
	
	/**
	 * Draws a ring made of a QUAD_STRIP centered around (0,0,0).
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param innerRadius The inner radius of the ring.
	 * @param outerRadius Radius The outer radius of the ring.
	 * @param num_segments How many segments the ring should have.
	 * @param isXY If true, the ring will XY, if false the ring will be XZ.
	 **/
	public static final void drawRing(IVertexDrawer va, float innerRadius, float outerRadius, int num_segments, boolean isXY)
	{
		float r = outerRadius;
		float r2 = innerRadius;
		float num_segmentsF = num_segments;
		float num_segmentsDivider = 1F / num_segmentsF;
		float twoPI = 2.0f * 3.1415926f;
		float twoPImulSegDivide = twoPI * num_segmentsDivider;
		
		for(int ii = 0; ii < num_segments+1; ii++)
		{ 
			float theta = twoPImulSegDivide * ii;//get the current angle 
			float tex_x = ((float)ii) * num_segmentsDivider;
			
			float ThetaCos = (float) Math.cos(theta);
			float ThetaSin = (float) Math.sin(theta);
			float xO = (float) (r * ThetaCos);//calculate the x component
			float yO = (float) (r * ThetaSin);//calculate the y component
			float xI = (float) (r2 * ThetaCos);//calculate the x component
			float yI = (float) (r2 * ThetaSin);//calculate the y component
			
			if(isXY)
			{
				//Outer Point
				va.setVertexUV(0, tex_x,1);
				va.vertex(xO,-yO,0);
				
				//Inner Point
				va.setVertexUV(0, tex_x,0);
				va.vertex(xI,-yI,0);
			}
			else // XZ
			{
				//Outer Point
				va.setVertexUV(0, tex_x,1);
				va.vertex(xO,0,yO);
				
				//Inner Point
				va.setVertexUV(0, tex_x,0);
				va.vertex(xI,0,-yI);
			}
		}
	}
	
	/**
	 * Draws a origin-marker made of LINES.
	 * <br>
	 * If the size is bigger or equal than 1024, segments will be set to <i>S = SIZE / 16</i>;
	 * If segments is smaller or equal than 1, the lines will be drawn directly, without segmentation.
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param size The size of the marker.
	 * @param segments How many line segments should be drawn.
	 **/
	public static final void drawOriginMarker(IVertexDrawer va, float size, int segments)
	{
		if(size >= 1024)
		{
			segments = (int) (size / 16);
		}
		
		if(segments <= 1)
		{
			// GREEN X
			va.setVertexColor(0,1,0,1);
			va.vertex(0, 0, 0);
			va.vertex(size, 0, 0);
			
			// BLUE Y
			va.setVertexColor(0,0,1,1);
			va.vertex(0, 0, 0);
			va.vertex(0, size, 0);
			
			// RED Z
			va.setVertexColor(1,0,0,1);
			va.vertex(0, 0, 0);
			va.vertex(0, 0, size);
		}
		else
		{
			// GREEN X
			va.setVertexColor(0,1,0,1);
			drawLineInSegments(va, 0,0,0,  size,0,0, segments);
			
			// BLUE Y
			va.setVertexColor(0,0,1,1);
			drawLineInSegments(va, 0,0,0,  0,size,0, segments);
			
			// RED Z
			va.setVertexColor(1,0,0,1);
			drawLineInSegments(va, 0,0,0,  0,0,size, segments);
		}
	}
	
	/**
	 * Draws a Grid with a given size and scale.
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param cellCount How many 'cells' the grid should have in each direction (North East South West).
	 * @param cellSize How big a single 'cell' is. (The scale of the grid)
	 **/
	public static final void drawGrid(IVertexDrawer va, int cellCount, float cellSize)
	{
		int sx = -cellCount;
		int sz = -cellCount;
		int ex = +cellCount;
		int ez = +cellCount;
		
		float sxF = sx * cellSize;
		float szF = sz * cellSize;
		float exF = ex * cellSize;
		float ezF = ez * cellSize;
		
		float xF = cellSize;
		float zF = cellSize;
		
		// Draw X lines
		for(int z = sz; z <= ez; z++)
		{
			zF = z * cellSize;
			va.vertex(sxF, 0, zF);
			va.vertex(exF, 0, zF);
		}
		
		// Draw Z lines
		for(int x = sx; x <= ex; x++)
		{
			xF = x * cellSize;
			va.vertex(xF, 0, szF);
			va.vertex(xF, 0, ezF);
		}
	}
	
	/**
	 * Draws a line made of segments (which are LINES).
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param ax The x start position of the line.
	 * @param ay The y start position of the line.
	 * @param az The z start position of the line.
	 * @param bx The x end position of the line.
	 * @param by The y end position of the line.
	 * @param bz The z end position of the line.
	 * @param n How many segments the line should be split into.
	 **/
	public static final void drawLineInSegments(IVertexDrawer va,
			float ax, float ay, float az,
			float bx, float by, float bz,
			int n)
	{
		final float nInv = 1F / (float)n;
		
		float interpolate = 0;
		
		float x;
		float y;
		float z;
		
		for(int i = 0; i < n; i++)
		{
			if((i+1) < n)
			{
				interpolate = i;
				interpolate *= nInv;
				x = ax + (interpolate * (bx - ax));
				y = ay + (interpolate * (by - ay));
				z = az + (interpolate * (bz - az));
				va.vertex(x, y, z);
				
				interpolate = i+1;
				interpolate *= nInv;
				x = ax + (interpolate * (bx - ax));
				y = ay + (interpolate * (by - ay));
				z = az + (interpolate * (bz - az));
				va.vertex(x, y, z);
			}
		}
	}
	
	/**
	 * Draws a line (LINES). UNTESTED!
	 * 
	 * @param va The vertexDrawer to output the mesh to.
	 * @param x ?
	 * @param y ?
	 * @param z ?
	 * @param closed If the line should be 'closed'.
	 **/
	public static final void drawLine(IVertexDrawer va, float[] x, float[] y, float[] z, boolean closed)
	{
		if(x.length != y.length && x.length != z.length)
		{
			throw new IllegalArgumentException("Line-Arrays are not of equal length.");
		}
		
		int length = closed ? (x.length) : (x.length-1);
		int lengthM1 = x.length - 1;
		
		float x0, y0, z0, x1, y1, z1;
		
		for(int i = 0, i2 = 0; i < length; i++, i2 = (i+1)%lengthM1)
		{
			x0 = x[i];
			x1 = x[i2];
			y0 = y[i];
			y1 = y[i2];
			z0 = z[i];
			z1 = z[i2];
			
			va.vertex(x0, y0, z0);
			va.vertex(x1, y1, z1);
		}
	}
	
	/**
	 * Draw textured box.
	 *
	 * @param va the va
	 * @param width the width
	 * @param height the height
	 * @param length the length
	 * @param centered the centered
	 */
	public static void drawTexturedBox(IVertexDrawer va, float width, float height, float length, boolean centered)
	{
		float minX = 0;
		float minY = 0;
		float minZ = 0;
		float maxX = width;
		float maxY = height;
		float maxZ = length;
		
		float W = width * texMul; //MathUtility.round(width, 2f);
		float H = height * texMul; //MathUtility.round(height, 2f);
		float L = length * texMul; //MathUtility.round(length, 2f);
		
		if(centered)
		{
			float w2 = width / 2F;
			float h2 = height / 2F;
			float l2 = length / 2F;
			minX -= w2;
			minY -= h2;
			minZ -= l2;
			maxX -= w2;
			maxY -= h2;
			maxZ -= l2;
		}
		
		// Top
		va.setVertexNormal(0, 1, 0);
		va.setVertexUV(0, 0, 0); va.vertex(minX, maxY, maxZ);
		va.setVertexUV(0, W, 0); va.vertex(maxX, maxY, maxZ);
		va.setVertexUV(0, W, L); va.vertex(maxX, maxY, minZ);
		va.setVertexUV(0, 0, L); va.vertex(minX, maxY, minZ);
		
		// Bottom
		va.setVertexNormal(0, -1, 0);
		va.setVertexUV(0, 0, 0); va.vertex(minX, minY, minZ);
		va.setVertexUV(0, W, 0); va.vertex(maxX, minY, minZ);
		va.setVertexUV(0, W, L); va.vertex(maxX, minY, maxZ);
		va.setVertexUV(0, 0, L); va.vertex(minX, minY, maxZ);
		
		// Left
		va.setVertexNormal(-1, 0, 0);
		va.setVertexUV(0, 0, 0); va.vertex(minX, maxY, maxZ);
		va.setVertexUV(0, L, 0); va.vertex(minX, maxY, minZ);
		va.setVertexUV(0, L, H); va.vertex(minX, minY, minZ);
		va.setVertexUV(0, 0, H); va.vertex(minX, minY, maxZ);
		
		// Right
		va.setVertexNormal(1, 0, 0);
		va.setVertexUV(0, 0, 0); va.vertex(maxX, maxY, minZ);
		va.setVertexUV(0, L, 0); va.vertex(maxX, maxY, maxZ);
		va.setVertexUV(0, L, H); va.vertex(maxX, minY, maxZ);
		va.setVertexUV(0, 0, H); va.vertex(maxX, minY, minZ);
		
		// Front
		va.setVertexNormal(0, 0, 1);
		va.setVertexUV(0, 0, 0); va.vertex(maxX, maxY, maxZ);
		va.setVertexUV(0, W, 0); va.vertex(minX, maxY, maxZ);
		va.setVertexUV(0, W, H); va.vertex(minX, minY, maxZ);
		va.setVertexUV(0, 0, H); va.vertex(maxX, minY, maxZ);
		
		// Back
		va.setVertexNormal(0, 0, -1);
		va.setVertexUV(0, 0, 0); va.vertex(minX, maxY, minZ);
		va.setVertexUV(0, W, 0); va.vertex(maxX, maxY, minZ);
		va.setVertexUV(0, W, H); va.vertex(maxX, minY, minZ);
		va.setVertexUV(0, 0, H); va.vertex(minX, minY, minZ);
		
	}
	
	
	
	/**
	 * Do render cone base.
	 *
	 * @param va the va
	 * @param Radius the radius
	 * @param num_segments the num_segments
	 * @param depthP the depth p
	 * @param depthN the depth n
	 * @param PEAK_COLOR the peak color
	 * @param CORNER_COLOR the corner color
	 * @param offset the offset
	 * @param flip the flip
	 */
	public static final void doRenderConeBase(IVertexDrawer va,
			float Radius, int num_segments,
			float depthP, float depthN,
			Vector4F PEAK_COLOR,
			Vector4F CORNER_COLOR,
			Vector3F offset,
			boolean flip){
		
		va.startDrawing(GL11.GL_QUAD_STRIP, true, true, false);
		va.setOriginTranslation(offset.x, offset.y, offset.z);
		float r = (float) Radius;
		float num_segmentsf = num_segments;
		
		for(int ii = 0; ii < (num_segments+1); ii++)
		{
			float theta = (2.0f * 3.1415926f * ii) / num_segments;//get the current angle
			float tex_x = (ii) / num_segmentsf;
			
			float ThetaCos = (float) Math.cos(theta);
			float ThetaSin = (float) Math.sin(theta);
			float xO = r * ThetaCos;//calculate the x component
			float yO = r * ThetaSin;//calculate the y component
			
			if(flip)
			{
				//Inner Point
				va.setVertexColor(PEAK_COLOR.x, PEAK_COLOR.y, PEAK_COLOR.z, PEAK_COLOR.w);
				va.setVertexUV(0, tex_x,0);
				va.vertex(0,-depthN,0);
				
				//Outer Point
				va.setVertexColor(CORNER_COLOR.x, CORNER_COLOR.y, CORNER_COLOR.z, CORNER_COLOR.w);
				va.setVertexUV(0, tex_x,1);
				va.vertex(xO,depthP,-yO);
			}
			else
			{
				//Outer Point
				va.setVertexColor(CORNER_COLOR.x, CORNER_COLOR.y, CORNER_COLOR.z, CORNER_COLOR.w);
				va.setVertexUV(0, tex_x,1);
				va.vertex(xO,depthP,-yO);
				
				//Inner Point
				va.setVertexColor(PEAK_COLOR.x, PEAK_COLOR.y, PEAK_COLOR.z, PEAK_COLOR.w);
				va.setVertexUV(0, tex_x,0);
				va.vertex(0,-depthN,0);
			}
		}
		
		va.stopDrawing();
	}

	/**
	 * Sets the texture multiplier.
	 *
	 * @param f the new texture multiplier
	 */
	public static void setTextureMultiplier(float f)
	{
		texMul = f;
	}
	
}
