/*
 * 
 */
package de.longor.gcore.graphics;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;

import de.longor1996.util.ByteBufferUtil;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class MatrixStack.
 */
public class MatrixStack
{
	
	/** The matrix buffer. */
	private final FloatBuffer matrixBuffer;
	
	/** The stack. */
	private final Matrix4F[] stack;
	
	/** The stack pointer. */
	private int stackPointer;
	
	/** The is dirty. */
	private boolean isDirty;
	
	/**
	 * Instantiates a new matrix stack.
	 */
	public MatrixStack()
	{
		this(8);
	}
	
	/**
	 * Instantiates a new matrix stack.
	 *
	 * @param size the size
	 */
	public MatrixStack(int size)
	{
		this.matrixBuffer = ByteBufferUtil.offheapNativeOrderedFloat(16);
		this.stack = Matrix4F.newInstance(size);
		this.stackPointer = 0;
		this.isDirty = true;
	}
	
	/**
	 * Sets the current Matrix to an identity Matrix.
	 **/
	public void glSetIdentity()
	{
		this.stack[this.stackPointer].setIdentity();
		this.isDirty = true;
	}
	
	/**
	 * Gl push matrix.
	 */
	public void glPushMatrix()
	{
		this.stackPointer++;
		
		if(this.stackPointer > this.stack.length)
		{
			throw new IllegalStateException("stackPointer is higher than stack.length!");
		}
		
		this.stack[this.stackPointer].setFromMatrix(this.stack[this.stackPointer-1]);
		this.isDirty = true;
	}
	
	/**
	 * Gl pop matrix.
	 */
	public void glPopMatrix()
	{
		this.stackPointer--;
		
		if(this.stackPointer < 0)
		{
			throw new IllegalStateException("stackPointer is smaller than zero!");
		}
		
		this.isDirty = true;
	}
	
	/**
	 * Gl translate.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void glTranslate(float x, float y, float z)
	{
		Matrix4F mat = this.stack[this.stackPointer];
		Matrix4F.translate(x, y, z, mat, mat);
		this.isDirty = true;
	}
	
	/**
	 * Gl rotate.
	 *
	 * @param theta the theta
	 * @param X the x
	 * @param Y the y
	 * @param Z the z
	 */
	public void glRotate(float theta, float X, float Y, float Z)
	{
		Matrix4F mat = this.stack[this.stackPointer];
		Matrix4F.rotateAroundAxisByDegrees(theta, X, Y, Z, mat, mat);
		this.isDirty = true;
	}
	
	/**
	 * Gl scale.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void glScale(float x, float y, float z)
	{
		Matrix4F mat = this.stack[this.stackPointer];
		Matrix4F.scale(x, y, z, mat, mat);
		this.isDirty = true;
	}
	
	/**
	 * MAT = mat__ * __mat;.
	 *
	 * @param __mat the __mat
	 */
	public void glMultiply(Matrix4F __mat)
	{
		Matrix4F mat__ = this.stack[this.stackPointer];
		Matrix4F.mul(mat__, __mat, mat__);
		this.isDirty = true;
	}
	
	/**
	 * MAT = __mat * mat__;.
	 *
	 * @param mat__ the mat__
	 */
	public void glMultiplyInv(Matrix4F mat__)
	{
		Matrix4F __mat = this.stack[this.stackPointer];
		Matrix4F.mul(__mat, mat__, __mat);
		this.isDirty = true;
	}
	
	/**
	 * Reset stack.
	 */
	public void resetStack()
	{
		for(Matrix4F matrix : this.stack)
		{
			matrix.setIdentity();
		}
		this.stackPointer = 0;
		this.isDirty = true;
	}
	
	/**
	 * Gets the matrix buffer.
	 *
	 * @return the matrix buffer
	 */
	public FloatBuffer getMatrixBuffer()
	{
		if(this.isDirty)
		{
			this.isDirty = false;
			this.matrixBuffer.clear();
			this.stack[this.stackPointer].putMatrixIntoFloatBuffer(this.matrixBuffer);
		}
		
		this.matrixBuffer.clear();
		return this.matrixBuffer;
	}
	
	/**
	 * Gets the matrix.
	 *
	 * @return the matrix
	 */
	public Matrix4F getMatrix()
	{
		return this.stack[this.stackPointer];
	}
	
	/**
	 * Upload as projection matrix.
	 */
	public void uploadAsProjectionMatrix()
	{
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadMatrix(this.getMatrixBuffer());
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}
	
	/**
	 * Upload as model view matrix.
	 */
	public void uploadAsModelViewMatrix()
	{
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadMatrix(this.getMatrixBuffer());
	}
	
	/**
	 * Gl load matrix.
	 *
	 * @param matrix the matrix
	 */
	public void glLoadMatrix(Matrix4F matrix)
	{
		this.stack[this.stackPointer].setFromMatrix(matrix);
		this.isDirty = true;
	}
	
}
