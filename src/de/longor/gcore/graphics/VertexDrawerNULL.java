/*
 * 
 */
package de.longor.gcore.graphics;

// TODO: Auto-generated Javadoc
/**
 * The Class VertexDrawerNULL.
 */
public class VertexDrawerNULL implements IVertexDrawer
{
	
	/** The Constant instance. */
	public static final VertexDrawerNULL instance = new VertexDrawerNULL();
	
	/** The is drawing. */
	private boolean isDrawing = false;
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setOriginTranslation(float, float, float)
	 */
	@Override
	public void setOriginTranslation(float x, float y, float z) {}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexColor(float, float, float, float)
	 */
	@Override
	public void setVertexColor(float r, float g, float b, float a) {}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexNormal(float, float, float)
	 */
	@Override
	public void setVertexNormal(float x, float y, float z) {}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexUV(int, float, float)
	 */
	@Override
	public void setVertexUV(int texUnit, float s, float t) {}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#vertex(float, float, float)
	 */
	@Override
	public void vertex(float x, float y, float z) {}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#startDrawing(int, boolean, boolean, boolean)
	 */
	@Override
	public void startDrawing(int mode, boolean color, boolean texture, boolean normals)
	{
		this.isDrawing = true;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#stopDrawing()
	 */
	@Override
	public void stopDrawing()
	{
		this.isDrawing = false;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#isDrawing()
	 */
	@Override
	public boolean isDrawing()
	{
		return this.isDrawing;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#hasDrawenAnyVertices()
	 */
	@Override
	public boolean hasDrawenAnyVertices()
	{
		return false;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#getCurrentVertexCount()
	 */
	@Override
	public int getCurrentVertexCount()
	{
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#remainingUntilFailure()
	 */
	@Override
	public int remainingUntilFailure()
	{
		return 0;
	}
}
