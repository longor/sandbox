/*
 * 
 */
package de.longor.gcore.graphics.font;

import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.VertexDrawerNULL;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor.gcore.graphics.textures.ITexture;
import de.longor.gcore.graphics.textures.TextureHelper;
import de.longor.gcore.graphics.textures.TextureUploader;
import de.longor1996.util.BinaryUtil;
import de.longor1996.util.CharSequenceStream;
import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.geom2D.RectangleI;

// TODO: Auto-generated Javadoc
/**
 * The Class FontRenderer.
 */
public class FontRenderer
{

	/** The Constant vgaPallet. */
	public static final float[] vgaPallet = new float[]
	{
		0.0F,0.0F,0.0F, // BLACK 0x00
		0.0F,0.0F,0.6666667F, // DARK BLUE 0x01
		0.0F,0.6666667F,0.0F, // DARK GREEN 0x02
		0.0F,0.6666667F,0.6666667F, // DARK CYAN 0x03
		0.6666667F,0.0F,0.0F, // DARK RED 0x04
		0.6666667F,0.0F,0.6666667F, // DARK PURPLE 0x05
		0.6666667F,0.33333334F,0.0F, // 0x06
		0.6666667F,0.6666667F,0.6666667F, // GRAY 0x07
		0.33333334F,0.33333334F,0.33333334F, // DARK GREY 0x08
		0.24313726F,0.24313726F,1.0F, // BRIGHT BLUE 0x09
		0.0F,1.0F,0.0F, // GREEN 0x0A
		0.0F,1.0F,1.0F, // CYAN  0x0B
		1.0F,0.09803922F,0.09803922F, // RED 0x0C
		1.0F,0.003921569F,1.0F,
		1.0F,1.0F,0.0F, // YELLOW  0x0E
		1.0F,1.0F,1.0F, // WHITE 0x0F
		0.0F,0.0F,0.0F,
		0.0627451F,0.0627451F,0.0627451F,
		0.1254902F,0.1254902F,0.1254902F,
		0.20784314F,0.20784314F,0.20784314F,
		0.27058825F,0.27058825F,0.27058825F,
		0.33333334F,0.33333334F,0.33333334F,
		0.39607844F,0.39607844F,0.39607844F,
		0.45882353F,0.45882353F,0.45882353F,
		0.5411765F,0.5411765F,0.5411765F,
		0.6039216F,0.6039216F,0.6039216F,
		0.6666667F,0.6666667F,0.6666667F,
		0.7294118F,0.7294118F,0.7294118F,
		0.7921569F,0.7921569F,0.7921569F,
		0.8745098F,0.8745098F,0.8745098F,
		0.9372549F,0.9372549F,0.9372549F,
		1.0F,1.0F,1.0F,
		0.0F,0.0F,1.0F,
		0.25490198F,0.0F,1.0F,
		0.50980395F,0.0F,1.0F,
		0.74509805F,0.0F,1.0F,
		1.0F,0.0F,1.0F,
		1.0F,0.0F,0.74509805F,
		1.0F,0.0F,0.50980395F,
		1.0F,0.0F,0.25490198F,
		1.0F,0.0F,0.0F,
		1.0F,0.25490198F,0.0F,
		1.0F,0.50980395F,0.0F,
		1.0F,0.74509805F,0.0F,
		1.0F,1.0F,0.0F,
		0.74509805F,1.0F,0.0F,
		0.50980395F,1.0F,0.0F,
		0.25490198F,1.0F,0.0F,
		0.0F,1.0F,0.0F,
		0.0F,1.0F,0.25490198F,
		0.0F,1.0F,0.50980395F,
		0.0F,1.0F,0.74509805F,
		0.0F,1.0F,1.0F,
		0.0F,0.74509805F,1.0F,
		0.0F,0.50980395F,1.0F,
		0.0F,0.25490198F,1.0F,
		0.50980395F,0.50980395F,1.0F,
		0.61960787F,0.50980395F,1.0F,
		0.74509805F,0.50980395F,1.0F,
		0.8745098F,0.50980395F,1.0F,
		1.0F,0.50980395F,1.0F,
		1.0F,0.50980395F,0.8745098F,
		1.0F,0.50980395F,0.74509805F,
		1.0F,0.50980395F,0.61960787F,
		1.0F,0.50980395F,0.50980395F,
		1.0F,0.61960787F,0.50980395F,
		1.0F,0.74509805F,0.50980395F,
		1.0F,0.8745098F,0.50980395F,
		1.0F,1.0F,0.50980395F,
		0.8745098F,1.0F,0.50980395F,
		0.74509805F,1.0F,0.50980395F,
		0.61960787F,1.0F,0.50980395F,
		0.50980395F,1.0F,0.50980395F,
		0.50980395F,1.0F,0.61960787F,
		0.50980395F,1.0F,0.74509805F,
		0.50980395F,1.0F,0.8745098F,
		0.50980395F,1.0F,1.0F,
		0.50980395F,0.8745098F,1.0F,
		0.50980395F,0.74509805F,1.0F,
		0.50980395F,0.61960787F,1.0F,
		0.7294118F,0.7294118F,1.0F,
		0.7921569F,0.7294118F,1.0F,
		0.8745098F,0.7294118F,1.0F,
		0.9372549F,0.7294118F,1.0F,
		1.0F,0.7294118F,1.0F,
		1.0F,0.7294118F,0.9372549F,
		1.0F,0.7294118F,0.8745098F,
		1.0F,0.7294118F,0.7921569F,
		1.0F,0.7294118F,0.7294118F,
		1.0F,0.7921569F,0.7294118F,
		1.0F,0.8745098F,0.7294118F,
		1.0F,0.9372549F,0.7294118F,
		1.0F,1.0F,0.7294118F,
		0.9372549F,1.0F,0.7294118F,
		0.8745098F,1.0F,0.7294118F,
		0.7921569F,1.0F,0.7294118F,
		0.7294118F,1.0F,0.7294118F,
		0.7294118F,1.0F,0.7921569F,
		0.7294118F,1.0F,0.8745098F,
		0.7294118F,1.0F,0.9372549F,
		0.7294118F,1.0F,1.0F,
		0.7294118F,0.9372549F,1.0F,
		0.7294118F,0.8745098F,1.0F,
		0.7294118F,0.7921569F,1.0F,
		0.0F,0.0F,0.44313726F,
		0.10980392F,0.0F,0.44313726F,
		0.22352941F,0.0F,0.44313726F,
		0.33333334F,0.0F,0.44313726F,
		0.44313726F,0.0F,0.44313726F,
		0.44313726F,0.0F,0.33333334F,
		0.44313726F,0.0F,0.22352941F,
		0.44313726F,0.0F,0.10980392F,
		0.44313726F,0.0F,0.0F,
		0.44313726F,0.10980392F,0.0F,
		0.44313726F,0.22352941F,0.0F,
		0.44313726F,0.33333334F,0.0F,
		0.44313726F,0.44313726F,0.0F,
		0.33333334F,0.44313726F,0.0F,
		0.22352941F,0.44313726F,0.0F,
		0.10980392F,0.44313726F,0.0F,
		0.0F,0.44313726F,0.0F,
		0.0F,0.44313726F,0.10980392F,
		0.0F,0.44313726F,0.22352941F,
		0.0F,0.44313726F,0.33333334F,
		0.0F,0.44313726F,0.44313726F,
		0.0F,0.33333334F,0.44313726F,
		0.0F,0.22352941F,0.44313726F,
		0.0F,0.10980392F,0.44313726F,
		0.22352941F,0.22352941F,0.44313726F,
		0.27058825F,0.22352941F,0.44313726F,
		0.33333334F,0.22352941F,0.44313726F,
		0.38039216F,0.22352941F,0.44313726F,
		0.44313726F,0.22352941F,0.44313726F,
		0.44313726F,0.22352941F,0.38039216F,
		0.44313726F,0.22352941F,0.33333334F,
		0.44313726F,0.22352941F,0.27058825F,
		0.44313726F,0.22352941F,0.22352941F,
		0.44313726F,0.27058825F,0.22352941F,
		0.44313726F,0.33333334F,0.22352941F,
		0.44313726F,0.38039216F,0.22352941F,
		0.44313726F,0.44313726F,0.22352941F,
		0.38039216F,0.44313726F,0.22352941F,
		0.33333334F,0.44313726F,0.22352941F,
		0.27058825F,0.44313726F,0.22352941F,
		0.22352941F,0.44313726F,0.22352941F,
		0.22352941F,0.44313726F,0.27058825F,
		0.22352941F,0.44313726F,0.33333334F,
		0.22352941F,0.44313726F,0.38039216F,
		0.22352941F,0.44313726F,0.44313726F,
		0.22352941F,0.38039216F,0.44313726F,
		0.22352941F,0.33333334F,0.44313726F,
		0.22352941F,0.27058825F,0.44313726F,
		0.31764707F,0.31764707F,0.44313726F,
		0.34901962F,0.31764707F,0.44313726F,
		0.38039216F,0.31764707F,0.44313726F,
		0.4117647F,0.31764707F,0.44313726F,
		0.44313726F,0.31764707F,0.44313726F,
		0.44313726F,0.31764707F,0.4117647F,
		0.44313726F,0.31764707F,0.38039216F,
		0.44313726F,0.31764707F,0.34901962F,
		0.44313726F,0.31764707F,0.31764707F,
		0.44313726F,0.34901962F,0.31764707F,
		0.44313726F,0.38039216F,0.31764707F,
		0.44313726F,0.4117647F,0.31764707F,
		0.44313726F,0.44313726F,0.31764707F,
		0.4117647F,0.44313726F,0.31764707F,
		0.38039216F,0.44313726F,0.31764707F,
		0.34901962F,0.44313726F,0.31764707F,
		0.31764707F,0.44313726F,0.31764707F,
		0.31764707F,0.44313726F,0.34901962F,
		0.31764707F,0.44313726F,0.38039216F,
		0.31764707F,0.44313726F,0.4117647F,
		0.31764707F,0.44313726F,0.44313726F,
		0.31764707F,0.4117647F,0.44313726F,
		0.31764707F,0.38039216F,0.44313726F,
		0.31764707F,0.34901962F,0.44313726F,
		0.0F,0.0F,0.25490198F,
		0.0627451F,0.0F,0.25490198F,
		0.1254902F,0.0F,0.25490198F,
		0.19215687F,0.0F,0.25490198F,
		0.25490198F,0.0F,0.25490198F,
		0.25490198F,0.0F,0.19215687F,
		0.25490198F,0.0F,0.1254902F,
		0.25490198F,0.0F,0.0627451F,
		0.25490198F,0.0F,0.0F,
		0.25490198F,0.0627451F,0.0F,
		0.25490198F,0.1254902F,0.0F,
		0.25490198F,0.19215687F,0.0F,
		0.25490198F,0.25490198F,0.0F,
		0.19215687F,0.25490198F,0.0F,
		0.1254902F,0.25490198F,0.0F,
		0.0627451F,0.25490198F,0.0F,
		0.0F,0.25490198F,0.0F,
		0.0F,0.25490198F,0.0627451F,
		0.0F,0.25490198F,0.1254902F,
		0.0F,0.25490198F,0.19215687F,
		0.0F,0.25490198F,0.25490198F,
		0.0F,0.19215687F,0.25490198F,
		0.0F,0.1254902F,0.25490198F,
		0.0F,0.0627451F,0.25490198F,
		0.1254902F,0.1254902F,0.25490198F,
		0.15686275F,0.1254902F,0.25490198F,
		0.19215687F,0.1254902F,0.25490198F,
		0.22352941F,0.1254902F,0.25490198F,
		0.25490198F,0.1254902F,0.25490198F,
		0.25490198F,0.1254902F,0.22352941F,
		0.25490198F,0.1254902F,0.19215687F,
		0.25490198F,0.1254902F,0.15686275F,
		0.25490198F,0.1254902F,0.1254902F,
		0.25490198F,0.15686275F,0.1254902F,
		0.25490198F,0.19215687F,0.1254902F,
		0.25490198F,0.22352941F,0.1254902F,
		0.25490198F,0.25490198F,0.1254902F,
		0.22352941F,0.25490198F,0.1254902F,
		0.19215687F,0.25490198F,0.1254902F,
		0.15686275F,0.25490198F,0.1254902F,
		0.1254902F,0.25490198F,0.1254902F,
		0.1254902F,0.25490198F,0.15686275F,
		0.1254902F,0.25490198F,0.19215687F,
		0.1254902F,0.25490198F,0.22352941F,
		0.1254902F,0.25490198F,0.25490198F,
		0.1254902F,0.22352941F,0.25490198F,
		0.1254902F,0.19215687F,0.25490198F,
		0.1254902F,0.15686275F,0.25490198F,
		0.1764706F,0.1764706F,0.25490198F,
		0.19215687F,0.1764706F,0.25490198F,
		0.20784314F,0.1764706F,0.25490198F,
		0.23921569F,0.1764706F,0.25490198F,
		0.25490198F,0.1764706F,0.25490198F,
		0.25490198F,0.1764706F,0.23921569F,
		0.25490198F,0.1764706F,0.20784314F,
		0.25490198F,0.1764706F,0.19215687F,
		0.25490198F,0.1764706F,0.1764706F,
		0.25490198F,0.19215687F,0.1764706F,
		0.25490198F,0.20784314F,0.1764706F,
		0.25490198F,0.23921569F,0.1764706F,
		0.25490198F,0.25490198F,0.1764706F,
		0.23921569F,0.25490198F,0.1764706F,
		0.20784314F,0.25490198F,0.1764706F,
		0.19215687F,0.25490198F,0.1764706F,
		0.1764706F,0.25490198F,0.1764706F,
		0.1764706F,0.25490198F,0.19215687F,
		0.1764706F,0.25490198F,0.20784314F,
		0.1764706F,0.25490198F,0.23921569F,
		0.1764706F,0.25490198F,0.25490198F,
		0.1764706F,0.23921569F,0.25490198F,
		0.1764706F,0.20784314F,0.25490198F,
		0.1764706F,0.19215687F,0.25490198F,
		0.13333334F,0.09411765F,0.02745098F,
		0.20392157F,0.14509805F,0.039215688F,
		0.27450982F,0.19607843F,0.05882353F,
		0.34509805F,0.2509804F,0.07058824F,
		0.4117647F,0.29411766F,0.09019608F,
		0.47843137F,0.34117648F,0.09803922F,
		0.5529412F,0.39607844F,0.11372549F,
		0.62352943F,0.4509804F,0.12941177F
	};
	
	/** The stream. */
	private CharSequenceStream stream;
	
	/** The font job. */
	private FontBuildJob fontJob;
	
	/** The font map texture. */
	private ITexture fontMapTexture;
	
	/** The is font built. */
	private boolean isFontBuilt;
	
	/** The font map handle. */
	private int fontMapHandle;
	
	/** The char table. */
	private FontChar[] charTable;
	
	// formatting
	/** The font size. */
	private float fontSize;
	
	/** The font alpha. */
	private float fontAlpha;
	
	/** The initial color r. */
	private float initialColorR;
	
	/** The initial color g. */
	private float initialColorG;
	
	/** The initial color b. */
	private float initialColorB;
	
	/**
	 * Instantiates a new font renderer.
	 */
	public FontRenderer()
	{
		this.fontJob = new FontBuildJob();
		this.stream = new CharSequenceStream();
		this.isFontBuilt = false;
		this.fontMapHandle = -1;
		this.fontSize = 16;
		this.fontAlpha = 1;
		this.initialColorR = 1;
		this.initialColorG = 1;
		this.initialColorB = 1;
	}
	
	/**
	 * Builds the font.
	 */
	public void buildFont()
	{
		fontJob.run();
	}
	
	/**
	 * Font update.
	 */
	public void fontUpdate()
	{
		if(!this.isFontBuilt)
		{
			if(this.fontJob.isBuilt)
			{
				// fontJob.builder.chars;
				
				this.charTable = new FontChar[this.fontJob.builder.biggestCodepoint+1];
				for(FontChar fc : this.fontJob.builder.chars)
				{
					this.charTable[fc.codepoint] = fc;
				}
				
				FontChar fakeChar = new FontChar(0, null);
				int fakeCharCount = 0;
				for(int i = 0; i < this.charTable.length; i++)
				{
					if(this.charTable[i] == null)
					{
						this.charTable[i] = fakeChar;
						fakeCharCount++;
					}
				}
				
				if(Boolean.FALSE)
				{
					System.out.println("[Info#] [FontRenderer] " + fakeCharCount + " fake characters linked.");
				}
				
				TextureUploader builder = new TextureUploader(this.fontJob.FNTMPW, this.fontJob.FNTMPH, this.fontJob.FNTMPBUF);
				builder.setWrapCLAMP();
				builder.setAnisotropy(0);
				builder.setLinearFilter(false);
				builder.setMipMap(0);
				
				this.fontMapTexture = builder.upload();
				this.fontMapHandle = this.fontMapTexture.getID();
				
				this.isFontBuilt = true;
			}
		}
	}
	
	/**
	 * Sets the font size.
	 *
	 * @param fontSize the new font size
	 */
	public void setFontSize(float fontSize)
	{
		this.fontSize = fontSize;
	}
	
	/**
	 * Sets the font alpha.
	 *
	 * @param alpha the new font alpha
	 */
	public void setFontAlpha(float alpha)
	{
		this.fontAlpha = MathUtility.clampFloat(0, 1, alpha);
	}

	/**
	 * Sets the font color.
	 *
	 * @param colorIndex the new font color
	 */
	public void setFontColor(int colorIndex)
	{
		if(colorIndex < 0)
		{
			colorIndex = 0xF;
		}
		if(((colorIndex*3)+3) > vgaPallet.length)
		{
			colorIndex = 0xF;
		}
		
		colorIndex *= 3;
		this.initialColorR = vgaPallet[colorIndex++];
		this.initialColorG = vgaPallet[colorIndex++];
		this.initialColorB = vgaPallet[colorIndex];
	}
	
	/**
	 * Sets the font color.
	 *
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 */
	public void setFontColor(float r, float g, float b)
	{
		initialColorR = r;
		initialColorG = g;
		initialColorB = b;
	}
	
	/**
	 * Gets the font size.
	 *
	 * @return the font size
	 */
	public float getFontSize()
	{
		return this.fontSize;
	}
	
	/**
	 * Gets the font size i.
	 *
	 * @return the font size i
	 */
	public int getFontSizeI()
	{
		return (int) this.fontSize;
	}
	
	/**
	 * Bind font map.
	 */
	public void bindFontMap()
	{
		if(this.fontMapHandle != -1)
		{
			TextureHelper.bindAndEnable(this.fontMapHandle);
		}
	}
	
	/**
	 * Draw string.
	 *
	 * @param string the string
	 * @param originX the origin x
	 * @param originY the origin y
	 * @param flags the flags
	 * @return the float
	 */
	public float drawString(CharSequence string, float originX, float originY, int flags)
	{
		final boolean drawAnything = (flags & 0b1) == 0;
		final IVertexDrawer va = drawAnything ? VertexDrawerVA.drawer : VertexDrawerNULL.instance;
		
		float xOffset = 0;
		float minX = 0;
		float minY = 0;
		float maxX = 0;
		float maxY = 0;
		float advance = 1;
		float colorR = this.initialColorR;
		float colorG = this.initialColorG;
		float colorB = this.initialColorB;
		boolean underlined = false;
		boolean striketrough = false;
		boolean background = false;
		boolean shadow = false;
		
		boolean first = true;
		float length = 0;
		
		va.startDrawing(IVertexDrawer.QUAD, true, true, false);
		this.stream.open(string);
		while(this.stream.hasNext())
		{
			char c = this.stream.next();
			int finalC = c < 0 ? 0 : (c >= this.charTable.length ? 0 : c);
			FontChar fc = this.charTable[finalC];
			
			if(fc == null)
			{
				throw new RuntimeException("Missing character in fontset: " + finalC);
			}
			
			minX = fc.minX;
			minY = fc.minY;
			maxX = fc.maxX;
			maxY = fc.maxY;
			advance = this.fontSize * fc.xAdvance;
			
			if((c == '\\') && (this.stream.peek(0) == '�'))
			{
				char tagChar = this.stream.peek(1);
				
				switch(tagChar)
				{
				case 'r':
				{
					background = false;
					underlined = false;
					striketrough = false;
					colorR = colorG = colorB = 1F;
					this.stream.seek(2);
					continue;
				}
				
				
				case 'g':
				{
					background ^= true;
					this.stream.seek(2);
					continue;
				}
				case 'u':
				{
					underlined = true;
					this.stream.seek(2);
					continue;
				}
				case 's':
				{
					striketrough = true;
					this.stream.seek(2);
					continue;
				}
				case 'v':
				{
					shadow = true;
					this.stream.seek(2);
					continue;
				}
				case 'c':
				{
					char A = this.stream.peek(2);
					char B = this.stream.peek(3);
					
					int _A = BinaryUtil.hexCharToInt(A);
					int _B = BinaryUtil.hexCharToInt(B);
					
					if((_A != -1) && (_B != -1))
					{
						// Its a color-code, Yay!
						int code = ((_A << 4) | _B) * 3;
						colorR = vgaPallet[code++];
						colorG = vgaPallet[code++];
						colorB = vgaPallet[code];
						
						this.stream.seek(4);
					}
					
					continue;
				}
				default: break;
				}
			}
			
			// Positioning
			float finalXOffset = originX + xOffset;
			float finalYOffset = originY;
			va.setOriginTranslation(finalXOffset, finalYOffset, 0);
			
			if(background)
			{
				va.setVertexColor(0, 0, 0, 0.75f * this.fontAlpha);
				va.setVertexUV(0, 0, 0);
				va.vertex(first ? -1 : 0, -1, 0);
				va.vertex(advance, -1, 0);
				va.vertex(advance, this.fontSize + 1, 0);
				va.vertex(first ? -1 : 0, this.fontSize + 1, 0);
			}
			
			if(shadow)
			{
				final float S = 0.75f;
				va.setVertexColor(Math.max(colorR-S,0), Math.max(colorG-S,0), Math.max(colorB-S,0), this.fontAlpha);
				va.setOriginTranslation(finalXOffset + 0.5f, finalYOffset + 0.5f, 0);
				va.setVertexUV(0, minX, minY); va.vertex(0, 0, 0);
				va.setVertexUV(0, maxX, minY); va.vertex(this.fontSize, 0, 0);
				va.setVertexUV(0, maxX, maxY); va.vertex(this.fontSize, this.fontSize, 0);
				va.setVertexUV(0, minX, maxY); va.vertex(0, this.fontSize, 0);
				va.setOriginTranslation(finalXOffset, finalYOffset, 0);
			}
			
			// Coloring
			va.setVertexColor(colorR, colorG, colorB, this.fontAlpha);
			
			// Draw
			va.setVertexUV(0, minX, minY); va.vertex(0, 0, 0);
			va.setVertexUV(0, maxX, minY); va.vertex(this.fontSize, 0, 0);
			va.setVertexUV(0, maxX, maxY); va.vertex(this.fontSize, this.fontSize, 0);
			va.setVertexUV(0, minX, maxY); va.vertex(0, this.fontSize, 0);
			
			if(underlined)
			{
				va.setVertexColor(colorR * 0.75F, colorG * 0.75F, colorB * 0.75F, this.fontAlpha);
				va.setVertexUV(0, 0, 0);
				va.vertex(first ? -1 : 0, this.fontSize, 0);
				va.vertex(advance, this.fontSize, 0);
				va.vertex(advance, this.fontSize + 1, 0);
				va.vertex(first ? -1 : 0, this.fontSize + 1, 0);
			}
			
			if((c != ' ') && striketrough)
			{
				va.setVertexColor(colorR * 0.75F, colorG * 0.75F, colorB * 0.75F, this.fontAlpha);
				float fontSize2 = (int) (this.fontSize / 2F);
				va.setVertexUV(0, 0, 0);
				va.vertex(-1, fontSize2 - 1, 0);
				va.vertex(advance, fontSize2 - 1, 0);
				va.vertex(advance, fontSize2 + 1, 0);
				va.vertex(-1, fontSize2 + 1, 0);
			}
			
			xOffset += advance;
			length += advance;
			first = false;
		}
		this.stream.close();
		va.stopDrawing();
		
		return length;
	}
	
	/**
	 * Gets the bounds.
	 *
	 * @param string the string
	 * @param fontSize the font size
	 * @return the bounds
	 */
	public RectangleI getBounds(CharSequence string, int fontSize)
	{
		float oldSize = this.fontSize;
		this.fontSize = fontSize;
		float width = this.drawString(string, 0, 0, 1);
		float height = fontSize;
		this.fontSize = oldSize;
		return new RectangleI((int) width, (int) height);
	}
	
}
