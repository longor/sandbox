/*
 * 
 */
package de.longor.gcore.graphics.font;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Scanner;

import org.bushe.swing.event.EventBus;

import de.longor.gcore.resourcemanager.ResourceLocation;
import de.longor.gcore.resourcemanager.ResourceManager;
import de.longor.gcore.resourcemanager.ResourceNotFoundException;
import de.longor.sandbox.SandboxFetchEvent;
import de.longor1996.util.StringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class FontBuildJob.
 */
public class FontBuildJob implements Runnable
{
	
	/** The builder. */
	protected FontBuilder builder;
	
	/** The is built. */
	protected boolean isBuilt;
	
	/** The fntmpw. */
	protected int FNTMPW;
	
	/** The fntmph. */
	protected int FNTMPH;
	
	/** The fntmpbuf. */
	protected ByteBuffer FNTMPBUF;
	
	/**
	 * Instantiates a new font build job.
	 */
	protected FontBuildJob()
	{
		this.builder = new FontBuilder();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		
		ResourceManager resourceManager = null;
		{
			SandboxFetchEvent evt = new SandboxFetchEvent();
			EventBus.publish(evt);
			resourceManager = evt.sandbox.resmanager;
		}
		
		// This commented-out section of code generates a table with all the needed default ASCII characters.
		/*
		{
			String A = "^�!\"�$%&/=?`��{[()]}\\�@��������+#-.,*'_:;~#-.,<>|";
			String B = "abcdefghijklmnopqrstuvwxyz";
			String C = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String D = " \n\t";
			
			String FINAL = A + B + C + D;
			IntStack stack = new IntStack();
			for(int i = 0; i < FINAL.length(); i++)
			{
				stack.push(FINAL.codePointAt(i));
			}
			stack.sort();
			
			while(stack.size() > 0)
			{
				int codepoint = stack.pop();
				System.out.println("DEFCHAR " + codepoint + " " + Character.getName(codepoint).replace(' ', '_'));
			}
		}
		//*/
		
		try
		{
			InputStream stream = resourceManager.getResource(ResourceLocation.location("sandbox:fonts/default_plain/fontheader.txt"), true);
			Scanner scanner = new Scanner(stream);
			{
				boolean ignore = false;
				while(scanner.hasNextLine())
				{
					String line = scanner.nextLine().trim();
					
					if(line.length() <= 1)
					{
						continue;
					}
					
					if(line.charAt(0) == '#')
					{
						continue;
					}
					
					if(line.equals("IGNORE_ON"))
					{
						ignore = true;
						continue;
					}
					
					if(line.equals("IGNORE_OFF"))
					{
						ignore = false;
						continue;
					}
					
					if(ignore)
					{
						continue;
					}
					
					if(line.startsWith("BUFFERGROW "))
					{
						line = line.substring(8);
						int growsize = StringUtil.decimalStringToInt(line, -1);
						
						if(growsize != -1)
						{
							this.builder.cmd_buffergrow(growsize);
						}
						
						continue;
					}
					
					if(line.startsWith("MULTIDEF "))
					{
						line = line.substring(9);
						
						String $$min = line.substring(0, line.indexOf(' '));
						String $$max = line.substring(line.indexOf(' ')+1);
						
						int min = StringUtil.decimalStringToInt($$min, -1);
						int max = StringUtil.decimalStringToInt($$max, -1);
						
						for(int i = min; i < max; i++)
						{
							int codepoint = i;
							String codename = Character.getName(codepoint);
							this.builder.cmd_defchar(codepoint, codename);
						}
						
						continue;
					}
					
					if(line.startsWith("DEFCHAR "))
					{
						try
						{
							line = line.substring(8);
							
							int codepoint = StringUtil.decimalStringToInt(line.substring(0, line.indexOf(' ')), Integer.MIN_VALUE);
							String codename = line.substring(line.indexOf(' ')+1);
							
							this.builder.cmd_defchar(codepoint, codename);
						}
						catch(RuntimeException e)
						{
							e.printStackTrace();
						}
						continue;
					}
					
					if(line.startsWith("ADVANCE "))
					{
						try
						{
							line = line.substring(8);
							
							String $$point = line.substring(0, line.indexOf(' '));
							String $$advance = line.substring(line.indexOf(' ')+1);
							
							int codepoint = $$point.charAt(0) == '#' ?
									$$point.substring(1).charAt(0) :
									StringUtil.decimalStringToInt($$point, Integer.MIN_VALUE);
							
							double advance = $$advance.indexOf('/') != -1 ?
									this.fraction($$advance) :
									StringUtil.toDouble($$advance, -1);
							
							if(((advance >= 0) || (advance == -1)) && (this.isValidCodepoint(codepoint) || (codepoint == -1)))
							{
								this.builder.cmd_advance(codepoint, advance);
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
						continue;
					}
					
					// System.out.println(">> " + line);
				}
			}
			scanner.close();
		}
		catch(ResourceNotFoundException e)
		{
			System.err.println("[Error] FontBuilderJob: Error while building Font!");
			e.printStackTrace();
			return;
		}
		
		try
		{
			this.builder.build_loadImages(resourceManager);
		}
		catch (IOException e)
		{
			System.err.println("[Error] FontBuilderJob: Error while building Font!");
			e.printStackTrace();
			return;
		}
		
		this.builder.build_calcAndCreateCharMap();
		
		this.builder.build_transferCharsIntoCharMap();
		
		this.builder.build_debug_storeCharMapOnDisk();
		
		this.builder.build_transformCharMapIntoByteBuffer();
		
		this.FNTMPW = this.builder.charMapWidth;
		this.FNTMPH = this.builder.charMapHeight;
		this.FNTMPBUF = this.builder.charMapBuffer;
		
		System.out.println("[Info] FontBuilderJob: Done building Font!");
		
		this.isBuilt = true;
		
	}
	
	/**
	 * Find char by name.
	 *
	 * @param substring the substring
	 * @return the int
	 */
	@SuppressWarnings("unused")
	private int findCharByName(String substring)
	{
		String ref = null;
		
		for(int i = 0; i < 65535; i++)
		{
			ref = Character.getName(i);
			
			if(ref == null)
			{
				continue;
			}
			
			if(ref.equals(substring))
			{
				return i;
			}
		}
		
		return Integer.MIN_VALUE;
	}

	/**
	 * Fraction.
	 *
	 * @param fraction the fraction
	 * @return the double
	 */
	private double fraction(String fraction)
	{
		int indexOf = fraction.indexOf('/');
		String left = fraction.substring(0, indexOf);
		String right = fraction.substring(indexOf+1);
		return StringUtil.toDouble(left, 3) / StringUtil.toDouble(right, 4);
	}

	/**
	 * Checks if is valid codepoint.
	 *
	 * @param codepoint the codepoint
	 * @return true, if is valid codepoint
	 */
	private boolean isValidCodepoint(int codepoint)
	{
		return (codepoint > 0) && (codepoint < 65534);
	}
	
}
