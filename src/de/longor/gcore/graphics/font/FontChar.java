/*
 * 
 */
package de.longor.gcore.graphics.font;

import java.awt.image.BufferedImage;

import de.longor1996.util.math.geom2D.RectangleI;

// TODO: Auto-generated Javadoc
/**
 * The Class FontChar.
 */
public class FontChar implements Comparable<FontChar>
{
	// char-data
	/** The codepoint. */
	protected int codepoint;
	
	/** The codename. */
	protected String codename;
	
	/** The x advance. */
	protected float xAdvance;
	
	// build-time char-data
	/** The fc image. */
	protected BufferedImage fcImage;
	
	/** The fc rect. */
	protected RectangleI fcRect;
	
	// uv rect
	/** The min x. */
	protected float minX;
	
	/** The min y. */
	protected float minY;
	
	/** The max x. */
	protected float maxX;
	
	/** The max y. */
	protected float maxY;
	
	/**
	 * Instantiates a new font char.
	 *
	 * @param codepoint the codepoint
	 * @param codename the codename
	 */
	public FontChar(int codepoint, String codename)
	{
		this.codepoint = codepoint;
		this.codename = codename;
		
		this.xAdvance = 0.1f;
		this.minX = this.minY = this.maxX = this.maxY = 0;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(FontChar o)
	{
		return (int) Math.signum(this.codepoint - o.codepoint);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if(o == null)
		{
			return false;
		}
		
		if(!(o instanceof FontChar))
		{
			return false;
		}
		
		return ((FontChar)o).codepoint == this.codepoint;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return this.codepoint;
	}
	
}
