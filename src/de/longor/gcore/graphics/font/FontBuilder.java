/*
 * 
 */
package de.longor.gcore.graphics.font;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;

import de.longor.gcore.graphics.textures.TextureHelper;
import de.longor.gcore.resourcemanager.ResourceLocation;
import de.longor.gcore.resourcemanager.ResourceManager;
import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.geom2D.RectangleI;
import de.longor1996.util.math.geom2D.RectanglePacker;

// TODO: Auto-generated Javadoc
/**
 * The Class FontBuilder.
 */
public class FontBuilder
{
	
	/** The chars. */
	ArrayList<FontChar> chars;
	
	/** The char map. */
	BufferedImage charMap;
	
	/** The char map width. */
	int charMapWidth;
	
	/** The char map height. */
	int charMapHeight;
	
	/** The biggest codepoint. */
	int biggestCodepoint = 0;
	
	/** The char map buffer. */
	ByteBuffer charMapBuffer;
	
	/** The debug. */
	boolean debug;
	
	/**
	 * Instantiates a new font builder.
	 */
	protected FontBuilder()
	{
		this.chars = new ArrayList<FontChar>(0);
		this.debug = false;
	}
	
	/**
	 * Cmd_defchar.
	 *
	 * @param codepoint the codepoint
	 * @param codename the codename
	 */
	public void cmd_defchar(int codepoint, String codename)
	{
		FontChar fc = new FontChar(codepoint, codename);
		FontChar fc2 = this.getCodePoint(codepoint);
		
		if(fc2 != null)
		{
			System.err.println("[Info] FontBuilder, Warning: Character "+codepoint+"/"+codename+"# got overriden.");
			this.chars.set(this.chars.indexOf(fc2), fc);
		}
		else
		{
			if(this.debug) {
				System.out.println("[Info] FontBuilder: Added new Character: "+codepoint+" / "+codename);
			}
			this.chars.add(fc);
		}
		
		if(codepoint > this.biggestCodepoint)
		{
			this.biggestCodepoint = codepoint + 1;
		}
	}
	
	/**
	 * Cmd_advance.
	 *
	 * @param codepoint the codepoint
	 * @param advance the advance
	 */
	public void cmd_advance(int codepoint, double advance)
	{
		if(codepoint == -1)
		{
			for(FontChar fc : this.chars)
			{
				if(this.debug)
				{
					System.out.println("[Info] FontBuilder: Changed Advance for " + fc.codepoint + " to " + advance);
				}
				
				fc.xAdvance = (float) advance;
			}
			
			return;
		}
		
		FontChar fc = this.getCodePoint(codepoint);
		
		if(fc != null)
		{
			if(this.debug)
			{
				System.out.println("[Info] FontBuilder: Changed Advance for " + codepoint + " ("+Character.getName(codepoint)+") to " + advance);
			}
			
			fc.xAdvance = (float) advance;
		}
	}
	
	/**
	 * Cmd_buffergrow.
	 *
	 * @param growsize the growsize
	 */
	public void cmd_buffergrow(int growsize)
	{
		if(this.debug) {
			System.out.println("[Info] FontBuilder: Expanding build-buffer by: " + growsize);
		}
		this.chars.ensureCapacity(this.chars.size()+growsize);
	}
	
	/**
	 * Gets the code point.
	 *
	 * @param codepoint the codepoint
	 * @return the code point
	 */
	public FontChar getCodePoint(int codepoint)
	{
		for(FontChar fc : this.chars)
		{
			if(fc.codepoint == codepoint)
			{
				return fc;
			}
		}
		
		return null;
	}
	
	/**
	 * Build_load images.
	 *
	 * @param resourceManager the resource manager
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void build_loadImages(ResourceManager resourceManager) throws IOException
	{
		for(FontChar fontChar : this.chars)
		{
			ResourceLocation locA = ResourceLocation.location("sandbox:fonts/default_plain/"+fontChar.codepoint+".png");
			ResourceLocation locB = ResourceLocation.location("sandbox:fonts/default_plain/"+fontChar.codename+".png");
			InputStream in = null;
			
			in = resourceManager.getResource(locB, false);
			
			if(in == null)
			{
				in = resourceManager.getResource(locA, false);
			}
			
			if(in == null)
			{
				throw new IOException("Could not locate font-symbol: " + fontChar.codepoint+"/"+fontChar.codename);
			}
			
			// load the image
			fontChar.fcImage = ImageIO.read(in);
			
			if(this.debug) {
				System.out.println("[Info#] [FontBuilder] Loaded Image: " + fontChar.codename + " // " + fontChar.fcImage);
			}
			
			// close stream
			in.close();
		}
	}
	
	/**
	 * Build_calc and create char map.
	 */
	public void build_calcAndCreateCharMap()
	{
		if(this.debug) {
			System.out.println("[Info#] [FontBuilder] Calculating FontMap Size...");
		}
		
		int requiredSurfaceSize = 0;
		for(FontChar fontChar : this.chars)
		{
			requiredSurfaceSize += fontChar.fcImage.getWidth() * fontChar.fcImage.getHeight();
		}
		
		if(requiredSurfaceSize <= 0)
		{
			requiredSurfaceSize = 32 * 32;
		}
		
		// Get the squareroot of the required edge-length, then multiply it by 2.
		int requiredEdgeLength = (int) (Math.sqrt(requiredSurfaceSize) * 1.2D);
		
		// Scale requiredEdgeLength to be a POT-number.
		int edgeLength = MathUtility.nextPOT(requiredEdgeLength);
		
		if(edgeLength > 4096)
		{
			edgeLength = 4096;
		}
		
		this.charMapWidth = edgeLength;
		this.charMapHeight = edgeLength;
		
		this.charMap = new BufferedImage(this.charMapWidth, this.charMapHeight, BufferedImage.TYPE_INT_ARGB);
	}
	
	/**
	 * Build_debug_store char map on disk.
	 */
	public void build_debug_storeCharMapOnDisk()
	{
		try
		{
			ImageIO.write(this.charMap, "PNG", new File("./charmap.png"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Build_transfer chars into char map.
	 */
	public void build_transferCharsIntoCharMap()
	{
		RectanglePacker<Object> packer = new RectanglePacker<Object>(this.charMapWidth, this.charMapHeight, 2);
		Collections.sort(this.chars);
		
		if(this.debug) {
			System.out.println("[Info#] [FontBuilder] Calculating FontMap Constants...");
		}
		
		float IMGW = this.charMapWidth;
		float IMGH = this.charMapHeight;
		
		final float pixelX = 1F / IMGW;
		final float pixelY = 1F / IMGH;
		
		final float halfPixelX = pixelX * 0.1F;
		final float halfPixelY = pixelY * 0.1F;
		
		if(this.debug)
		{
			System.out.println("[Info#][FontBuilder] Building FontMap... ");
			System.out.println("[Info#][FontBuilder] FontMap Size: " + this.charMapWidth + "x" + this.charMapHeight);
			System.out.println("[Info#][FontBuilder] FontMap Pixel-Size: " + pixelX + "   " + pixelY);
			
			System.out.println("[Info#] [FontBuilder] Blitting FontMap...");
		}
		
		Graphics2D g2D = this.charMap.createGraphics();
		{
			if(Boolean.FALSE)
			{
				g2D.setColor(Color.BLACK);
				g2D.fillRect(0, 0, this.charMapWidth, this.charMapHeight);
			}
			
			g2D.setColor(Color.WHITE);
			g2D.fillRect(0, 0, 2, 2);
			
			if(Boolean.TRUE)
			{
				g2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g2D.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
			}
			
			g2D.setColor(Color.WHITE);
			
			for(FontChar fc : this.chars)
			{
				// Rectangle rectangle = new Rectangle(fc.fcImage.getWidth(), fc.fcImage.getHeight());
				RectangleI finalRectangle = packer.insert(fc.fcImage.getWidth(), fc.fcImage.getHeight(), new Object());
				
				if(finalRectangle == null)
				{
					System.err.println("[Error] [FontBuilder] Unable to pack any more symbols into BitMap!");
					break;
				}
				
				// Put the rectangle into the FontChar
				fc.fcRect = finalRectangle;
				
				// Blit the FontChar into the BitMap.
				g2D.drawImage(fc.fcImage, finalRectangle.x, finalRectangle.y, finalRectangle.width, finalRectangle.height, null);
				
				// Now calculate the UV-coordinates.
				fc.minX = finalRectangle.x; fc.minX /= IMGW; fc.minX += halfPixelX;
				fc.minY = finalRectangle.y; fc.minY /= IMGH; fc.minY += halfPixelY;
				
				fc.maxX = finalRectangle.x + finalRectangle.width;  fc.maxX /= IMGW; fc.maxX -= halfPixelX;
				fc.maxY = finalRectangle.y + finalRectangle.height; fc.maxY /= IMGH; fc.maxY -= halfPixelY;
				
				/*
				fc.minX = ((float)finalRectangle.x / IMGW) + halfPixelX;
				fc.minY = ((float)finalRectangle.y / IMGH) - halfPixelY;
				fc.maxX = (((float)finalRectangle.x + (float)finalRectangle.width) / IMGW) + halfPixelX;
				fc.maxY = (((float)finalRectangle.y + (float)finalRectangle.height) / IMGH) - halfPixelY;
				//*/
			}
		}
		g2D.dispose();
		
		if(this.debug) {
			System.out.println("[Info#] [FontBuilder] Done blitting FontMap.");
		}
	}
	
	/**
	 * Build_transform char map into byte buffer.
	 */
	public void build_transformCharMapIntoByteBuffer()
	{
		this.charMapBuffer = TextureHelper.convertBufferedImageToByteBuffer(this.charMap, null);
	}
	
}
