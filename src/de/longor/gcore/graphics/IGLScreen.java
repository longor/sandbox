/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.LWJGLException;

// TODO: Auto-generated Javadoc
/**
 * The Interface IGLScreen.
 */
public interface IGLScreen
{
	
	/**
	 * Show.
	 *
	 * @throws LWJGLException the LWJGL exception
	 */
	public void show() throws LWJGLException;
	
	/**
	 * Cleanup.
	 */
	public void cleanup();
	
	/**
	 * Pre_update.
	 *
	 * @return true, if successful
	 */
	public boolean pre_update();
	
	/**
	 * Post_update.
	 */
	public void post_update();
	
	/**
	 * Clear.
	 *
	 * @param color the color
	 * @param depth the depth
	 * @param stencil the stencil
	 */
	public void clear(boolean color, boolean depth, boolean stencil);
	
	/**
	 * Checks if is close requested.
	 *
	 * @return true, if is close requested
	 */
	public boolean isCloseRequested();
	
	/**
	 * Gets the virtual width.
	 *
	 * @return the virtual width
	 */
	public float getVirtualWidth();
	
	/**
	 * Gets the virtual height.
	 *
	 * @return the virtual height
	 */
	public float getVirtualHeight();
	
	/**
	 * Gets the physical width.
	 *
	 * @return the physical width
	 */
	public float getPhysicalWidth();
	
	/**
	 * Gets the physical height.
	 *
	 * @return the physical height
	 */
	public float getPhysicalHeight();
	
	/**
	 * Gets the aspect ratio.
	 *
	 * @return the aspect ratio
	 */
	public float getAspectRatio();
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title);
	
	/**
	 * Sets the virtual scale.
	 *
	 * @param scale the new virtual scale
	 */
	public void setVirtualScale(int scale);
	
	/**
	 * Sets the framerate cap.
	 *
	 * @param maximumFramerate the new framerate cap
	 */
	public void setFramerateCap(int maximumFramerate);
	
	/**
	 * Sets the virtual minimum screen size.
	 *
	 * @param vw the vw
	 * @param vh the vh
	 */
	public void setVirtualMinimumScreenSize(int vw, int vh);
	
	/**
	 * Sets the physical minimum screen size.
	 *
	 * @param vw the vw
	 * @param vh the vh
	 */
	public void setPhysicalMinimumScreenSize(int vw, int vh);
	
	/**
	 * Sets the borderless.
	 *
	 * @param isBorderless the new borderless
	 */
	public void setBorderless(boolean isBorderless);
	
	/**
	 * Sets the expand window.
	 *
	 * @param expand the new expand window
	 */
	public void setExpandWindow(boolean expand);
	
	/**
	 * Sets the resized.
	 */
	public void setResized();
	
	/**
	 * Checks if is physical size real.
	 *
	 * @return true, if is physical size real
	 */
	public boolean isPhysicalSizeReal();
	
}
