/*
 * 
 */
package de.longor.gcore.graphics;

import java.nio.FloatBuffer;
import java.util.Arrays;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

// TODO: Auto-generated Javadoc
/**
 * 
 * Vertex Drawer based on intermediate vertex-batching using Vertex-Arrays.
 * 
 * Supports:
 * - Per-Vertex Colors
 * - Per-Vertex Normals
 * - Per-Vertex Multilayer Texture-Coordinates
 * - Vertex Offsetting.
 * 
 **/
public class VertexDrawerVA implements IVertexDrawer {
	
	/** The Constant drawer. */
	public static final VertexDrawerVA drawer = new VertexDrawerVA();
	
	/** The Constant defaultActiveTextureUnits. */
	private static final int[] defaultActiveTextureUnits = new int[]{0};
	
	/** The Constant MAX_TEXTURE_UNITS. */
	private static final int MAX_TEXTURE_UNITS = 4;
	
	/** The vertex buffer. */
	FloatBuffer vertexBuffer;
	
	/** The normal buffer. */
	FloatBuffer normalBuffer;
	
	/** The color buffer. */
	FloatBuffer colorBuffer;
	
	/** The texture buffer. */
	FloatBuffer[] textureBuffer;
	
	/** The vertex count. */
	int vertexCount = 0;
	
	/** The draw mode. */
	int drawMode = -1;
	
	/** The active texture units. */
	int[] activeTextureUnits = defaultActiveTextureUnits;
	
	/** The r. */
	float r = 1;
	
	/** The g. */
	float g = 1;
	
	/** The b. */
	float b = 1;
	
	/** The a. */
	float a = 1;
	
	/** The nx. */
	float nx = 0;
	
	/** The ny. */
	float ny = 1;
	
	/** The nz. */
	float nz = 0;
	
	/** The off x. */
	float offX = 0;
	
	/** The off y. */
	float offY = 0;
	
	/** The off z. */
	float offZ = 0;
	
	/** The s. */
	float[] s = new float[MAX_TEXTURE_UNITS];
	
	/** The t. */
	float[] t = new float[MAX_TEXTURE_UNITS];
	
	/** The use color. */
	boolean useColor = false;
	
	/** The use normals. */
	boolean useNormals = false;
	
	/** The use uv. */
	boolean useUV = false;
	
	/** The is drawing. */
	boolean isDrawing = false;
	
	/**
	 * Instantiates a new vertex drawer va.
	 */
	public VertexDrawerVA()
	{
		// 1024 = 1K
		// 1024 * 1024 = 1M
		// 1024 * 1024 * 2 = 2M
		
		//   KB     MB     MB*N
		this(1024 * 1024 * 2, 1);
	}
	
	/**
	 * Instantiates a new vertex drawer va.
	 *
	 * @param amount the amount
	 * @param texture_units the texture_units
	 */
	public VertexDrawerVA(int amount, int texture_units)
	{
		this.vertexBuffer = BufferUtils.createFloatBuffer(amount);
		this.normalBuffer = BufferUtils.createFloatBuffer(amount);
		this.colorBuffer = BufferUtils.createFloatBuffer(amount);
		this.textureBuffer = new FloatBuffer[texture_units];
		
		for(int i = 0; i < texture_units; i++) {
			this.textureBuffer[i] = BufferUtils.createFloatBuffer(amount);
		}
		
		this.reset();
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setOriginTranslation(float, float, float)
	 */
	@Override
	public void setOriginTranslation(float x, float y, float z) {
		this.offX = x;
		this.offY = y;
		this.offZ = z;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexColor(float, float, float, float)
	 */
	@Override
	public void setVertexColor(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexNormal(float, float, float)
	 */
	@Override
	public void setVertexNormal(float x, float y, float z) {
		this.nx = x;
		this.ny = y;
		this.nz = z;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#setVertexUV(int, float, float)
	 */
	@Override
	public void setVertexUV(int texUnit, float s, float t) {
		this.s[texUnit] = s;
		this.t[texUnit] = t;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#vertex(float, float, float)
	 */
	@Override
	public void vertex(float x, float y, float z) {
		this.vertexBuffer.put(x + this.offX);
		this.vertexBuffer.put(y + this.offY);
		this.vertexBuffer.put(z + this.offZ);
		
		if(this.useNormals)
		{
			this.normalBuffer.put(this.nx);
			this.normalBuffer.put(this.ny);
			this.normalBuffer.put(this.nz);
		}
		
		if(this.useColor)
		{
			this.colorBuffer.put(this.r);
			this.colorBuffer.put(this.g);
			this.colorBuffer.put(this.b);
			this.colorBuffer.put(this.a);
		}
		
		if(this.useUV) {
			for(int i = 0; i < this.textureBuffer.length; i++)
			{
				this.textureBuffer[i].put(this.s[i]);
				this.textureBuffer[i].put(this.t[i]);
			}
		}
		
		this.vertexCount++;
	}
	
	/**
	 * Reset.
	 */
	private void reset() {
		this.r =
		this.g =
		this.b =
		this.a =
		this.nx =
		this.ny =
		this.nz =
		this.offX =
		this.offY =
		this.offZ =
		this.drawMode = 0;
		
		Arrays.fill(this.s, 0);
		Arrays.fill(this.t, 0);
		
		this.isDrawing = false;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#startDrawing(int, boolean, boolean, boolean)
	 */
	@Override
	public void startDrawing(int mode, boolean color, boolean texture, boolean normals) {
		if (this.isDrawing) {
			throw new RuntimeException("VertexDrawerVA-instance is already drawing!");
		}
		
		this.reset();
		this.vertexCount = 0;
		this.drawMode = mode;
		this.useColor = color;
		this.useNormals = normals;
		this.useUV = texture;
		this.isDrawing = true;
		this.activeTextureUnits = defaultActiveTextureUnits;
		
	}
	
	/**
	 * Post start.
	 *
	 * @param activeTextureUnits the active texture units
	 */
	public void postStart(int[] activeTextureUnits)
	{
		this.activeTextureUnits = activeTextureUnits;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#stopDrawing()
	 */
	@Override
	public void stopDrawing() {
		this.vertexBuffer.rewind();
		this.colorBuffer.rewind();
		this.normalBuffer.rewind();
		for(int i = 0; i < this.textureBuffer.length; i++) {
			this.textureBuffer[i].rewind();
		}
		
		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glVertexPointer(3, /* stride */3 << 2, this.vertexBuffer);
		
		if (this.useColor)
		{
			GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glColorPointer(4, /* stride */4 << 2, this.colorBuffer);
		}
		
		if (this.useNormals)
		{
			GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
			GL11.glNormalPointer(/* stride */ 3 << 2, this.normalBuffer);
		}
		
		if (this.useUV)
		{
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			
			for(int i = 0; i < this.activeTextureUnits.length; i++)
			{
				int unitID = this.activeTextureUnits[i];
				GL13.glActiveTexture(GL13.GL_TEXTURE0 + unitID);
				GL11.glTexCoordPointer(2, /* stride */2 << 2, this.textureBuffer[unitID]);
			}
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
		}
		
		GL11.glDrawArrays(this.drawMode, 0, this.vertexCount);
		
		GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		
		if (this.useColor) {
			GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
		
		if (this.useNormals) {
			GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
		}
		
		if (this.useUV) {
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
		
		this.reset();
		
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#isDrawing()
	 */
	@Override
	public boolean isDrawing() {
		return this.isDrawing;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#hasDrawenAnyVertices()
	 */
	@Override
	public boolean hasDrawenAnyVertices() {
		return this.vertexCount != 0;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#getCurrentVertexCount()
	 */
	@Override
	public int getCurrentVertexCount() {
		return this.vertexCount;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.IVertexDrawer#remainingUntilFailure()
	 */
	@Override
	public int remainingUntilFailure() {
		return this.vertexBuffer.remaining();
	}
	
}
