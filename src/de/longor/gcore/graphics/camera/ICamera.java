/*
 * 
 */
package de.longor.gcore.graphics.camera;

import de.longor1996.util.math.Matrix4F;
import de.longor1996.util.math.Vector3F;

// TODO: Auto-generated Javadoc
/**
 * The Interface ICamera.
 */
public interface ICamera
{
	
	/**
	 * Gets the camera matrix.
	 *
	 * @param interpolation the interpolation
	 * @return the camera matrix
	 */
	public Matrix4F getCameraMatrix(float interpolation);
	
	/**
	 * Gets the camera rotation matrix.
	 *
	 * @param interpolation the interpolation
	 * @return the camera rotation matrix
	 */
	public Matrix4F getCameraRotationMatrix(float interpolation);
	
	/**
	 * Gets the position.
	 *
	 * @param out the out
	 * @return the position
	 */
	public void getPosition(Vector3F out);
	
	/**
	 * Gets the forward.
	 *
	 * @param out the out
	 * @return the forward
	 */
	public void getForward(Vector3F out);
	
}
