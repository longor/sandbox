/*
 * 
 */
package de.longor.gcore.graphics;


// TODO: Auto-generated Javadoc
/**
 * The Interface IVertexDrawer.
 */
public interface IVertexDrawer {
	
	/** The Constant POINT. */
	public static final int POINT = 0x0;
	
	/** The Constant LINE. */
	public static final int LINE = 0x1;
	
	/** The Constant LINE_LOOP. */
	public static final int LINE_LOOP = 0x2;
	
	/** The Constant LINE_STRIP. */
	public static final int LINE_STRIP = 0x3;
	
	/** The Constant TRIANGLE. */
	public static final int TRIANGLE = 0x4;
	
	/** The Constant QUAD. */
	public static final int QUAD = 0x7;
	
	/** The Constant POLYGON. */
	public static final int POLYGON = 0x9;
	
	// VERTEX ATTRIBUTES
	/**
	 * Sets the origin translation.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setOriginTranslation(float x, float y, float z);
	
	/**
	 * Sets the vertex color.
	 *
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public void setVertexColor(float r, float g, float b, float a);
	
	/**
	 * Sets the vertex normal.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void setVertexNormal(float x, float y, float z);
	
	/**
	 * Sets the vertex uv.
	 *
	 * @param texUnit the tex unit
	 * @param s the s
	 * @param t the t
	 */
	public void setVertexUV(int texUnit, float s, float t);
	
	// VERTEX BATCHING
	/**
	 * Vertex.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 */
	public void vertex(float x, float y, float z);
	
	// START / STOP
	/**
	 * Start drawing.
	 *
	 * @param mode the mode
	 * @param color the color
	 * @param texture the texture
	 * @param normals the normals
	 */
	public void startDrawing(int mode, boolean color, boolean texture, boolean normals);
	
	/**
	 * Stop drawing.
	 */
	public void stopDrawing();
	
	// STATE ASKERS
	/**
	 * Checks if is drawing.
	 *
	 * @return true, if is drawing
	 */
	public boolean isDrawing();
	
	/**
	 * Checks for drawen any vertices.
	 *
	 * @return true, if successful
	 */
	public boolean hasDrawenAnyVertices();
	
	/**
	 * Gets the current vertex count.
	 *
	 * @return the current vertex count
	 */
	public int getCurrentVertexCount();
	
	/**
	 * Remaining until failure.
	 *
	 * @return the int
	 */
	public int remainingUntilFailure();
	
}
