/*
 * 
 */
package de.longor.gcore.graphics.projection;

import de.longor.gcore.graphics.IGLScreen;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class PhysicalScreenProjection.
 */
public class PhysicalScreenProjection implements IProjection
{
	
	/** The null. */
	Matrix4F NULL = Matrix4F.newInstance();
	
	/** The screen. */
	final IGLScreen screen;
	
	/**
	 * Instantiates a new physical screen projection.
	 *
	 * @param screen2 the screen2
	 */
	public PhysicalScreenProjection(IGLScreen screen2)
	{
		screen = screen2;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.projection.IProjection#getProjectionMatrix(float)
	 */
	@Override
	public Matrix4F getProjectionMatrix(float interpolation)
	{
		NULL.setIdentity();
		float left = -1;
		float right = screen.getPhysicalWidth();
		float bottom = screen.getPhysicalHeight()+1;
		float top = 0;
		NULL.calculateOrthoProjectionMatrix(left, right, bottom, top, 1, -1);
		return NULL;
	}

}