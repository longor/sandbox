/*
 * 
 */
package de.longor.gcore.graphics.projection;

import de.longor.gcore.graphics.screen.GLNativeScreen;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class VirtualScreenProjection.
 */
public class VirtualScreenProjection implements IProjection
{
	
	/** The null. */
	Matrix4F NULL = Matrix4F.newInstance();
	
	/** The screen. */
	final GLNativeScreen screen;
	
	/**
	 * Instantiates a new virtual screen projection.
	 *
	 * @param scr the scr
	 */
	public VirtualScreenProjection(GLNativeScreen scr)
	{
		screen = scr;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.projection.IProjection#getProjectionMatrix(float)
	 */
	@Override
	public Matrix4F getProjectionMatrix(float interpolation)
	{
		NULL.setIdentity();
		NULL.calculateScreenProjectionMatrix(screen.getVirtualWidth(), screen.getVirtualHeight());
		return NULL;
	}

}