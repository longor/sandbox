/*
 * 
 */
package de.longor.gcore.graphics.projection;

import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Interface IProjection.
 */
public interface IProjection
{
	
	/**
	 * Gets the projection matrix.
	 *
	 * @param interpolation the interpolation
	 * @return the projection matrix
	 */
	public Matrix4F getProjectionMatrix(float interpolation);
	
}
