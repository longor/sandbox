/*
 * 
 */
package de.longor.gcore.graphics.projection;

import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class NullProjection.
 */
public class NullProjection implements IProjection {
	
	/** The null. */
	Matrix4F NULL = Matrix4F.newInstance();
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.projection.IProjection#getProjectionMatrix(float)
	 */
	@Override
	public Matrix4F getProjectionMatrix(float interpolation)
	{
		return NULL;
	}

}