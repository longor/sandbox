/*
 * 
 */
package de.longor.gcore.graphics.projection;

import de.longor.gcore.graphics.IGLScreen;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class SimpleFirstPersonProjection.
 */
public class SimpleFirstPersonProjection implements IProjection
{
	
	/** The null. */
	Matrix4F NULL = Matrix4F.newInstance();
	
	/** The screen. */
	final IGLScreen screen;
	
	/** The field of view. */
	final float fieldOfView;
	
	/**
	 * Instantiates a new simple first person projection.
	 *
	 * @param scr the scr
	 */
	public SimpleFirstPersonProjection(IGLScreen scr)
	{
		screen = scr;
		fieldOfView = 90f;
	}
	
	/**
	 * Instantiates a new simple first person projection.
	 *
	 * @param screen2 the screen2
	 * @param fov the fov
	 */
	public SimpleFirstPersonProjection(IGLScreen screen2, float fov)
	{
		screen = screen2;
		fieldOfView = fov;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.projection.IProjection#getProjectionMatrix(float)
	 */
	@Override
	public Matrix4F getProjectionMatrix(float interpolation)
	{
		NULL.setIdentity();
		NULL.calculatePerspectiveProjectionMatrix(fieldOfView, screen.getAspectRatio(), 0.1f, 512f);
		return NULL;
	}

}