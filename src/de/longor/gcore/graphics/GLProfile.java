/*
 * 
 */
package de.longor.gcore.graphics;

import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

// TODO: Auto-generated Javadoc
/**
 * The Class GLProfile.
 */
public class GLProfile
{
	
	/** The context. */
	static ContextCapabilities context;
	
	/** The anisotropy enabled. */
	public static boolean anisotropyEnabled;
	
	/** The max anisotropy. */
	public static int maxAnisotropy;
	
	/** The G l13. */
	public static boolean GL13;
	
	/**
	 * Load profile.
	 */
	public static void loadProfile()
	{
		context = GLContext.getCapabilities();
		
		GL13 = context.OpenGL13;
		
		anisotropyEnabled = context.GL_EXT_texture_filter_anisotropic;
		
		if(anisotropyEnabled)
		{
			maxAnisotropy = (int) GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT);
		}
		else
		{
			maxAnisotropy = 0;
		}
		
	}
	
}
