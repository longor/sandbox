/*
 * 
 */
package de.longor.gcore.graphics;

import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Interface Camera.
 */
@Deprecated
public interface Camera
{
	
	/**
	 * The Class NullCamera.
	 */
	@Deprecated
	public class NullCamera implements Camera {
		
		/** The null. */
		Matrix4F NULL = Matrix4F.newInstance();
		
		/* (non-Javadoc)
		 * @see de.longor.gcore.graphics.Camera#getViewMatrix(float)
		 */
		@Override
		public Matrix4F getViewMatrix(float interpolation)
		{
			return NULL;
		}
	}
	
	/**
	 * The Class SimpleRotatingCamera.
	 */
	@Deprecated
	public class SimpleRotatingCamera implements Camera {
		
		/** The null. */
		Matrix4F NULL = Matrix4F.newInstance();
		
		/* (non-Javadoc)
		 * @see de.longor.gcore.graphics.Camera#getViewMatrix(float)
		 */
		@Override
		public Matrix4F getViewMatrix(float interpolation)
		{
			NULL.setIdentity();
			
			Matrix4F.translate(0, 0, 1, NULL, NULL);
			float rot = (float) (MathUtility.getLoadTimePoint()) / 50F;
			Matrix4F.rotateAroundAxisByDegrees(rot, 0, 1, 0, NULL, NULL);
			Matrix4F.translate(0, -1, 0, NULL, NULL);
			
			return NULL;
		}
	}
	
	/**
	 * Gets the view matrix.
	 *
	 * @param interpolation the interpolation
	 * @return the view matrix
	 */
	public Matrix4F getViewMatrix(float interpolation);
	
}
