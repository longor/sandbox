/*
 * 
 */
package de.longor.gcore.resourcemanager;

import java.io.IOException;
import java.io.InputStream;

// TODO: Auto-generated Javadoc
/**
 * The Interface IResourceProvider.
 */
public interface IResourceProvider
{
	
	/**
	 * Gets the provider name.
	 *
	 * @return the provider name
	 */
	public abstract String getProviderName();
	
	/// ToDo?
	// public abstract boolean doesResourceExist(ResourceLocation location);
	
	/**
	 * Opens a stream to the resource.
	 * 
	 * This method may throw a {@code ResourceNotFoundException},
	 * if the resource does not exist.
	 *
	 * @param location the location
	 * @return the resource stream
	 * @throws ResourceNotFoundException the resource not found exception
	 */
	public abstract InputStream getResourceStream(ResourceLocation location) throws ResourceNotFoundException;
	
	/**
	 * Opens the provider for usage.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public abstract void openProvider() throws IOException;
	
	/**
	 * Closes the provider.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public abstract void closeProvider() throws IOException;
	
}
