/*
 * 
 */
package de.longor.gcore.resourcemanager;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceLocation.
 */
public class ResourceLocation
{
	/**
	 * The relative abstract path to a/the resource.
	 **/
	private final String resourcePath;
	
	/**
	 * Creates a new ResourceLocation object.
	 *
	 * @param resourcePath the resource path
	 * @return the resource location
	 */
	public static ResourceLocation location(String resourcePath)
	{
		return new ResourceLocation(resourcePath);
	}
	
	/**
	 * To make sure that ResourceLocation objects are reused, you have to create them trough the 'location' method.
	 *
	 * @param resourcePath the resource path
	 */
	private ResourceLocation(String resourcePath)
	{
		// Package check
		if(resourcePath.indexOf(':') != -1)
		{
			int indexOf = resourcePath.indexOf(':');
			String pack = resourcePath.substring(0, indexOf);
			String path = resourcePath.substring(indexOf+1);
			
			StringBuilder builder = new StringBuilder();
			builder.append("assets/");
			builder.append(pack);
			builder.append("/");
			builder.append(path);
			resourcePath = builder.toString();
		}
		
		this.resourcePath = resourcePath;
	}
	
	/**
	 * Returns the abstract path.
	 *
	 * @return the path
	 */
	public String getPath()
	{
		return this.resourcePath;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "resourceLocation:{path:\""+this.getPath()+"\"}";
	}
	
}
