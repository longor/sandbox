/*
 * 
 */
package de.longor.gcore.resourcemanager;

import java.io.IOException;
import java.io.InputStream;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemClassLoaderResourceProvider.
 */
public class SystemClassLoaderResourceProvider implements IResourceProvider
{
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#getProviderName()
	 */
	@Override
	public String getProviderName()
	{
		return "systemclassloader";
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#getResourceStream(de.longor.gcore.resourcemanager.ResourceLocation)
	 */
	@Override
	public InputStream getResourceStream(ResourceLocation location) throws ResourceNotFoundException
	{
		if(location == null)
		{
			throw new ResourceNotFoundException("'location' is null!");
		}
		
		InputStream in = ClassLoader.getSystemResourceAsStream(location.getPath());
		
		if(in == null)
		{
			throw new ResourceNotFoundException("Could not locate resource: " + location + "!");
		}
		
		return in;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#openProvider()
	 */
	@Override
	public void openProvider() throws IOException
	{
		// Nothing to do here!
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#closeProvider()
	 */
	@Override
	public void closeProvider() throws IOException
	{
		// Nothing to do here!
	}
	
}
