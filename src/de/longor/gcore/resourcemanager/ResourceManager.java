/*
 * 
 */
package de.longor.gcore.resourcemanager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceManager.
 */
public class ResourceManager
{
	
	/** The providers. */
	private final ArrayList<IResourceProvider> providers;
	
	/**
	 * Instantiates a new resource manager.
	 */
	public ResourceManager()
	{
		this.providers = new ArrayList<IResourceProvider>();
		
		this.addResourceProvider(new SystemClassLoaderResourceProvider());
	}
	
	/**
	 * Adds the resource provider.
	 *
	 * @param provider the provider
	 */
	public void addResourceProvider(IResourceProvider provider)
	{
		this.providers.add(provider);
	}
	
	/**
	 * Removes the resource provider.
	 *
	 * @param provider the provider
	 */
	public void removeResourceProvider(IResourceProvider provider)
	{
		this.providers.remove(provider);
		
		try
		{
			provider.closeProvider();
		}
		catch (IOException e)
		{
			// TODO: What ToDo here if the provider fails to close?
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Open providers.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void openProviders() throws IOException
	{
		
		for(IResourceProvider provider : this.providers)
		{
			try {
				provider.openProvider();
			} catch (IOException e) {
				e.printStackTrace();
				throw new IOException("Could not open all providers.", e);
			}
		}
		
	}
	
	/**
	 * Close providers.
	 */
	public void closeProviders()
	{
		
		for(IResourceProvider provider : this.providers)
		{
			try
			{
				provider.closeProvider();
			}
			catch (IOException e)
			{
				// TODO: What ToDo here if a provider fails to close?
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * May throw a {@link ResourceNotFoundException}.
	 *
	 * @param location the location
	 * @param throwAnyExceptions the throw any exceptions
	 * @return the resource
	 */
	public InputStream getResource(ResourceLocation location, boolean throwAnyExceptions)
	{
		if(location == null)
		{
			if(throwAnyExceptions)
			{
				throw new ResourceNotFoundException("Could not find resource: 'location' is null.");
			}
			else
			{
				return null;
			}
		}
		
		for(IResourceProvider provider : this.providers)
		{
			try
			{
				return provider.getResourceStream(location);
			}
			catch(ResourceNotFoundException e)
			{
				// Ignore and continue looking for the resource!
			}
		}
		
		if(throwAnyExceptions)
		{
			throw new ResourceNotFoundException("Could not find resource: " + location);
		}
		else
		{
			return null;
		}
		
	}
	
}
