/*
 *
 */
package de.longor.gcore.resourcemanager;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceNotFoundException.
 */
public class ResourceNotFoundException extends RuntimeException
{
	
	/**
	 * Instantiates a new resource-not-found exception.
	 *
	 * @param string the string
	 */
	public ResourceNotFoundException(String string)
	{
		super(string);
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7133180316604437323L;
	
}
