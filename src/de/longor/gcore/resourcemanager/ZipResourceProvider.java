/*
 * 
 */
package de.longor.gcore.resourcemanager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

// TODO: Auto-generated Javadoc
/**
 * The Class ZipResourceProvider.
 */
public class ZipResourceProvider implements IResourceProvider
{
	
	/** The source. */
	private File source;
	
	/** The zip file. */
	private ZipFile zipFile;
	
	/**
	 * Instantiates a new zip resource provider.
	 *
	 * @param zipfile the zipfile
	 */
	public ZipResourceProvider(File zipfile)
	{
		this.source = zipfile;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#getProviderName()
	 */
	@Override
	public String getProviderName()
	{
		return "zip#" + this.source.getName();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#getResourceStream(de.longor.gcore.resourcemanager.ResourceLocation)
	 */
	@Override
	public InputStream getResourceStream(ResourceLocation location) throws ResourceNotFoundException
	{
		ZipEntry entry = this.zipFile.getEntry(location.getPath());
		
		try
		{
			return this.zipFile.getInputStream(entry);
		}
		catch (IOException e)
		{
			throw new ResourceNotFoundException("Could not locate resource "+location+" inside zip: " + e.getLocalizedMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#openProvider()
	 */
	@Override
	public void openProvider() throws IOException
	{
		this.zipFile = new ZipFile(this.source);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.resourcemanager.IResourceProvider#closeProvider()
	 */
	@Override
	public void closeProvider() throws IOException
	{
		this.zipFile.close();
	}
	
}
