/*
 * 
 */
package de.longor.gcore;


import java.util.HashMap;

import de.longor1996.util.StringStream;

// TODO: Auto-generated Javadoc
/**
 * The Class ArgsMap.
 */
public class ArgsMap
{
	
	/**
	 * Creates the and load.
	 *
	 * @param args the args
	 * @return the args map
	 */
	public static final ArgsMap createAndLoad(String[] args)
	{
		ArgsMap map = new ArgsMap();
		map.load(args);
		return map;
	}
	
	/** The Constant NULL. */
	private static final String NULL = "<null>";
	
	/** The arguments. */
	private HashMap<String, String> arguments;
	
	/**
	 * Instantiates a new args map.
	 */
	public ArgsMap()
	{
		this.arguments = new HashMap<String, String>();
	}
	
	/**
	 * Load.
	 *
	 * @param args the args
	 */
	public void load(String[] args)
	{
		// Make a new StringStream!
		StringStream str = new StringStream(args);
		
		// As long as we have Arguments, we will continue scanning trough them!
		while(str.hasNext())
		{
			// Get the next argument string...
			String arg = str.next();
			
			// Check!
			if(arg.startsWith("--"))
			{
				// -- means: This is a Parameter. Parse the next String as Parameter.
				this.arguments.put(arg.substring(2), str.next_safe(NULL));
			}
			else if(arg.startsWith("+"))
			{
				// + means: This is a Flag.
				this.arguments.put(arg.substring(1), NULL);
			}
		}
	}
	
	/**
	 * Gets the value of the given argument.
	 * May return {@code null}.
	 *
	 * @param key the key
	 * @return the string
	 */
	public String getString(String key)
	{
		return this.arguments.get(key);
	}
	
	/**
	 * Gets the value of the given argument.
	 *
	 * @param key the key
	 * @param def the def
	 * @return The value assigned to the given argument, or {@code def}, if the argument does not exist.
	 */
	public String getString(String key, String def)
	{
		return this.arguments.containsKey(key) ? this.arguments.get(key) : def;
	}
	
	/**
	 * Gets the value of the given argument as integer.
	 *
	 * @param key the key
	 * @param def the def
	 * @return The value assigned to the given argument, or {@code def}, if the argument does not exist.
	 */
	public int getInteger(String key, int def)
	{
		String value = this.getString(key);
		
		if(value == null)
		{
			return def;
		}
		
		return value.startsWith("0x") ? Integer.parseInt(value.substring(2), 16) : Integer.parseInt(value);
	}
	
	/**
	 * Gets the value of the given argument as boolean.
	 *
	 * @param key the key
	 * @param def the def
	 * @return The value assigned to the given argument, or {@code def}, if the argument does not exist.
	 */
	public boolean getBoolean(String key, boolean def)
	{
		String value = this.getString(key);
		
		if(value == null)
		{
			return def;
		}
		
		return Boolean.parseBoolean(value);
	}
	
	/**
	 * Sets the argument.
	 *
	 * @param k the k
	 * @param v the v
	 */
	public void setArgument(String k, String v)
	{
		this.arguments.put(k, v);
	}
	
	/**
	 * Checks for argument.
	 *
	 * @param key the key
	 * @return true, if successful
	 */
	public boolean hasArgument(String key)
	{
		return this.arguments.containsKey(key);
	}
	
}
