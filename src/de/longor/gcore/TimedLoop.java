/*
 * 
 */
package de.longor.gcore;


// TODO: Auto-generated Javadoc
/**
 * The Class TimedLoop.
 */
public class TimedLoop implements Runnable
{
	// the amount of ticks per second
	/** The ticks per second. */
	private int ticksPerSecond;
	
	// if the loop is running
	/** The is running. */
	private boolean isRunning;
	
	// if the loop is paused
	/** The is paused. */
	private boolean isPaused;
	
	// If the timer should sleep if there is nothing to do. (This will disable the 'frame' method!!!)
	/** The sleep. */
	private boolean sleep;
	
	// 
	/** The do_frame. */
	private boolean do_frame;
	
	// 
	/** The do_tick. */
	private boolean do_tick;
	
	// 
	/** The do_second. */
	private boolean do_second;
	
	// total game time at any given point (High Precision Number)
	/** The loop time. */
	private double loopTime = 0;
	
	// get the next Time for ticking
	/** The next time. */
	private double nextTime = System.nanoTime() / 1000000000.0;
	
	// the runtime of the last frame
	/** The last frame time. */
	private double lastFrameTime = 1;
	
	/** The frame count. */
	private int frameCount = 0;
	
	/** The last fp supdate. */
	private double lastFPSupdate = 0;
	
	/** The fps. */
	private int fps = 0;
	
	/** The loop thread. */
	private Thread loopThread;
	
	/** The loop handler. */
	private TimedLoopHandler loopHandler;
	
	/**
	 * Instantiates a new timed loop.
	 *
	 * @param loopHandler the loop handler
	 */
	public TimedLoop(TimedLoopHandler loopHandler)
	{
		this.loopHandler = loopHandler;
		isRunning = false;
		isPaused = false;
		sleep = true;
		
		do_tick = true;
		do_frame = true;
	}
	
	/**
	 * Instantiates a new timed loop.
	 */
	public TimedLoop()
	{
		this.loopHandler = new NullLoopHandler();
		isRunning = false;
		isPaused = false;
		sleep = true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		this.isRunning = true;
		
		onLoopStart();
		
		nextTime = System.nanoTime() / 1000000000.0;
		lastFrameTime = 1;
		
		while (this.isRunning)
		{
			// start time of this frame run
			double startTime = System.nanoTime();
			long start = System.currentTimeMillis();
			
			// convert the time to seconds
			double currTime = System.nanoTime() / 1000000000.0;
			
			if (currTime >= nextTime)
			{
				// assign the time for the next update
				nextTime += 1D / (double)this.ticksPerSecond;
				
				if(do_tick)
				{
					inf_tick();
					
					if(!isPaused)
						this.tick();
				}
			}
			else
			{
				if(sleep)
				{
					// calculate the time to sleep
					int sleepTime = (int) (1000.0 * (nextTime - currTime));
					
					// sanity check
					if (sleepTime > 0 && sleepTime < 10)
					{
						// sleep until the next update
						try
						{
							Thread.sleep(sleepTime);
						}
						catch (InterruptedException e)
						{
							// do nothing
						}
					}
				}
			}
			
			if(!sleep)
			{
				if(!isPaused && do_frame)
				{
					float interpolation = Math.min(1.0f, (float) ((currTime - lastFrameTime) / (1D / (double)this.ticksPerSecond)));
					this.inf_frame(interpolation);
					
					if(!isPaused)
						this.frame(interpolation);
				}
				
				if((start - lastFPSupdate) >= 1000)
				{
					lastFPSupdate = start;
					fps = frameCount > 9999 ? 9999 : frameCount;
					frameCount = 0;
					
					if(do_second)
					{
						inf_second();
						
						if(!isPaused)
							second();
					}
				}
			}
			
			lastFrameTime = (System.nanoTime() - startTime) / 1000000000.0;
			loopTime += lastFrameTime;
			frameCount++;
		}
		
		onLoopStop();
		
	}
	
	/**
	 * On loop start.
	 */
	public void onLoopStart()
	{
		loopHandler.onLoopStart();
	}
	
	/**
	 * On loop pause.
	 */
	public void onLoopPause()
	{
		loopHandler.onLoopPause();
	}
	
	/**
	 * On loop unpause.
	 */
	public void onLoopUnpause()
	{
		loopHandler.onLoopUnpause();
	}
	
	/**
	 * On loop stop.
	 */
	public void onLoopStop()
	{
		loopHandler.onLoopStop();
	}
	
	/**
	 * Second.
	 */
	public void second()
	{
		loopHandler.second();
	}
	
	/**
	 * Loop.
	 *
	 * @param variableDelta the variable delta
	 */
	public void loop(float variableDelta)
	{
		loopHandler.loop(variableDelta);
	}
	
	/**
	 * Tick.
	 */
	public void tick()
	{
		loopHandler.tick();
	}
	
	/**
	 * Frame.
	 *
	 * @param interpolation the interpolation
	 */
	public void frame(float interpolation)
	{
		loopHandler.frame(interpolation);
	}
	
	/**
	 * Inf_loop.
	 *
	 * @param variableDelta the variable delta
	 */
	public void inf_loop(float variableDelta)
	{
		loopHandler.inf_loop(variableDelta);
	}
	
	/**
	 * Inf_tick.
	 */
	public void inf_tick()
	{
		loopHandler.inf_tick();
	}
	
	/**
	 * Inf_frame.
	 *
	 * @param interpolation the interpolation
	 */
	public void inf_frame(float interpolation)
	{
		loopHandler.inf_frame(interpolation);
	}
	
	/**
	 * Inf_second.
	 */
	public void inf_second()
	{
		loopHandler.inf_second();
	}
	
	/**
	 * Gets the loop time.
	 *
	 * @return the loop time
	 */
	public double getLoopTime()
	{
		return loopTime;
	}
	
	/**
	 * Gets the ticks per second.
	 *
	 * @return the ticks per second
	 */
	public int getTicksPerSecond()
	{
		return ticksPerSecond;
	}
	
	/**
	 * Gets the fps.
	 *
	 * @return the fps
	 */
	public int getFPS()
	{
		return fps;
	}
	
	/**
	 * Gets the last frame time.
	 *
	 * @return the last frame time
	 */
	public double getLastFrameTime()
	{
		return lastFrameTime;
	}
	
	/**
	 * Checks if is running.
	 *
	 * @return true, if is running
	 */
	public boolean isRunning()
	{
		return isRunning;
	}
	
	/**
	 * Checks if is paused.
	 *
	 * @return true, if is paused
	 */
	public boolean isPaused()
	{
		return isPaused;
	}
	
	/**
	 * Sets the tick rate.
	 *
	 * @param tickRate the new tick rate
	 */
	public void setTickRate(int tickRate)
	{
		if(tickRate < 1)
		{
			throw new IllegalArgumentException("A loop can not run at 0 or less ticks per second!");
		}
		else if(tickRate > 1000)
		{
			throw new IllegalArgumentException("A loop should not run at more than 1000 ticks per second!");
		}
		
		this.ticksPerSecond = tickRate;
	}
	
	/**
	 * Sets the sleeping.
	 *
	 * @param b the new sleeping
	 */
	public void setSleeping(boolean b)
	{
		this.sleep = b;
	}
	
	/**
	 * Sets the timers.
	 *
	 * @param tick the tick
	 * @param frame the frame
	 * @param second the second
	 */
	public void setTimers(boolean tick, boolean frame, boolean second)
	{
		do_tick = tick;
		do_frame = frame;
		do_second = second;
	}
	
	/**
	 * Stop loop.
	 */
	public void stopLoop()
	{
		isRunning = false;
	}
	
	/**
	 * Pause loop.
	 */
	public void pauseLoop()
	{
		isPaused = true;
		onLoopPause();
	}
	
	/**
	 * Unpause loop.
	 */
	public void unpauseLoop()
	{
		isPaused = false;
		onLoopUnpause();
	}
	
	/**
	 * Run as thread.
	 *
	 * @param threadName the thread name
	 * @param threadPriority the thread priority
	 * @param isDeamon the is deamon
	 */
	public void runAsThread(String threadName, int threadPriority, boolean isDeamon)
	{
		if(loopThread != null)
		{
			throw new IllegalStateException("Loop is already running.");
		}
		
		// Ready...
		loopThread = new Thread(this);
		
		// Set...
		loopThread.setName(threadName);
		loopThread.setPriority(threadPriority);
		loopThread.setDaemon(isDeamon);
		
		// GO!
		loopThread.start();
	}
	
	/**
	 * The Class NullLoopHandler.
	 */
	public static class NullLoopHandler implements TimedLoopHandler
	{
		// Doesn't do anything! This is pretty much a 'LoopHandlerAdapter'-class, just like the 'WindowAdapter'-class.
	}
	
}
