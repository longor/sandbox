/*
 * 
 */
package de.longor.gcore;

// TODO: Auto-generated Javadoc
/**
 * The Class TimedTask.
 */
public abstract class TimedTask
{
	
	/** The time until execution. */
	float timeUntilExecution;
	
	/** The initial time. */
	float initialTime;
	
	/** The loop. */
	boolean loop;
	
	/** The done. */
	boolean done;
	
	/**
	 * Instantiates a new timed task.
	 *
	 * @param timeUntilExecution the time until execution
	 * @param loop the loop
	 */
	public TimedTask(float timeUntilExecution, boolean loop)
	{
		this.initialTime = timeUntilExecution;
		this.timeUntilExecution = timeUntilExecution;
		this.loop = loop;
	}
	
	/**
	 * Loop update.
	 *
	 * @param variableTimeDelta the variable time delta
	 */
	public void loopUpdate(float variableTimeDelta)
	{
		timeUntilExecution -= variableTimeDelta;
		
		if(timeUntilExecution <= 0.0f && !done)
		{
			execute();
			
			if(loop)
			{
				timeUntilExecution = initialTime;
			}
			else
			{
				done = true;
			}
		}
	}
	
	/**
	 * Execute.
	 */
	public abstract void execute();
	
}
