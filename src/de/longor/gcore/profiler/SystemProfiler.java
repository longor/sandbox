/*
 *
 */
package de.longor.gcore.profiler;

import org.lwjgl.opengl.GL11;

import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor1996.util.math.MathUtility;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemProfiler.
 */
public class SystemProfiler
{
	
	/** The lines. */
	Dataline[] lines;
	
	/** The last updated. */
	long lastUpdated;
	
	/** The display x. */
	float displayX;
	
	/** The display y. */
	float displayY;
	
	/** The display height. */
	float displayHeight;
	
	/** The display width scale. */
	float displayWidthScale;
	
	/** The happening o meter. */
	boolean happeningOMeter;
	
	/**
	 * Instantiates a new system profiler.
	 */
	public SystemProfiler()
	{
		final int len = 64;
		displayHeight = 16;
		displayWidthScale = 1;
		happeningOMeter = true;
		
		lines = new Dataline[1];
		lines[0] = new MEMORYDataline(len);
		// lines[1] = new CPUDataline(len);
	}
	
	/**
	 * Sets the display height.
	 *
	 * @param height the new display height
	 */
	public void setDisplayHeight(float height)
	{
		displayHeight = height;
	}
	
	/**
	 * Sets the display width scale.
	 *
	 * @param widthScale the new display width scale
	 */
	public void setDisplayWidthScale(float widthScale)
	{
		displayWidthScale = widthScale;
	}
	
	/**
	 * Adds the dataline.
	 *
	 * @param line the line
	 */
	public void addDataline(Dataline line)
	{
		assert line != null : "'line'-parameter can not be null.";
		
		Dataline[] nlines = new Dataline[lines.length+1];
		System.arraycopy(lines, 0, nlines, 0, lines.length);
		lines = nlines;
		lines[lines.length-1] = line;
	}
	
	/**
	 * Update.
	 */
	public void update()
	{
		long currentTime = System.currentTimeMillis();
		long timeDifference = currentTime - lastUpdated;
		
		if(timeDifference >= 100)
		{
			lastUpdated = currentTime;
			updateDatalines();
		}
	}
	
	/**
	 * Update datalines.
	 */
	private void updateDatalines()
	{
		for(Dataline line : lines)
		{
			line.update();
		}
	}

	/**
	 * Draw.
	 */
	public void draw()
	{
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		IVertexDrawer va = VertexDrawerVA.drawer;
		
		for(Dataline line : lines)
		{
			drawDataline(va, line);
		}
		
		if(happeningOMeter)
		{
			va.startDrawing(GL11.GL_LINE_STRIP, true, false, false);
			va.setOriginTranslation(displayX + getDisplayWidth()/2F, displayY + displayHeight + displayHeight/2F, 0);
			va.setVertexColor(1, 1, 1, 0.25f);
			float c = (float) MathUtility.getLoadTimePoint() / 1000F;
			for(int i = 0; i < 16; i++)
			{
				float ox = (float) Math.sin(i * c);
				float oy = (float) Math.cos(i * c);
				ox *= i * 2;
				oy *= i * 2;
				va.vertex(ox, oy, 0);
			}
			va.stopDrawing();
		}
		
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	/**
	 * Draw dataline.
	 *
	 * @param va the va
	 * @param line the line
	 */
	private void drawDataline(IVertexDrawer va, Dataline line)
	{
		va.startDrawing(GL11.GL_LINE_STRIP, true, false, false);
		va.setOriginTranslation(displayX, displayY + displayHeight, 0);
		va.setVertexColor(line.color[0], line.color[1], line.color[2], 0.5F);
		va.vertex(0, 0, 0);
		
		float[] data = line.data;
		float displayHeight = this.displayHeight;
		float x = 0;
		float y = 0;
		
		for(int i = 0; i < data.length; i++)
		{
			x = i;
			x *= displayWidthScale;
			y = (1-data[i]) * displayHeight;
			va.vertex(x, y, 0);
		}
		va.vertex((length()-1)*displayWidthScale, 0, 0);
		
		va.stopDrawing();
	}
	
	/**
	 * The Class Dataline.
	 */
	public static abstract class Dataline
	{
		
		/** The data. */
		float[] data;
		
		/** The color. */
		float[] color;
		
		/**
		 * Instantiates a new dataline.
		 *
		 * @param length the length
		 * @param linecolor the linecolor
		 */
		public Dataline(int length, float[] linecolor)
		{
			data = new float[length];
			color = linecolor;
		}
		
		// newData must be a value between 0 and 1!
		/**
		 * Update.
		 */
		public void update()
		{
			float newData = fetchData();
			System.arraycopy(data, 1, data, 0, data.length - 1);
			data[data.length-1] = newData;
		}
		
		/**
		 * Fetch data.
		 *
		 * @return the float
		 */
		public abstract float fetchData();
	}

	/**
	 * Length.
	 *
	 * @return the int
	 */
	public int length() {
		return lines[0].data.length;
	}

	/**
	 * Gets the display width.
	 *
	 * @return the display width
	 */
	public float getDisplayWidth() {
		return length() * displayWidthScale;
	}

	/**
	 * Gets the display height.
	 *
	 * @return the display height
	 */
	public float getDisplayHeight() {
		return displayHeight;
	}

	/**
	 * Sets the screen position.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public void setScreenPosition(float x, float y)
	{
		displayX = x;
		displayY = y;
	}
	
}
