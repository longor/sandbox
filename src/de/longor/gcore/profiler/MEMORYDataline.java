/*
 * 
 */
package de.longor.gcore.profiler;

import de.longor.gcore.profiler.SystemProfiler.Dataline;

// TODO: Auto-generated Javadoc
/**
 * The Class MEMORYDataline.
 */
public class MEMORYDataline extends Dataline
{
	
	/** The runtime. */
	Runtime runtime;
	
	/**
	 * Instantiates a new MEMORY dataline.
	 *
	 * @param length the length
	 */
	public MEMORYDataline(int length)
	{
		super(length, new float[]{0, 0, 1});
		runtime = Runtime.getRuntime();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.profiler.SystemProfiler.Dataline#fetchData()
	 */
	@Override
	public float fetchData()
	{
		// fetch data
		long totalMemory = runtime.totalMemory();
		long usedMemory = totalMemory - runtime.freeMemory();
		
		// normalize
		float memory = (float)usedMemory / (float)totalMemory;
		
		return memory;
	}
}