/*
 *
 */
package de.longor.gcore.profiler;

import java.lang.management.ManagementFactory;

import com.sun.management.OperatingSystemMXBean;

import de.longor.gcore.profiler.SystemProfiler.Dataline;

// TODO: Auto-generated Javadoc
/**
 * The Class CPUDataline.
 */
public class CPUDataline extends Dataline
{
	
	/** The os bean. */
	OperatingSystemMXBean osBean;
	
	/**
	 * Instantiates a new CPU dataline.
	 *
	 * @param length the length
	 */
	public CPUDataline(int length)
	{
		super(length, new float[]{1, 0, 0});
		osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.profiler.SystemProfiler.Dataline#fetchData()
	 */
	@Override
	public float fetchData()
	{
		if(osBean != null)
		{
			float cpuTime = (float) osBean.getProcessCpuLoad();
			
			if(cpuTime < 0)
				cpuTime = 0;
			if(cpuTime > 1)
				cpuTime = 1;
			
			return cpuTime;
		}
		
		return 0;
	}
}
