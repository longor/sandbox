/*
 * 
 */
package de.longor.gcore.profiler;

import de.longor.gcore.GameLoop;
import de.longor.gcore.profiler.SystemProfiler.Dataline;
import de.longor1996.util.math.MathUtility;

// TODO: Auto-generated Javadoc
/**
 * The Class FPSDataline.
 */
public class FPSDataline extends Dataline
{
	
	/** The loop. */
	private GameLoop loop;
	
	/**
	 * Instantiates a new FPS dataline.
	 *
	 * @param loop the loop
	 * @param length the length
	 */
	public FPSDataline(GameLoop loop, int length)
	{
		super(length, new float[]{1,1,0});
		this.loop = loop;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.profiler.SystemProfiler.Dataline#fetchData()
	 */
	@Override
	public float fetchData()
	{
		return MathUtility.clampFloat(0, 1, (float) (loop.getLastFrameTime() / 0.1D));
	}
	
}
