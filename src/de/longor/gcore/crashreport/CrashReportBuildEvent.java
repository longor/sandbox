/*
 * 
 */
package de.longor.gcore.crashreport;

import de.longor.eventor.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class CrashReportBuildEvent.
 */
public class CrashReportBuildEvent extends Event {
	
	/** The report. */
	public final CrashReport report;
	
	/**
	 * Instantiates a new crash report build event.
	 *
	 * @param rep the rep
	 */
	public CrashReportBuildEvent(CrashReport rep)
	{
		report = rep;
	}
	
}
