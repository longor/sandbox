/*
 * 
 */
package de.longor.gcore.crashreport;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class CrashWindow.
 */
public class CrashWindow
{
	
	/**
	 * Show crash window.
	 *
	 * @param report the report
	 */
	public static final void showCrashWindow(CrashReport report)
	{
		JFrame frame = new JFrame();
		frame.setTitle("Crash Report");
		frame.setSize(800, 500);
		frame.setMinimumSize(new Dimension(320, 240));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setLocationRelativeTo(null);
		
		JTextArea text = new JTextArea();
		StringBuilder builder = new StringBuilder("The application crashed.\n\n");
		report.generateReport(builder);
		text.setText(builder.toString());
		text.setEditable(false);
		text.setWrapStyleWord(true);
		text.setLineWrap(true);
		
		JScrollPane pane = new JScrollPane(text);
		pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		frame.add(pane, BorderLayout.CENTER);
		
		frame.setVisible(true);
	}
	
}
