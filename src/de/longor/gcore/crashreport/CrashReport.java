/*
 * 
 */
package de.longor.gcore.crashreport;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class CrashReport.
 */
public class CrashReport
{
	
	/** The objects. */
	List<Object> objects;
	
	/** The exceptions. */
	List<Throwable> exceptions;
	
	/**
	 * Instantiates a new crash report.
	 */
	public CrashReport()
	{
		exceptions = new ArrayList<Throwable>();
		objects = new ArrayList<Object>();
	}
	
	/**
	 * Adds the exception.
	 *
	 * @param e the e
	 * @return the crash report
	 */
	public CrashReport addException(Throwable e)
	{
		if(exceptions.contains(e))
			return this;
		
		exceptions.add(e);
		
		if(e.getCause() != null)
		{
			addException(e);
		}
		
		return this;
	}
	
	/**
	 * Adds the object.
	 *
	 * @param object the object
	 * @return the crash report
	 */
	public CrashReport addObject(Object object)
	{
		objects.add(object);
		return this;
	}
	
	/**
	 * Generate report.
	 *
	 * @param out the out
	 */
	public void generateReport(StringBuilder out)
	{
		out.append("    Stacktrace(s)\n");
		out.append("==================================================\n\n");
		for(Throwable th : exceptions)
		{
			out.append(stacktraceToString(th));
			out.append("\n\n");
		}
		out.append("\n");
		
		out.append("    Objects\n");
		out.append("==================================================\n\n");
		for(Object obj : objects)
		{
			out.append(obj.toString());
			out.append("\n");
		}
	}

	/**
	 * Stacktrace to string.
	 *
	 * @param th the th
	 * @return the string
	 */
	private String stacktraceToString(Throwable th)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		th.printStackTrace(pw);
		pw.close();
		
		try {
			sw.close();
		} catch (IOException e) {
			// This is NEVER going to happen!
		}
		
		return sw.toString();
	}
	
	/**
	 * Creates the.
	 *
	 * @param e the e
	 * @return the crash report
	 */
	public static CrashReport create(Throwable e)
	{
		CrashReport report = new CrashReport();
		report.addException(e);
		return report;
	}
	
}
