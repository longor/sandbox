/*
 * 
 */
package de.longor.sandbox.world;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;

import de.longor.gcore.graphics.MatrixStack;
import de.longor.sandbox.world.bullet.BulletDebugDrawer;
import de.longor.sandbox.world.bullet.BulletPhyComponent;

// TODO: Auto-generated Javadoc
/**
 * The Class DebugRenderSystem.
 */
public class DebugRenderSystem extends EntityProcessingSystem
{
	
	/** The pm. */
	@Mapper ComponentMapper<BulletPhyComponent> pm;
	
	/** The matrix stack. */
	MatrixStack matrixStack = null;
	
	/**
	 * Instantiates a new debug render system.
	 */
	@SuppressWarnings("unchecked")
	public DebugRenderSystem()
	{
		super(Aspect.getAspectForAll(BulletPhyComponent.class));
	}
	
	/* (non-Javadoc)
	 * @see com.artemis.systems.EntityProcessingSystem#process(com.artemis.Entity)
	 */
	@Override
	protected void process(Entity e)
	{
		BulletPhyComponent phy = pm.get(e);
		
		if(phy == null)
			return;
		
		BulletDebugDrawer.drawObject(matrixStack, phy.phyBody, phy.isStatic());
	}
	
	/**
	 * Sets the.
	 *
	 * @param matrixStack the matrix stack
	 */
	public void set(MatrixStack matrixStack)
	{
		this.matrixStack = matrixStack;
	}
	
}
