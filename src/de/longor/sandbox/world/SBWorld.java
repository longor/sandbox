/*
 *
 */
package de.longor.sandbox.world;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.vecmath.Vector3f;

import org.lwjgl.opengl.GL11;

import com.artemis.Entity;
import com.artemis.World;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.linearmath.Transform;

import de.longor.gcore.graphics.BasicGeometry3D;
import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.MatrixStack;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor.sandbox.Sandbox;
import de.longor.sandbox.world.bullet.BulletDebugDrawer;
import de.longor.sandbox.world.bullet.BulletPhyComponent;
import de.longor.sandbox.world.bullet.BulletUtil;
import de.longor1996.util.ByteBufferUtil;
import de.longor1996.util.math.Vector3F;
import de.longor1996.util.math.Vector4F;

// TODO: Auto-generated Javadoc
/**
 * The Class SBWorld.
 */
public class SBWorld
{

	/** The bullet world collision solver. */
	private SequentialImpulseConstraintSolver bulletWorldCollisionSolver;

	/** The bullet world collision configuration. */
	private DefaultCollisionConfiguration bulletWorldCollisionConfiguration;

	/** The bullet world collision dispatcher. */
	private CollisionDispatcher bulletWorldCollisionDispatcher;

	/** The bullet world broadphase. */
	private DbvtBroadphase bulletWorldBroadphase;

	/** The bullet world. */
	private DiscreteDynamicsWorld bulletWorld;

	/** The removal list. */
	private ArrayList<CollisionObject> removalList;

	/** The artemis world. */
	private World artemisWorld;

	/** The world debug renderer. */
	private DebugRenderSystem worldDebugRenderer;

	/** The sandbox. */
	public Sandbox sandbox;

	/**
	 * Inits the.
	 */
	public void init()
	{
		this.bulletWorldBroadphase = new DbvtBroadphase();
		this.bulletWorldCollisionConfiguration = new DefaultCollisionConfiguration();
		this.bulletWorldCollisionDispatcher = new CollisionDispatcher(this.bulletWorldCollisionConfiguration);
		this.bulletWorldCollisionSolver = new SequentialImpulseConstraintSolver();

		this.bulletWorld = new DiscreteDynamicsWorld(
				this.bulletWorldCollisionDispatcher,
				this.bulletWorldBroadphase,
				this.bulletWorldCollisionSolver,
				this.bulletWorldCollisionConfiguration
		);

		this.bulletWorld.setGravity(new Vector3f(0,-9.8f,0));
		removalList = new ArrayList<CollisionObject>();

		worldDebugRenderer = new DebugRenderSystem();

		artemisWorld = new World();
		artemisWorld.setSystem(worldDebugRenderer, true);
		artemisWorld.initialize();

	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		this.removeAllObjects();

		// Ground
		{
			/*
			final int FS = 500;
			final int HS = 500;
			RigidBody body = BulletUtil.mkBox(0, -HS, 0, 0, 0, 0, FS, HS, FS, 0, true);
			world.addRigidBody(body);
			//*/

			//*
			int rad = 5;
			for(int i = -rad; i < rad; i++)
			for(int j = -rad; j < rad; j++)
			{
				final int S = 50;
				RigidBody body = BulletUtil.mkBox(i*S*2, (-S*2), j*S*2, 0, 0, 0, S, S, S, 0, false);
				addRigidBody(body);
			}
			//*/
		}

		// Floating Ground
		{
			RigidBody body = BulletUtil.mkBox(2, 8, 4, 0, 0, 0, 4, 2, 8, 0, false);
			addRigidBody(body);
		}
	}

	/**
	 * Adds the rigid body.
	 *
	 * @param body the body
	 */
	public void addRigidBody(RigidBody body)
	{
		bulletWorld.addRigidBody(body);

		Entity ent = createEntity();
		BulletPhyComponent phy = new BulletPhyComponent(body);
		ent.addComponent(phy);
		ent.addToWorld();

		body.setUserPointer(ent);
	}

	/**
	 * Creates the entity.
	 *
	 * @return the entity
	 */
	public Entity createEntity()
	{
		return artemisWorld.createEntity();
	}

	/**
	 * Loop update.
	 *
	 * @param variableDelta the variable delta
	 */
	public void loopUpdate(float variableDelta)
	{
		artemisWorld.setDelta(variableDelta);
		artemisWorld.process();
	}

	/**
	 * Tick update.
	 *
	 * @param paused the paused
	 */
	public void tickUpdate(boolean paused)
	{
		{
			CollisionObject object = null;
			// int toRemove = (int) Math.ceil(removalList.size() / 20f);
			// for(int I = 0; I < toRemove && !removalList.isEmpty(); I++)
			while(!removalList.isEmpty())
			{
				synchronized (this)
				{
					object = removalList.remove(removalList.size()-1);
					bulletWorld.removeCollisionObject(object);
				}

				Object usrobj = object.getUserPointer();
				if(usrobj != null && usrobj instanceof Entity)
				{
					artemisWorld.deleteEntity((Entity) usrobj);
				}
			}
		}

		// This loop will take a LOT of time! MultiThread it?
		Transform t = new Transform();
		for(CollisionObject object : bulletWorld.getCollisionObjectArray())
		{
			object.getInterpolationWorldTransform(t);

			if(t.origin.y < -sandbox.deletionDepth)
			{
				removeObject(object);
			}
		}

		if(paused)
		{

		}
		else
		{
			bulletWorld.stepSimulation(1F / 40F, 10);
		}
	}

	/** The Constant SUN. */
	private static final Vector4F SUN = new Vector4F(1,1,0.25f, 0f).doNormalize().setW(0f);

	/** The Constant lightBuffer. */
	private static final FloatBuffer lightBuffer = ByteBufferUtil.wrap(SUN.x, SUN.y, SUN.z, 0);

	/**
	 * Render world.
	 *
	 * @param matrixStack the matrix stack
	 */
	public void renderWorld(MatrixStack matrixStack)
	{
		GL11.glLineWidth(0.5f);
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, SBWorld.lightBuffer); //Position The Light

		BulletDebugDrawer.drawAnything = true;
		BulletDebugDrawer.drawCollisionMesh = true;
		BulletDebugDrawer.drawOrigin = false;
		
		Vector3F camPos = new Vector3F();
		sandbox.camera.getPosition(camPos);
		
		// GL11.glEnable(GL11.GL_CULL_FACE);
		sandbox.shaderSimple.doUseProgram();
		worldDebugRenderer.set(matrixStack);
		worldDebugRenderer.process();
		sandbox.shaderSimple.doNotUseProgram();
		matrixStack.uploadAsModelViewMatrix();
		GL11.glDisable(GL11.GL_CULL_FACE);

		if(Boolean.FALSE)
		{
			Vector3f min = new Vector3f();
			Vector3f max = new Vector3f();
			bulletWorldBroadphase.getBroadphaseAabb(min, max);
			GL11.glEnable(GL11.GL_BLEND);
			IVertexDrawer va = VertexDrawerVA.drawer;
			va.startDrawing(GL11.GL_LINES, true, false, false);
			va.setVertexColor(1, 1, 1, 0.1f);
			BasicGeometry3D.drawWireframeBox(va, min.x, min.y, min.z, max.x, max.y, max.z);
			va.stopDrawing();
			GL11.glDisable(GL11.GL_BLEND);
		}
	}

	/**
	 * Gets the phy world.
	 *
	 * @return the phy world
	 */
	public DynamicsWorld getPhyWorld() {
		return bulletWorld;
	}

	/**
	 * Gets the ECS world.
	 *
	 * @return the ECS world
	 */
	public World getECSWorld() {
		return artemisWorld;
	}

	/**
	 * Removes the object.
	 *
	 * @param object the object
	 */
	public synchronized void removeObject(Object object)
	{
		if(object == null)
		{
			throw new IllegalArgumentException("Cannot remove 'null', as it points to no object.");
		}

		if(object instanceof CollisionObject)
		{
			CollisionObject cobj = (CollisionObject) object;

			removalList.add(cobj);
		}
	}

	/**
	 * Activate objects.
	 *
	 * @param min the min
	 * @param max the max
	 */
	public void activateObjects(Vector3f min, Vector3f max)
	{
		Transform t = new Transform();
		for(CollisionObject object : bulletWorld.getCollisionObjectArray())
		{
			object.getInterpolationWorldTransform(t);

			boolean isInside = false;
			{
				float x = t.origin.x;
				float y = t.origin.y;
				float z = t.origin.z;

				if(x >= min.x && y >= min.y && z >= min.z)
				{
					if(x <= max.x && y <= max.y && z <= max.z)
					{
						isInside = true;
					}
				}
			}

			if(isInside)
			{
				object.activate(true);
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return "de.longor.sandbox.World:{objects:"+bulletWorld.getNumCollisionObjects()+"}";
	}

	/**
	 * Removes the all objects.
	 */
	public void removeAllObjects()
	{
		bulletWorld.clearForces();
		for(CollisionObject object : bulletWorld.getCollisionObjectArray())
			removeObject(object);
	}

	/**
	 * Gets the phy world object count.
	 *
	 * @return the phy world object count
	 */
	public int getPhyWorldObjectCount()
	{
		return bulletWorld.getNumCollisionObjects();
	}

	/**
	 * Gets the ECS world object count.
	 *
	 * @return the ECS world object count
	 */
	public int getECSWorldObjectCount()
	{
		return artemisWorld.getEntityManager().getActiveEntityCount();
	}

}
