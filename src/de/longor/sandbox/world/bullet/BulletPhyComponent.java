/*
 * 
 */
package de.longor.sandbox.world.bullet;

import com.artemis.Component;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;

// TODO: Auto-generated Javadoc
/**
 * The Class BulletPhyComponent.
 */
public class BulletPhyComponent extends Component
{
	
	/** The phy body. */
	public final RigidBody phyBody;
	
	/** The phy shape. */
	public final CollisionShape phyShape;
	
	/** The is static. */
	final boolean isStatic;
	
	/**
	 * Instantiates a new bullet phy component.
	 *
	 * @param body the body
	 */
	public BulletPhyComponent(RigidBody body)
	{
		phyBody = body;
		phyShape = body.getCollisionShape();
		isStatic = body.getInvMass() == 0.0;
	}
	
	/**
	 * Checks if is static.
	 *
	 * @return true, if is static
	 */
	public boolean isStatic()
	{
		return isStatic;
	}
	
}
