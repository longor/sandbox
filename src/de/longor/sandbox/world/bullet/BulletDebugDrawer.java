/*
 * 
 */
package de.longor.sandbox.world.bullet;

import javax.vecmath.Vector3f;

import org.lwjgl.opengl.GL11;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CapsuleShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.collision.shapes.StaticPlaneShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;

import de.longor.gcore.graphics.BasicGeometry3D;
import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.MatrixStack;
import de.longor.gcore.graphics.VertexDrawerIM;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor.gcore.graphics.textures.ITexture;
import de.longor1996.util.math.Color;
import de.longor1996.util.math.Matrix4F;

// TODO: Auto-generated Javadoc
/**
 * The Class BulletDebugDrawer.
 */
public class BulletDebugDrawer
{
	
	/** The draw origin. */
	public static boolean drawOrigin = false;
	
	/** The draw collision mesh. */
	public static boolean drawCollisionMesh = true;
	
	/** The draw anything. */
	public static boolean drawAnything = true;
	
	/** The draw box outline. */
	public static boolean drawBoxOutline = false;
	
	/** The texture. */
	public static ITexture texture;
	
	/** The Constant SCRAP0. */
	private static final Vector3f SCRAP0 = new Vector3f();
	
	/** The Constant SCRAP_TRANSFORM. */
	private static final Transform SCRAP_TRANSFORM = new Transform();
	
	/** The Constant SCRAP_MATRIX0. */
	private static final float[] SCRAP_MATRIX0 = new float[16];
	
	/** The Constant SCRAP_MATRIX1. */
	private static final Matrix4F SCRAP_MATRIX1 = Matrix4F.newInstance();
	
	/**
	 * Draw shape.
	 *
	 * @param shape the shape
	 * @param object the object
	 * @param isStatic the is static
	 */
	public static void drawShape(CollisionShape shape, CollisionObject object, boolean isStatic)
	{
		IVertexDrawer vertexer = VertexDrawerIM.drawer;
		
		if(shape instanceof SphereShape)
		{
			SphereShape sphere = (SphereShape) shape;
			float rad = sphere.getRadius();
			texture.bind();
			
			vertexer.startDrawing(GL11.GL_TRIANGLES, true, true, true);
			vertexer.setVertexColor(0, 1, 0, 1);
			BasicGeometry3D.drawSphere(vertexer, rad, true);
			vertexer.stopDrawing();
		}
		else if(shape instanceof CapsuleShape)
		{
			/*
			CapsuleShape capsule = (CapsuleShape) shape;
			
			float radious = capsule.getRadius();
			float halfHeight = capsule.getHalfHeight() + radious;
			
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
			AABBRenderer.setRGBA(1, 1, 1, 1);
			AABBRenderer.drawAABBce(null, new Vector3F(-radious, -halfHeight, -radious));
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
			//*/
		}
		else if(shape instanceof BoxShape)
		{
			BoxShape box = (BoxShape) shape;
			Vector3f extents = box.getHalfExtentsWithMargin(SCRAP0);
			float hwidth = extents.x;
			float hheight = extents.y;
			float hlength = extents.z;
			float width = hwidth * 2f;
			float height = hheight * 2f;
			float length = hlength * 2f;
			float volume = width*height*length;
			
			if(texture != null)
			{
				if(!isStatic)
				{
					texture.bind();
					vertexer.startDrawing(GL11.GL_QUADS, true, true, false);
					vertexer.setVertexColor(0f, 1f, 0f, 1f);
					// vertexer.setVertexColor(0.5f, 0.75f, 0.5f, 1f);
					BasicGeometry3D.drawTexturedBox(vertexer, width, height, length, true);
					vertexer.stopDrawing();
					texture.unbind();
					
					if(drawBoxOutline)
					{
						vertexer.startDrawing(GL11.GL_LINES, true, false, false);
						vertexer.setVertexColor(0f, 1f, 0f, 1f);
						BasicGeometry3D.drawWireframeBox(vertexer, width+0.05f, height+0.05f, length+0.05f, true);
						vertexer.stopDrawing();
					}
				}
				else
				{
					texture.bind();
					vertexer.startDrawing(GL11.GL_QUADS, true, true, true);
					
					Color color = selectColorByVolume(volume);
					vertexer.setVertexColor(color.r, color.g, color.b, 1f);
					
					BasicGeometry3D.setTextureMultiplier(volume >= 1000 ? 0.1f : 1f);
					BasicGeometry3D.drawTexturedBox(vertexer, width, height, length, true);
					BasicGeometry3D.setTextureMultiplier(1f);
					vertexer.stopDrawing();
					texture.unbind();
					
					if(drawBoxOutline)
					{
						vertexer.startDrawing(GL11.GL_LINES, true, false, false);
						vertexer.setVertexColor(0f, 0f, 0f, 1f);
						BasicGeometry3D.drawWireframeBox(vertexer, width+0.05f, height+0.05f, length+0.05f, true);
						vertexer.stopDrawing();
					}
				}
			}
			else
			{
				vertexer.startDrawing(GL11.GL_LINES, true, false, false);
				vertexer.setVertexColor(0f, 0f, 0f, 1f);
				BasicGeometry3D.drawWireframeBox(vertexer, width+0.05f, height+0.05f, length+0.05f, true);
				vertexer.stopDrawing();
			}
		}
		else if((shape instanceof StaticPlaneShape))
		{
			vertexer.startDrawing(GL11.GL_LINES, true, false, false);
			vertexer.setVertexColor(1f, 1f, 1f, 0.1f);
			BasicGeometry3D.drawGrid(vertexer, 512, 1f);
			vertexer.stopDrawing();
		}
	}
	
	/** The Constant SIZE0. */
	private static final float SIZE0 = (float) Math.pow(5, 3);
	
	/** The Constant SIZE1. */
	private static final float SIZE1 = (float) Math.pow(128, 3);
	
	/** The Constant SIZE2. */
	private static final float SIZE2 = (float) Math.pow(1024, 3);
	
	/**
	 * Select color by volume.
	 *
	 * @param volume the volume
	 * @return the color
	 */
	private static Color selectColorByVolume(float volume)
	{
		if(volume < SIZE0)
			return Color.DARK_GRAY;
		if(volume < SIZE1)
			return Color.GRAY;
		if(volume < SIZE2)
			return Color.LIGHT_GRAY;
		
		return Color.WHITE;
	}
	
	/** The t. */
	private static Transform t = SCRAP_TRANSFORM;
	
	/** The matrix. */
	private static float[] matrix = SCRAP_MATRIX0;
	
	/**
	 * Draw object.
	 *
	 * @param matrixStack the matrix stack
	 * @param object the object
	 * @param isStatic the is static
	 */
	public static void drawObject(MatrixStack matrixStack, CollisionObject object, boolean isStatic)
	{
		if(!drawAnything)
			return;
		
		object.getInterpolationWorldTransform(t);
		t.getOpenGLMatrix(matrix);
		
		matrixStack.glPushMatrix();
		matrixStack.glMultiply(SCRAP_MATRIX1.setFromFloatArray(matrix));
		matrixStack.uploadAsModelViewMatrix();
		{
			if(drawOrigin)
			{
				IVertexDrawer vertexer = VertexDrawerVA.drawer;
				vertexer.startDrawing(GL11.GL_LINES, true, false, false);
				vertexer.setOriginTranslation(0, 0, 0);
				vertexer.setVertexColor(0, 1, 0, 1);
				vertexer.vertex(0, 0, 0);
				vertexer.vertex(1, 0, 0);
				vertexer.setVertexColor(0, 0, 1, 1);
				vertexer.vertex(0, 0, 0);
				vertexer.vertex(0, 1, 0);
				vertexer.setVertexColor(1, 0, 0, 1);
				vertexer.vertex(0, 0, 0);
				vertexer.vertex(0, 0, 1);
				vertexer.stopDrawing();
			}
			
			if(drawCollisionMesh)
			{
				CollisionShape shape = object.getCollisionShape();
				BulletDebugDrawer.drawShape(shape, object, isStatic);
			}
		}
		matrixStack.glPopMatrix();
	}

	/**
	 * Draw objects.
	 *
	 * @param matrixStack the matrix stack
	 * @param collisionObjectArray the collision object array
	 */
	public static void drawObjects(MatrixStack matrixStack, ObjectArrayList<CollisionObject> collisionObjectArray)
	{
		if(!drawAnything)
			return;
		
		Transform t = SCRAP_TRANSFORM;
		float[] matrix = SCRAP_MATRIX0;
		boolean isStatic;
		
		for(CollisionObject object : collisionObjectArray)
		{
			object.getInterpolationWorldTransform(t);
			t.getOpenGLMatrix(matrix);
			isStatic = (object instanceof RigidBody) ? (((RigidBody) object).getInvMass() == 0.0) : false;
			
			matrixStack.glPushMatrix();
			matrixStack.glMultiply(SCRAP_MATRIX1.setFromFloatArray(matrix));
			matrixStack.uploadAsModelViewMatrix();
			{
				if(drawOrigin)
				{
					IVertexDrawer vertexer = VertexDrawerVA.drawer;
					vertexer.startDrawing(GL11.GL_LINES, true, false, false);
					vertexer.setOriginTranslation(0, 0, 0);
					vertexer.setVertexColor(0, 1, 0, 1);
					vertexer.vertex(0, 0, 0);
					vertexer.vertex(1, 0, 0);
					vertexer.setVertexColor(0, 0, 1, 1);
					vertexer.vertex(0, 0, 0);
					vertexer.vertex(0, 1, 0);
					vertexer.setVertexColor(1, 0, 0, 1);
					vertexer.vertex(0, 0, 0);
					vertexer.vertex(0, 0, 1);
					vertexer.stopDrawing();
				}
				
				if(drawCollisionMesh)
				{
					CollisionShape shape = object.getCollisionShape();
					BulletDebugDrawer.drawShape(shape, object, isStatic);
				}
			}
			matrixStack.glPopMatrix();
		}
		matrixStack.uploadAsModelViewMatrix();
	}

}
