/*
 * 
 */
package de.longor.sandbox.world.bullet;

import java.util.HashMap;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;

import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.Quaternion;
import de.longor1996.util.math.Vector3F;

// TODO: Auto-generated Javadoc
/**
 * The Class BulletUtil.
 */
public class BulletUtil
{
	
	/** The Constant shapeCache. */
	private static final HashMap<String, CollisionShape> shapeCache = new HashMap<String, CollisionShape>();
	
	/**
	 * Gets the shape cache size.
	 *
	 * @return the shape cache size
	 */
	public static int getShapeCacheSize()
	{
		return shapeCache.size();
	}
	
	/**
	 * Clear shape cache.
	 */
	public static void clearShapeCache()
	{
		shapeCache.clear();
	}
	
	/**
	 * Mk box.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param yaw the yaw
	 * @param pitch the pitch
	 * @param roll the roll
	 * @param halfwidth the halfwidth
	 * @param halfheight the halfheight
	 * @param halflength the halflength
	 * @param mass the mass
	 * @param centered the centered
	 * @return the rigid body
	 */
	public static final RigidBody mkBox(
			float x, float y, float z,
			float yaw, float pitch, float roll,
			float halfwidth, float halfheight, float halflength,
			float mass, boolean centered
	)
	{
		DefaultMotionState fallMotionState = null;
		
		if(centered)
		{
			fallMotionState = BulletUtil.mkMotionState(
					x, y, z,
					yaw, pitch, roll
			);
		}
		else
		{
			fallMotionState = BulletUtil.mkMotionState(
					x+halfwidth, y+halfheight, z+halflength,
					yaw, pitch, roll
			);
		}
		
		CollisionShape bigboxShape = cache_box(halfwidth, halfheight, halflength);
		
		Vector3f localInertia = new Vector3f(0, 0, 0);
		bigboxShape.calculateLocalInertia(mass, localInertia);
		
		RigidBodyConstructionInfo fallRigidBodyCI = new RigidBodyConstructionInfo(mass, fallMotionState, bigboxShape, localInertia);
        RigidBody body = new RigidBody(fallRigidBodyCI);
        
        return body;
	}
	
	/**
	 * Cache_box.
	 *
	 * @param halfwidth the halfwidth
	 * @param halfheight the halfheight
	 * @param halflength the halflength
	 * @return the collision shape
	 */
	private static CollisionShape cache_box(float halfwidth, float halfheight, float halflength)
	{
		halfwidth = MathUtility.round(halfwidth, 100);
		halfheight = MathUtility.round(halfheight, 100);
		halflength = MathUtility.round(halflength, 100);
		
		String S = "box:" + halfwidth + "/" + halfheight + "/" + halflength;
		
		CollisionShape shape = shapeCache.get(S);
		
		if(shape == null)
		{
			shape = new BoxShape(new Vector3f(halfwidth, halfheight, halflength));
			shapeCache.put(S, shape);
		}
		
		return shape;
	}
	
	/**
	 * Mk sphere.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param radious the radious
	 * @param mass the mass
	 * @return the rigid body
	 */
	public static final RigidBody mkSphere(
			float x, float y, float z, float radious, float mass
	)
	{
		DefaultMotionState fallMotionState = BulletUtil.mkMotionState(
				x, y, z,
				0, 0, 0
		);
		
		CollisionShape bigboxShape = cache_sphere(radious);
		
		Vector3f localInertia = new Vector3f(0, 0, 0);
		bigboxShape.calculateLocalInertia(mass, localInertia);
		
		RigidBodyConstructionInfo fallRigidBodyCI = new RigidBodyConstructionInfo(mass, fallMotionState, bigboxShape, localInertia);
        RigidBody body = new RigidBody(fallRigidBodyCI);
        
        return body;
	}
	
	/**
	 * Cache_sphere.
	 *
	 * @param radious the radious
	 * @return the collision shape
	 */
	private static CollisionShape cache_sphere(float radious)
	{
		radious = MathUtility.round(radious, 100);
		
		String S = "sphere:" + radious;
		
		CollisionShape shape = shapeCache.get(S);
		
		if(shape == null)
		{
			shape = new SphereShape(radious);
			shapeCache.put(S, shape);
		}
		
		return shape;
	}

	/**
	 * Mk motion state.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param yaw the yaw
	 * @param pitch the pitch
	 * @param roll the roll
	 * @return the default motion state
	 */
	public static final DefaultMotionState mkMotionState(float x, float y, float z, float yaw, float pitch, float roll)
	{
		Quat4f rotation = new Quat4f(BulletUtil.cast(new Quaternion().rotateDegree(yaw, pitch, roll)));
		Vector3f position = new Vector3f(x,y,z);
		
		return new DefaultMotionState(mkTransform(rotation, position));
	}
	
	/**
	 * Mk transform.
	 *
	 * @param quat4f the quat4f
	 * @param vector3f the vector3f
	 * @return the transform
	 */
	public static final Transform mkTransform(Quat4f quat4f, Vector3f vector3f)
	{
		Transform t = new Transform();
		t.setRotation(quat4f);
		t.origin.set(vector3f);
		return t;
	}
	
	/**
	 * Mk transform.
	 *
	 * @param x the x
	 * @param y the y
	 * @param z the z
	 * @param yaw the yaw
	 * @param pitch the pitch
	 * @param roll the roll
	 * @return the transform
	 */
	public static final Transform mkTransform(float x, float y, float z, float yaw, float pitch, float roll)
	{
		Quat4f rotation = new Quat4f(BulletUtil.cast(new Quaternion().rotateDegree(yaw, pitch, roll)));
		Vector3f position = new Vector3f(x,y,z);
		
		return (mkTransform(rotation, position));
	}
	
	
	
	
	
	
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @return the vector3 f
	 */
	public static final Vector3F cast(Vector3f vin)
	{
		return new Vector3F(vin.x,vin.y,vin.z);
	}
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @param out the out
	 * @return the vector3 f
	 */
	public static final Vector3F cast(Vector3f vin, Vector3F out)
	{
		return out.set(vin.x,vin.y,vin.z);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Cast.
	 *
	 * @param qin the qin
	 * @return the quat4f
	 */
	public static final Quat4f cast(Quaternion qin)
	{
		return new Quat4f(qin.x,qin.y,qin.z,qin.w);
	}
	
	/**
	 * Cast.
	 *
	 * @param qin the qin
	 * @param out the out
	 * @return the quat4f
	 */
	public static final Quat4f cast(Quaternion qin, Quat4f out)
	{
		out.set(qin.x,qin.y,qin.z,qin.w);
		return out;
	}
	
	/**
	 * Cast.
	 *
	 * @param qin the qin
	 * @return the quaternion
	 */
	public static final Quaternion cast(Quat4f qin)
	{
		return new Quaternion(qin.x,qin.y,qin.z,qin.w);
	}
	
	/**
	 * Cast.
	 *
	 * @param qin the qin
	 * @param out the out
	 * @return the quaternion
	 */
	public static final Quaternion cast(Quat4f qin, Quaternion out)
	{
		return out.set(qin.x,qin.y,qin.z,qin.w);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @return the vector3f
	 */
	public static Vector3f cast(Vector3F vin)
	{
		return new Vector3f(vin.x,vin.y,vin.z);
	}
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @param out the out
	 * @return the vector3f
	 */
	public static Vector3f cast(Vector3F vin, Vector3f out)
	{
		out.set(vin.x, vin.y, vin.z);
		return out;
	}
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @param scale the scale
	 * @return the vector3f
	 */
	public static Vector3f cast(Vector3F vin, float scale)
	{
		return new Vector3f(vin.x*scale,vin.y*scale,vin.z*scale);
	}
	
	/**
	 * Cast.
	 *
	 * @param vin the vin
	 * @param out the out
	 * @param scale the scale
	 * @return the vector3f
	 */
	public static Vector3f cast(Vector3F vin, Vector3f out, float scale)
	{
		out.set(vin.x*scale, vin.y*scale, vin.z*scale);
		return out;
	}
	
}
