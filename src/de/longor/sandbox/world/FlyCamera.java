/*
 *
 */
package de.longor.sandbox.world;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import de.longor.gcore.graphics.IGLScreen;
import de.longor.gcore.graphics.camera.ICamera;
import de.longor.gcore.graphics.projection.IProjection;
import de.longor.gcore.input.InputManager;
import de.longor.sandbox.ITickable;
import de.longor.sandbox.Sandbox;
import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.Matrix4F;
import de.longor1996.util.math.Vector3F;

// TODO: Auto-generated Javadoc
/**
 * The Class FlyCamera.
 */
public class FlyCamera implements IProjection ,ICamera, ITickable
{
	
	/** The cam matrix. */
	Matrix4F camMatrix;
	
	/** The rot matrix. */
	Matrix4F rotMatrix;
	
	/** The proj matrix. */
	Matrix4F projMatrix;
	
	/** The input manager. */
	final InputManager inputManager;
	
	/** The screen. */
	final IGLScreen screen;
	
	/** The sandbox. */
	final Sandbox sandbox;
	
	/** The field of view. */
	float fieldOfView;
	
	/** The position. */
	Vector3F position;
	
	/** The rotation. */
	Vector3F rotation;
	
	/** The velocity. */
	Vector3F velocity;
	
	/** The forward. */
	Vector3F forward;
	
	/** The cam speed. */
	float camSpeed;
	
	/** The Lposition. */
	Vector3F Lposition;
	
	/** The Lrotation. */
	Vector3F Lrotation;
	
	/** The interpolated position. */
	Vector3F interpolatedPosition;
	
	/**
	 * Instantiates a new fly camera.
	 *
	 * @param sandbox the sandbox
	 * @param screen the screen
	 * @param input the input
	 */
	public FlyCamera(Sandbox sandbox, IGLScreen screen, InputManager input)
	{
		this.screen = screen;
		this.sandbox = sandbox;
		this.inputManager = input;
		this.fieldOfView = 90f;
		this.camSpeed = 0.5f;
		rotMatrix = Matrix4F.newInstance();
		camMatrix = Matrix4F.newInstance();
		projMatrix = Matrix4F.newInstance();
		
		position = new Vector3F(0,2,0);
		rotation = new Vector3F(0,0,0);
		velocity = new Vector3F(0,0,0);
		forward = new Vector3F(0,0,0);
		
		Lposition = new Vector3F(position);
		Lrotation = new Vector3F(rotation);
		interpolatedPosition = new Vector3F(position);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.sandbox.ITickable#tickUpdate()
	 */
	@Override
	public void tickUpdate()
	{
		Lposition.set(position);
		Lrotation.set(rotation);
		
		if(inputManager.isMouseGrabbed())
		{
			// Mouse Input Processing
			{
				float mx = Mouse.getDX();
				float my = Mouse.getDY();
				
				float speedUD = 0.5f;
				float speedLR = 0.5f;
				
				rotation.x += my * speedUD;
				rotation.y -= mx * speedLR;
				
				rotation.x = MathUtility.clampFloat(-90, +90, rotation.x);
				
				{
					float mw = Mouse.getDWheel();
					
					if(mw < 0)
					{
						camSpeed *= 0.5f;
					}
					
					if(mw > 0)
					{
						camSpeed *= 2f;
					}
					
					camSpeed = MathUtility.clampFloat(0.0625f, 32f, camSpeed);
				}
				
				rotation.y = MathUtility.wrapAround(0f, 360f, rotation.y);
			}
			
			// Forward Vector Recalculation
			{
				forward.x = (float) Math.sin(rotation.y * MathUtility.PI180);
				forward.y = (float) Math.tan(rotation.x * MathUtility.PI180);
				forward.z = (float) Math.cos(rotation.y * MathUtility.PI180);
				forward.doNormalize();
				
			}
			
			// Keyboard Input Processing
			{
				Vector3F movementVector = new Vector3F();
				
				if(inputManager.isKeyDown(Keyboard.KEY_W))
				{
					Vector3F vec = new Vector3F(forward);
					vec.doNormalize();
					movementVector.doAdd(vec);
				}
				if(inputManager.isKeyDown(Keyboard.KEY_S))
				{
					Vector3F vec = new Vector3F(forward);
					vec.doNormalize();
					vec.doMultiplyBy(-1);
					movementVector.doAdd(vec);
				}
				
				if(inputManager.isKeyDown(Keyboard.KEY_A))
				{
					Vector3F vec = Vector3F.getRotationAsVectorOverPI180(0, rotation.y - 90);
					vec.doNormalize();
					vec.doMultiplyBy(-1);
					movementVector.doAdd(vec);
				}
				
				if(inputManager.isKeyDown(Keyboard.KEY_D))
				{
					Vector3F vec = Vector3F.getRotationAsVectorOverPI180(0, rotation.y + 90);
					vec.doNormalize();
					vec.doMultiplyBy(-1);
					movementVector.doAdd(vec);
				}
				
				movementVector.doMultiplyBy(camSpeed);
				velocity.doAdd(movementVector);
			}
		}
		
		position.doAdd(velocity);
		velocity.doMultiplyBy(0.75f);
		
		position.y = MathUtility.clampFloat(-(sandbox.deletionDepth-1), 1000000000F, position.y);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.camera.ICamera#getCameraMatrix(float)
	 */
	@Override
	public Matrix4F getCameraMatrix(float interpolation)
	{
		if(interpolation > 1f)
			interpolation = 1f;
		if(interpolation < 0f)
			interpolation = 0f;
		
		// Rotation Matrix Recalculation
		{
			rotMatrix.setIdentity();
			
			float rotX = MathUtility.linearInterpolation(Lrotation.x, rotation.x, interpolation);
			float rotY = MathUtility.linearInterpolation(Lrotation.y, rotation.y, interpolation);
			
			// PITCH
			Matrix4F.rotateAroundAxisByDegrees(-1 * rotX, Vector3F.AX, rotMatrix, rotMatrix);
			
			// YAW
			Matrix4F.rotateAroundAxisByDegrees(180F-rotY, Vector3F.AY, rotMatrix, rotMatrix);
		}
		
		// Camera Matrix Recalculation
		{
			// By localizing the matrices here, the JVM may pull them into the CPU faster.
			camMatrix.setIdentity();
			
			Vector3F.getLinearInterpolation(Lposition, position, interpolation, interpolatedPosition);
			
			Matrix4F.translate(0,0,1, camMatrix, camMatrix);
			Matrix4F.mul(camMatrix, rotMatrix, camMatrix);
			Matrix4F.translateNegative(interpolatedPosition, camMatrix, camMatrix);
		}
		
		return camMatrix;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.projection.IProjection#getProjectionMatrix(float)
	 */
	@Override
	public Matrix4F getProjectionMatrix(float interpolation)
	{
		projMatrix.setIdentity();
		projMatrix.calculatePerspectiveProjectionMatrix(fieldOfView, screen.getAspectRatio(), 0.1f, 1024f);
		return projMatrix;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.camera.ICamera#getCameraRotationMatrix(float)
	 */
	@Override
	public Matrix4F getCameraRotationMatrix(float interpolation) {
		return rotMatrix;
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.camera.ICamera#getPosition(de.longor1996.util.math.Vector3F)
	 */
	@Override
	public void getPosition(Vector3F out)
	{
		out.set(position);
	}

	/* (non-Javadoc)
	 * @see de.longor.gcore.graphics.camera.ICamera#getForward(de.longor1996.util.math.Vector3F)
	 */
	@Override
	public void getForward(Vector3F out)
	{
		out.set(forward);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "FlyCamera:{position:"+position.toIntString()+",rotation:"+rotation.toIntStringXY() + "}";
	}
	
}
