/*
 * 
 */
package de.longor.sandbox;

// TODO: Auto-generated Javadoc
/**
 * The Interface ITickable.
 */
public interface ITickable
{
	
	/**
	 * Tick update.
	 */
	public void tickUpdate();
	
}
