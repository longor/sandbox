/*
 *
 */
package de.longor.sandbox;

import java.io.IOException;
import java.nio.FloatBuffer;

import javax.vecmath.Vector3f;

import org.bushe.swing.event.annotation.EventSubscriber;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.python.util.PythonInterpreter;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;

import de.longor.eventor.Eventor;
import de.longor.eventor.SystemInitialize;
import de.longor.gcore.ArgsMap;
import de.longor.gcore.GameLoop;
import de.longor.gcore.TimedTask;
import de.longor.gcore.graphics.BasicGeometry2D;
import de.longor.gcore.graphics.BasicGeometry3D;
import de.longor.gcore.graphics.IGLScreen;
import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.MatrixStack;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor.gcore.graphics.camera.ICamera;
import de.longor.gcore.graphics.dprc.GLException;
import de.longor.gcore.graphics.dprc.GLSLShader;
import de.longor.gcore.graphics.font.FontRenderer;
import de.longor.gcore.graphics.projection.IProjection;
import de.longor.gcore.graphics.textures.BasicTexture;
import de.longor.gcore.graphics.textures.ITexture;
import de.longor.gcore.graphics.textures.XPNGTexture;
import de.longor.gcore.input.InputManager;
import de.longor.gcore.input.KeyboardEvent;
import de.longor.gcore.input.MouseEvent;
import de.longor.gcore.resourcemanager.ResourceManager;
import de.longor.sandbox.gui.GUIPanel;
import de.longor.sandbox.gui.GUIRenderer;
import de.longor.sandbox.gui.screens.PauseMenu;
import de.longor.sandbox.world.FlyCamera;
import de.longor.sandbox.world.SBWorld;
import de.longor.sandbox.world.bullet.BulletDebugDrawer;
import de.longor.sandbox.world.bullet.BulletUtil;
import de.longor1996.util.ByteBufferUtil;
import de.longor1996.util.StringUtil;
import de.longor1996.util.concurrent.WorkThreadPool;
import de.longor1996.util.math.Color;
import de.longor1996.util.math.MathUtility;
import de.longor1996.util.math.Matrix4F;
import de.longor1996.util.math.Vector3F;
import de.longor1996.util.math.Vector4F;

// TODO: Auto-generated Javadoc
/**
 * The Class Sandbox.
 */
public class Sandbox
{

	/** The appargs. */
	public ArgsMap appargs;

	/** The eventor. */
	public Eventor eventor;

	/** The gameloop. */
	public GameLoop gameloop;

	/** The resmanager. */
	public ResourceManager resmanager;

	/** The threadpool. */
	public WorkThreadPool threadpool;

	/** The screen. */
	public IGLScreen screen;

	/** The input. */
	public InputManager input;

	/** The font renderer. */
	public FontRenderer fontRenderer;

	/** The matrix stack. */
	public MatrixStack matrixStack = new MatrixStack(4);

	/** The interp. */
	public PythonInterpreter interp;

	/** The callback. */
	public ClosestRayResultCallback callback;

	/** The currently selected object. **/
	public CollisionObject selectedObject;

	/** The gui renderer. */
	public GUIRenderer guiRenderer;

	/** The gui current. */
	public GUIPanel guiCurrent;

	/** The gc task. */
	private TimedTask gcTask;

	/** The grid tex. */
	public ITexture gridTex;

	/** The grid transparent tex. */
	public ITexture gridTransparentTex;

	/** The shader simple. */
	public GLSLShader shaderSimple;

	/** The projection. */
	public IProjection projection;

	/** The camera. */
	public ICamera camera;

	/** The camera tickable. */
	public ITickable cameraTickable;

	/** The world. */
	public SBWorld world;

	/** The raytrace distance. */
	public float raytraceDistance;

	/** The deletion depth. */
	public float deletionDepth = 5000f;

	/** The lighting_diffuse. */
	public FloatBuffer lighting_diffuse;

	/** The lighting_ambient. */
	public FloatBuffer lighting_ambient;

	/** The lighting_direction. */
	public FloatBuffer lighting_direction;

	/** The draw sky. */
	public boolean drawSky = true;

	/** The draw gui. */
	public boolean drawGUI = true;

	/** The draw zero plane grid. */
	public boolean drawZeroPlaneGrid = false;

	/** The draw deletion grid. */
	public boolean drawDeletionGrid = true;

	/** The draw origin marker. */
	public boolean drawOriginMarker = false;

	/** The draw crosshair. */
	public boolean drawCrosshair = true;

	/** The draw debug info. */
	public boolean drawDebugInfo = true;

	/** The draw debug graph. */
	public boolean drawDebugGraph = true;

	/**
	 * Fetch.
	 *
	 * @param evt the evt
	 */
	@EventSubscriber
	public void fetch(SandboxFetchEvent evt)
	{
		evt.sandbox = this;
	}

	/**
	 * Inits the.
	 *
	 * @param event the event
	 */
	@EventSubscriber
	public void init(SystemInitialize event)
	{
		gcTask = new TimedTask(5f, true)
		{
			@Override
			public void execute()
			{
				System.gc();
			}
		};

		{
			Vector3F ldir = new Vector3F(0.737f,0.5f,0.6f).doNormalize();
			lighting_diffuse = ByteBufferUtil.wrap(1f, 1f, 1f, 1f);
			lighting_ambient = ByteBufferUtil.wrap(0.4f, 0.4f, 0.6f, 1f);
			lighting_direction = ByteBufferUtil.wrap(ldir.x, ldir.y, ldir.z, 0);
		}

		fontRenderer = new FontRenderer();
		fontRenderer.buildFont();
		fontRenderer.fontUpdate();

		guiRenderer = new GUIRenderer();
		guiRenderer.setFontRenderer(fontRenderer);
		guiRenderer.setWhitePixel(BasicTexture.WHITEPIXEL);
		setCurrentGUI(PauseMenu.build(this));

		world = new SBWorld();
		world.sandbox = this;
		world.init();

		interp = new PythonInterpreter();
		interp.set("world", world);
		interp.set("bulletutil", new BulletUtil());
		interp.set("mathutil", new MathUtility());
		interp.set("sandbox", this);

		world.reset();

		FlyCamera cam = new FlyCamera(this, screen, input);
		camera = cam;
		projection = cam;
		cameraTickable = cam;

		raytraceDistance = 1024f;
		callback = new ClosestRayResultCallback(new Vector3f(), new Vector3f());

		{
			{
				try {
					shaderSimple = new GLSLShader(ClassLoader.getSystemResourceAsStream("assets/sandbox/shaders/simple.shader"));
					shaderSimple.doLoadIntoOpenGL();
					shaderSimple.doNotUseProgram();
				} catch (GLException e) {
					e.printStackTrace();
				}
			}
			
			try {
				gridTex = new XPNGTexture(false,
						ClassLoader.getSystemResourceAsStream("assets/sandbox/textures/grid_big.xpng")
				);
				gridTransparentTex = new XPNGTexture(false,
						ClassLoader.getSystemResourceAsStream("assets/sandbox/textures/grid_big_transparent.xpng")
				);
			} catch (IOException e) {
				e.printStackTrace();
				event.errors.add(e);
				throw new RuntimeException(e);
			}

			BulletDebugDrawer.texture = gridTex;
		}



		GL11.glEnable(GL11.GL_LIGHTING);
		{
			GL11.glEnable(GL11.GL_LIGHT0);

			GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, lighting_ambient); //Setup The Ambient Light
			GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, lighting_diffuse); //Setup The Diffuse Light
			GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, lighting_direction); //Position The Light
		}
		GL11.glDisable(GL11.GL_LIGHTING);

		{
			GL11.glFog(GL11.GL_FOG_COLOR, ByteBufferUtil.wrap(1, 1, 1, 1));
			GL11.glFogf(GL11.GL_FOG_DENSITY, 0.00025f);
		}

		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_DITHER);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glShadeModel(GL11.GL_SMOOTH);

	}

	/**
	 * Loop update.
	 *
	 * @param variableDelta the variable delta
	 */
	public void loopUpdate(float variableDelta)
	{
		world.loopUpdate(variableDelta);
		gcTask.loopUpdate(variableDelta);
	}

	/**
	 * Tick update.
	 */
	public void tickUpdate()
	{
		input.tickUpdate();
		cameraTickable.tickUpdate();
		raytraceFrom__(camera);
		world.tickUpdate(!input.isMouseGrabbed());
	}

	/**
	 * Key event.
	 *
	 * @param evt the evt
	 */
	@EventSubscriber
	public void keyEvent(KeyboardEvent evt)
	{
		if(!input.isMouseGrabbed())
		{
			// UI Handling
			if(guiCurrent != null)
			{
				guiCurrent.keyEvent(evt);
			}
			return;
		}

		if(evt.keyState)
			return;

		//*
		if(evt.keyCode == Keyboard.KEY_ESCAPE)
		{
			if(guiCurrent == null)
			{
				setCurrentGUI(PauseMenu.build(this));
			}
			return;
		}
		//*/

		if(input.isMouseGrabbed())
		{
			// REMOVE OBJECT
			if(evt.keyCode == Keyboard.KEY_R)
			{
				if(selectedObject != null)
				{
					CollisionObject object = selectedObject;

					// wake up all the other objects around this one
					{
						Transform t = new Transform();
						Vector3f min = new Vector3f(0,0,0);
						Vector3f max = new Vector3f(0,0,0);
						Vector3f ext = new Vector3f(10,10,10);
						object.getInterpolationWorldTransform(t);
						object.getCollisionShape().getAabb(t, min, max);
						min.sub(ext);
						max.add(ext);
						world.activateObjects(min, max);
					}
					world.removeObject(object);

					selectedObject = null;
				}
				return;
			}

			// 'EXPLOSION'
			if(evt.keyCode == Keyboard.KEY_E)
			{
				if(isRayCallbackValid())
				{
					ObjectArrayList<CollisionObject> list = world.getPhyWorld().getCollisionObjectArray();
					float explosionForce = 8;
					Vector3f boomVector = new Vector3f(0,0,0);
					Vector3f blocVector = callback.hitPointWorld;

					for(CollisionObject object : list)
					{
						if(object instanceof RigidBody)
						{
							((RigidBody) object).getCenterOfMassPosition(boomVector);
							boomVector.sub(blocVector);
							float distance = boomVector.length();

							if(distance < explosionForce*3)
							{
								boomVector.scale(explosionForce / distance);
								((RigidBody) object).applyCentralImpulse(boomVector);
								object.activate(true);
							}
						}
					}
				}

				return;
			}

			// SPAWN BOX
			if(evt.keyCode == Keyboard.KEY_B)
			{
				if(isRayCallbackValid())
				{
					Vector3f pos1 = new Vector3f(callback.hitPointWorld);
					Vector3f pos2 = new Vector3f(callback.hitNormalWorld);
					pos2.scale(2);
					pos1.add(pos2);
					for(int i = 0; i < 1; i++)
					{
						RigidBody body = BulletUtil.mkBox(pos1.x,pos1.y,pos1.z, 0, 0, 0, 2, 8, 2, 32, false);
						world.addRigidBody(body);
					}
				}
				return;
			}
			// SPAWN SPHERE
			if(evt.keyCode == Keyboard.KEY_N)
			{
				if(isRayCallbackValid())
				{
					Vector3f pos1 = new Vector3f(callback.hitPointWorld);
					Vector3f pos2 = new Vector3f(callback.hitNormalWorld);
					pos2.scale(2);
					pos1.add(pos2);
					for(int i = 0; i < 1; i++)
					{
						RigidBody body = BulletUtil.mkSphere(pos1.x,pos1.y,pos1.z, 2, 8);
						world.addRigidBody(body);
					}
				}
				return;
			}
			// SPAWB BIG SOLID BOX
			if(evt.keyCode == Keyboard.KEY_C)
			{
				if(isRayCallbackValid())
				{
					Vector3f pos1 = new Vector3f(callback.hitPointWorld);
					Vector3f pos2 = new Vector3f(callback.hitNormalWorld);
					pos2.scale(4.1f);
					pos1.add(pos2);

					RigidBody body = BulletUtil.mkBox(pos1.x,pos1.y,pos1.z, 0, 0, 0, 4, 4, 4, 0, false);
					world.addRigidBody(body);
				}
				return;
			}

			// MAKE STATIC
			if(evt.keyCode == Keyboard.KEY_G)
			{
				if(selectedObject != null)
				{
					CollisionObject object = selectedObject;

					if(object instanceof RigidBody)
					{
						float mass = 0f;
						Vector3f inertia = new Vector3f();
						object.getCollisionShape().calculateLocalInertia(mass, inertia);
						((RigidBody) object).setMassProps(mass, inertia);
					}
				}
				return;
			}
		}

	}

	/**
	 * Mouse event.
	 *
	 * @param evt the evt
	 */
	@EventSubscriber
	public void mouseEvent(MouseEvent evt)
	{
		if(!input.isMouseGrabbed())
		{
			// UI Handling
			if(guiCurrent != null)
			{
				guiCurrent.mouseEvent(evt);
			}
			return;
		}
		else if(evt.isClick())
		{
			if(isRayCallbackValid())
			{
				selectedObject = callback.collisionObject;
			}
			else
			{
				selectedObject = null;
			}
		}

	}

	/**
	 * Checks if is ray callback valid.
	 *
	 * @return true, if is ray callback valid
	 */
	private boolean isRayCallbackValid()
	{
		Vector3f v = callback.hitPointWorld;
		if(v.x == 0 && v.y == 0 && v.z == 0)
			return false;
		return true;
	}

	/**
	 * Gets the pointer.
	 *
	 * @return the pointer
	 */
	public Vector3f getPointer()
	{
		return isRayCallbackValid() ? callback.hitPointWorld : new Vector3f();
	}

	/**
	 * Render frame.
	 *
	 * @param interpolation the interpolation
	 */
	public void renderFrame(float interpolation)
	{
		{
			GL11.glDepthFunc(GL11.GL_LESS);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			GL11.glLineWidth(4f);
			GL11.glPointSize(4f);
			BasicTexture.WHITEPIXEL.bind();
		}

		// Setup Projection
		matrixStack.glLoadMatrix(projection.getProjectionMatrix(interpolation));
		matrixStack.uploadAsProjectionMatrix();

		// SKY RENDERING
		if(drawSky)
		{
			renderSky(interpolation);
		}

		matrixStack.glSetIdentity();
		matrixStack.glLoadMatrix(camera.getCameraMatrix(interpolation));
		matrixStack.uploadAsModelViewMatrix();

		if(drawOriginMarker)
		{
			IVertexDrawer va = VertexDrawerVA.drawer;
			va.startDrawing(GL11.GL_LINES, true, false, false);
			va.setOriginTranslation(0, 0.001f, 0);
			BasicGeometry3D.drawOriginMarker(va, 1024*1024, 16);
			va.stopDrawing();
		}

		if(isRayCallbackValid())
		{
			IVertexDrawer va = VertexDrawerVA.drawer;
			va.startDrawing(GL11.GL_LINES, true, false, false);
			va.setOriginTranslation(callback.hitPointWorld.x, callback.hitPointWorld.y, callback.hitPointWorld.z);
			va.setVertexColor(0, 0, 1, 1);
			va.vertex(0, 0, 0);
			va.vertex(callback.hitNormalWorld.x, callback.hitNormalWorld.y, callback.hitNormalWorld.z);
			va.setVertexColor(0.5f, 0.5f, 1, 1);
			BasicGeometry3D.drawWireframeBox(va, 0.2f, 0.2f, 0.2f, true);
			va.stopDrawing();

			// Draw Selection Box
			if(callback.collisionObject != null)
			{
				if(callback.collisionObject != selectedObject)
				{
					GL11.glLineWidth(0.5f);
					Vector3f min = new Vector3f();
					Vector3f max = new Vector3f();
					Transform t = new Transform();
					callback.collisionObject.getInterpolationWorldTransform(t);
					callback.collisionObject.getCollisionShape().getAabb(t, min, max);

					float x0 = min.x;
					float y0 = min.y;
					float z0 = min.z;
					float x1 = max.x;
					float y1 = max.y;
					float z1 = max.z;
					va.startDrawing(GL11.GL_LINES, true, false, false);
					va.setVertexColor(1, 1, 1, 1);
					BasicGeometry3D.drawWireframeBox(va, x0, y0, z0, x1, y1, z1);
					va.stopDrawing();
				}
			}
		}

		if(selectedObject != null)
		{
			GL11.glLineWidth(2f);
			Vector3f min = new Vector3f();
			Vector3f max = new Vector3f();
			Transform t = new Transform();
			selectedObject.getInterpolationWorldTransform(t);
			selectedObject.getCollisionShape().getAabb(t, min, max);

			float x0 = min.x;
			float y0 = min.y;
			float z0 = min.z;
			float x1 = max.x;
			float y1 = max.y;
			float z1 = max.z;
			IVertexDrawer va = VertexDrawerVA.drawer;
			va.startDrawing(GL11.GL_LINES, true, false, false);
			va.setVertexColor(1, 0, 0, 1);
			BasicGeometry3D.drawWireframeBox(va, x0, y0, z0, x1, y1, z1);
			va.stopDrawing();
		}

		drawInfiniteGrids();

		// shaderSimple.doUseProgram();
		world.renderWorld(matrixStack);
		// shaderSimple.doNotUseProgram();

	}

	/**
	 * Render sky.
	 *
	 * @param interpolation the interpolation
	 */
	private void renderSky(float interpolation)
	{
		matrixStack.glSetIdentity();
		matrixStack.glLoadMatrix(camera.getCameraRotationMatrix(interpolation));
		matrixStack.uploadAsModelViewMatrix();

		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDepthMask(false);
		{
			final Vector3F campos = new Vector3F();
			final Vector3F offset = new Vector3F(0,0,0);
			final Vector4F distanceColor = new Vector4F(1,1,1,1);
			final Vector4F downColor = new Vector4F(0,0,0,1);
			final Vector4F upColor = new Vector4F(0.5f,0.7f,0.7f,1);
			final int segments = 32;
			final float radius = 32f;
			camera.getPosition(campos);

			// DOWN
			{
				final boolean flip = true;
				final float depthP = 0.01f;
				final float depthN = 2f;
				IVertexDrawer va = VertexDrawerVA.drawer;
				BasicGeometry3D.doRenderConeBase(va, radius, segments, depthP, depthN, downColor, distanceColor, offset, flip);
			}
			// UP
			{
				final boolean flip = false;
				final float depthP = -0.01f;
				final float depthN = -2f;
				IVertexDrawer va = VertexDrawerVA.drawer;
				BasicGeometry3D.doRenderConeBase(va, radius, segments, depthP, depthN, upColor, distanceColor, offset, flip);
			}

			// SUN
			{
				final float s = 32;
				final float x = lighting_direction.get(0) * s;
				final float y = lighting_direction.get(1) * s;
				final float z = lighting_direction.get(2) * s;

				IVertexDrawer va = VertexDrawerVA.drawer;
				va.startDrawing(GL11.GL_TRIANGLES, true, false, false);
				va.setVertexColor(1, 1, 0.5f, 1);
				va.setOriginTranslation(x, y, z);
				BasicGeometry3D.drawSphere(va, 2f, true);
				va.stopDrawing();
			}
		}
		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_CULL_FACE);
	}

	/**
	 * Raytrace from__.
	 *
	 * @param camera the camera
	 */
	private void raytraceFrom__(ICamera camera)
	{
		Vector3F p = new Vector3F();
		camera.getPosition(p);

		Vector3f start = new Vector3f();
		Vector3f end = new Vector3f();
		callback = new ClosestRayResultCallback(start, end);

		if(input.isMouseGrabbed())
		{
			Vector3F cend = new Vector3F();
			camera.getForward(cend);
			cend.doMultiplyBy(raytraceDistance);
			cend.doAdd(p);
			start.set(p.x, p.y, p.z);
			end.set(cend.x, cend.y, cend.z);
		}
		else
		{
			raytraceFromMouse(camera, start, end);
		}

		callback.rayFromWorld.set(start);
		callback.rayToWorld.set(end);

		world.getPhyWorld().rayTest(start, end, callback);
	}

	/**
	 * Raytrace from mouse.
	 *
	 * @param camera the camera
	 * @param start the start
	 * @param end the end
	 */
	private void raytraceFromMouse(ICamera camera, Vector3f start, Vector3f end)
	{
		// Create Ray Start/End in NormalizedDeviceCoordinates
		Vector4F lRayStart_NDC;
		Vector4F lRayEnd_NDC;
		{
			float mouseX = input.getMouseX();
			float mouseY = input.getMouseY_UnInv();
			float screenWidth = screen.getPhysicalWidth();
			float screenHeight = screen.getPhysicalHeight();

			// The ray Start and End positions, in Normalized Device Coordinates (Have you read Tutorial 4 ?)
			lRayStart_NDC = new Vector4F(
			    ((float)mouseX/(float)screenWidth  - 0.5f) * 2.0f, // [0,1024] -> [-1,1]
			    ((float)mouseY/(float)screenHeight - 0.5f) * 2.0f, // [0, 768] -> [-1,1]
			    -1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
			    1.0f
			);
			lRayEnd_NDC = new Vector4F(
			    ((float)mouseX/(float)screenWidth  - 0.5f) * 2.0f,
			    ((float)mouseY/(float)screenHeight - 0.5f) * 2.0f,
			    0.0,
			    1.0f
			);
		}

		// Compute the inverse of the multiplication of the Projection and Camera-Matrix
		// glm::mat4 M = glm::inverse(ProjectionMatrix * ViewMatrix);
		Matrix4F M = Matrix4F.newInstance();
		Matrix4F ProjectionMatrix = projection.getProjectionMatrix(1F);
		Matrix4F ViewMatrix = camera.getCameraMatrix(1F);
		Matrix4F.mul(ProjectionMatrix, ViewMatrix, M);
		Matrix4F.invert(M, M);

		// Calculate... wat?
		//glm::vec4 lRayStart_world = M * lRayStart_NDC; lRayStart_world/=lRayStart_world.w;
		//glm::vec4 lRayEnd_world   = M * lRayEnd_NDC  ; lRayEnd_world  /=lRayEnd_world.w;
		Vector4F lRayStart_world = new Vector4F();
		Vector4F lRayEnd_world = new Vector4F();
		Matrix4F.transformVector4F(M, lRayStart_NDC, lRayStart_world);
		Matrix4F.transformVector4F(M, lRayEnd_NDC, lRayEnd_world);
		lRayStart_world.doDivide(lRayStart_world.w);
		lRayEnd_world.doDivide(lRayEnd_world.w);

		// Calculate the final ray-direction...
		// glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
		// lRayDir_world = glm::normalize(lRayDir_world);
		Vector3F lRayDir_world = new Vector3F();
		lRayDir_world.set(lRayEnd_world);
		lRayDir_world.doSubtract(lRayStart_world);

		// set the start to the camera position
		Vector3F camera2 = new Vector3F();
		camera.getPosition(camera2);
		start.set(camera2.x, camera2.y, camera2.z);

		// set the end to the ray-direction
		end.set(lRayDir_world.x,lRayDir_world.y,lRayDir_world.z);

		// scale the ray-direction,
		end.scale(raytraceDistance);

		// and add the camera position to it
		end.add(start);
	}

	/**
	 * Draw infinite grids.
	 */
	private void drawInfiniteGrids()
	{
		// If neither of the grids is active, don't draw them.
		if(!(drawDeletionGrid || drawZeroPlaneGrid))
		{
			return;
		}

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_CULL_FACE);

		IVertexDrawer va = VertexDrawerVA.drawer;
		Vector3F camPos = new Vector3F();
		camera.getPosition(camPos);

		gridTransparentTex.bind();
		va.startDrawing(GL11.GL_QUADS, true, true, false);
		{
			final float square = 100f;
			float x = (float) (Math.floor(camPos.x / square) * square);
			float z = (float) (Math.floor(camPos.z / square) * square);
			float yDist = 1f - MathUtility.clampFloat(0f, 1f, Math.abs(camPos.y+deletionDepth) / 2000f);
			float dDist = MathUtility.clampFloat(0f, 1f, Math.abs(camPos.y) / 200f) * 2f;
			final float size = (square * square) / 2f;
			final float S = 1 * square*10f;
			final float T = 1 * square*10f;

			va.setVertexColor(1, 1, 1, 1);

			final int RANGE = 32;
			for(int xI = -RANGE; xI < RANGE; xI++)
			{
				for(int zI = -RANGE; zI < RANGE; zI++)
				{
					float fx = x + xI*(size*2);
					float fz = z + zI*(size*2);

					if(drawDeletionGrid)
					{
						va.setOriginTranslation(fx, -deletionDepth, fz);
						va.setVertexColor(1, 0, 0, yDist);
						va.setVertexUV(0, 0, 0); va.vertex(-size, 0, +size);
						va.setVertexUV(0, S, 0); va.vertex(+size, 0, +size);
						va.setVertexUV(0, S, T); va.vertex(+size, 0, -size);
						va.setVertexUV(0, 0, T); va.vertex(-size, 0, -size);
					}

					if(drawZeroPlaneGrid)
					{
						va.setOriginTranslation(fx, -dDist, fz);
						va.setVertexColor(1, 1, 1, 1);
						va.setVertexUV(0, 0, 0); va.vertex(-size, 0, +size);
						va.setVertexUV(0, S, 0); va.vertex(+size, 0, +size);
						va.setVertexUV(0, S, T); va.vertex(+size, 0, -size);
						va.setVertexUV(0, 0, T); va.vertex(-size, 0, -size);
					}
				}
			}
		}
		va.stopDrawing();

		gridTransparentTex.unbind();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
	}

	/**
	 * Screen resize.
	 *
	 * @param screen the screen
	 * @param physicalWidth the physical width
	 * @param physicalHeight the physical height
	 */
	public void screenResize(IGLScreen screen, float physicalWidth, float physicalHeight)
	{
		if(guiCurrent != null && guiCurrent.shouldResizeToFillScreen())
		{
			guiCurrent.position.setXY(0, 0);
			guiCurrent.size.setXY(screen.getPhysicalWidth(), screen.getPhysicalHeight());
			guiCurrent.size.clampXY(1,Integer.MAX_VALUE);
		}
		if(guiCurrent != null)
		{
			guiCurrent.relayout();
		}
	}

	/**
	 * Sets the current gui.
	 *
	 * @param panel the new current gui
	 */
	public void setCurrentGUI(GUIPanel panel)
	{
		if(this.guiCurrent != null)
		{
			// unload
			guiCurrent.unload();
		}

		guiCurrent = panel;

		if(guiCurrent != null && guiCurrent.shouldResizeToFillScreen())
		{
			guiCurrent.position.setXY(0, 0);
			guiCurrent.size.setXY(screen.getPhysicalWidth(), screen.getPhysicalHeight());
			guiCurrent.size.clampXY(1,Integer.MAX_VALUE);
		}

		if(this.guiCurrent != null)
		{
			// load
			guiCurrent.load();

			if(screen.isPhysicalSizeReal())
			{
				guiCurrent.relayout();
			}
			else
			{
				screen.setResized();
			}
		}

		input.setMouseGrabbed(guiCurrent == null);
	}

	/**
	 * Render frame2 d.
	 *
	 * @param interpolation the interpolation
	 */
	public void renderFrame2D(float interpolation)
	{
		// Draw Crosshair
		if(drawCrosshair)
		{
			GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE_MINUS_SRC_COLOR);

			float B = 32;
			float D = 2;
			float midX = screen.getPhysicalWidth() / 2F;
			float midY = screen.getPhysicalHeight() / 2F;
			BasicTexture.WHITEPIXEL.bind();
			BasicGeometry2D.setPrimaryColor(Color.WHITE);
			BasicGeometry2D.setDrawer(VertexDrawerVA.drawer);
			BasicGeometry2D.drawFlatUntexturedRectanglePD(midX-(B/2f), midY-(D/2f), B, D);
			BasicGeometry2D.drawFlatUntexturedRectanglePD(midX-(D/2f), midY-(B/2f), D, B);

			// reset GL-state
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		}

		// Draw Debug
		if(drawDebugInfo)
		{
			Runtime run = Runtime.getRuntime();
			String mem = StringUtil.humanReadableByteCount(run.totalMemory() - run.freeMemory());
			String memTotal = StringUtil.humanReadableByteCount(run.totalMemory());
			String memMax = StringUtil.humanReadableByteCount(run.maxMemory());
			String collisionObjects = StringUtil.humanReadable(world.getPhyWorldObjectCount());
			String logicalObjects = StringUtil.humanReadable(world.getECSWorldObjectCount());
			boolean unsynced = world.getPhyWorldObjectCount() != world.getECSWorldObjectCount();

			fontRenderer.bindFontMap();
			fontRenderer.setFontColor(0x0F);

			int YO = 1;
			int XO = 1;
			int FLAGS = 0;

			fontRenderer.drawString("Memory: " + mem + " (of " + memTotal + " of "+memMax+")", XO, YO, FLAGS); YO += 18;

			if(unsynced)
			{
				fontRenderer.drawString("\\�c0CObjects: " + collisionObjects + "phy " + logicalObjects + "logic", XO, YO, FLAGS); YO += 18;
			}
			else
			{
				fontRenderer.drawString("Objects: " + collisionObjects, XO, YO, FLAGS); YO += 18;
			}

			fontRenderer.drawString("CollisionShapes: " + BulletUtil.getShapeCacheSize(), XO, YO, FLAGS); YO += 18;

			fontRenderer.drawString("Camera: " + camera, XO, YO, FLAGS); YO += 18;
		}

		// Draw UI
		if(guiCurrent != null && drawGUI)
		{
			BasicTexture.WHITEPIXEL.bind();
			BasicGeometry2D.setDrawer(VertexDrawerVA.drawer);
			guiRenderer.renderUI(interpolation, guiCurrent);
		}

		// reset GL-state
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		BasicTexture.WHITEPIXEL.bind();
	}

}
