/*
 * 
 */
package de.longor.sandbox;

import de.longor.eventor.Event;

// TODO: Auto-generated Javadoc
/**
 * The Class SandboxFetchEvent.
 */
public class SandboxFetchEvent extends Event
{
	
	/** The sandbox. */
	public Sandbox sandbox;
	
	/**
	 * Instantiates a new sandbox fetch event.
	 */
	public SandboxFetchEvent()
	{
		sandbox = null;
	}
}
