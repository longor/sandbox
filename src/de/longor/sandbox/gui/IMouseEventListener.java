/*
 *
 */
package de.longor.sandbox.gui;

import de.longor.gcore.input.MouseEvent;

/**
 */
public interface IMouseEventListener
{

	/**
	 * Mouse event.
	 *
	 * @param evt the evt
	 */
	public void mouseEvent(MouseEvent evt);

}
