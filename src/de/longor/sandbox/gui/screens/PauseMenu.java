/*
 * 
 */
package de.longor.sandbox.gui.screens;

import org.lwjgl.input.Keyboard;

import de.longor.gcore.input.KeyboardEvent;
import de.longor.sandbox.Sandbox;
import de.longor.sandbox.gui.GUIComponent;
import de.longor.sandbox.gui.GUIPanel;
import de.longor.sandbox.gui.IKeyEventListener;
import de.longor.sandbox.gui.components.GUIButton;
import de.longor.sandbox.gui.components.IActionListener;
import de.longor.sandbox.gui.components.graphical.GOGradient;
import de.longor.sandbox.world.bullet.BulletUtil;
import de.longor1996.util.math.Color;

// TODO: Auto-generated Javadoc
/**
 * The Class PauseMenu.
 */
public class PauseMenu
{
	
	/**
	 * Builds the.
	 *
	 * @param sandbox the sandbox
	 * @return the GUI panel
	 */
	public static GUIPanel build(Sandbox sandbox)
	{
		GUIPanel pausemenu = new GUIPanel();
		pausemenu.background = Color.BLACK_TRANSLUCENT2;
		pausemenu.shouldResizeToFullScreen = true;
		pausemenu.addKeyListener(new IKeyEventListener()
		{
			@Override public void keyEvent(KeyboardEvent evt)
			{
				if(!evt.keyState && evt.keyCode == Keyboard.KEY_ESCAPE)
					sandbox.setCurrentGUI(null);
			}
		});
		
		
		pausemenu.components.add(new GOGradient(Color.BLACK, Color.TRANSPARENT, 0).setSize(96f*2.5f, 65535f).setLocation(0, 0));
		
		GUIButton button_exitGame = buildExitButton(sandbox);
		pausemenu.components.add(button_exitGame);
		
		GUIButton button_returnToGame = buildReturnToGameButton(sandbox);
		pausemenu.components.add(button_returnToGame);
		
		pausemenu.components.add(buildResetWorldButton(sandbox));
		
		
		
		
		
		
		
		pausemenu.components.add(new GUIButton("Execute Script 'test.py'").addClickListener(new IActionListener()
		{
			@Override public void action(Object source)
			{
				sandbox.interp.execfile("test.py");
			}
		}).setButtonColor(Color.DARK_GRAY_TRANSLUCENT2, Color.GREEN_TRANSLUCENT2).setSize(96*3, 24).setLocation(4, 4 + (24 + 4) * 2));
		pausemenu.components.add(new GUIButton("Execute Script 'test2.py'").addClickListener(new IActionListener()
		{
			@Override public void action(Object source)
			{
				sandbox.interp.execfile("test2.py");
			}
		}).setButtonColor(Color.DARK_GRAY_TRANSLUCENT2, Color.GREEN_TRANSLUCENT2).setSize(96*3, 24).setLocation(4, 4 + (24 + 4) * 3));
		
		
		
		
		
		
		
		IActionListener layout = new IActionListener()
		{
			@Override public void action(Object source)
			{
				GUIPanel pausemenu = (GUIPanel) source;
				button_exitGame.position.y = pausemenu.size.y - (24 + 4);
			}
		};
		pausemenu.addRelayoutListener(layout);
		
		
		
		return pausemenu;
	}
	
	
	
	/**
	 * Builds the reset world button.
	 *
	 * @param sandbox the sandbox
	 * @return the GUI component
	 */
	private static GUIComponent buildResetWorldButton(Sandbox sandbox) {
		return new GUIButton("Reset World").addClickListener(new IActionListener()
		{
			@Override public void action(Object source)
			{
				BulletUtil.clearShapeCache();
				sandbox.world.reset();
			}
		}).setButtonColor(Color.DARK_GRAY_TRANSLUCENT2, Color.ORANGE_TRANSLUCENT2).setSize(96*2, 24).setLocation(4, 4 + 24 + 4);
	}



	/**
	 * Builds the return to game button.
	 *
	 * @param sandbox the sandbox
	 * @return the GUI button
	 */
	private static GUIButton buildReturnToGameButton(Sandbox sandbox)
	{
		return (GUIButton) new GUIButton("Return to Game").addClickListener(new IActionListener()
		{
			@Override public void action(Object source)
			{
				sandbox.setCurrentGUI(null);
			}
		}).setButtonColor(Color.DARK_GRAY_TRANSLUCENT2, Color.ORANGE_TRANSLUCENT2).setSize(96*2, 24).setLocation(4, 4);
	}
	
	
	
	/**
	 * Builds the exit button.
	 *
	 * @param sandbox the sandbox
	 * @return the GUI button
	 */
	private static GUIButton buildExitButton(Sandbox sandbox)
	{
		return  (GUIButton) new GUIButton("Exit Game").addClickListener(new IActionListener()
		{
			@Override public void action(Object source)
			{
				sandbox.gameloop.stopLoop();
			}
		}).setButtonColor(Color.DARK_GRAY_TRANSLUCENT2, Color.RED_TRANSLUCENT2).setSize(96*2, 24).setLocation(4, 4 + 24 + 4);
	}
	
	
	
}
