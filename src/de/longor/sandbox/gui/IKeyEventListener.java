/*
 *
 */
package de.longor.sandbox.gui;

import de.longor.gcore.input.KeyboardEvent;

/**
 *
 */
public interface IKeyEventListener
{

	/**
	 * Key event.
	 *
	 * @param evt the evt
	 */
	public void keyEvent(KeyboardEvent evt);

}
