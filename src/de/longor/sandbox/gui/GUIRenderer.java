/*
 * 
 */
package de.longor.sandbox.gui;

import java.util.Stack;

import de.longor.gcore.graphics.BasicGeometry2D;
import de.longor.gcore.graphics.font.FontRenderer;
import de.longor.gcore.graphics.textures.BasicTexture;
import de.longor1996.util.math.Color;
import de.longor1996.util.math.Vector2F;
import de.longor1996.util.math.geom2D.RectangleF;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIRenderer.
 */
public class GUIRenderer
{
	
	/** The frame stack. */
	private Stack<Vector2F> frameStack;
	
	/** The frame stack sum. */
	private Vector2F frameStackSum;
	
	/** The fr. */
	private FontRenderer fr;
	
	/** The wp. */
	private BasicTexture wp;
	
	/**
	 * Instantiates a new GUI renderer.
	 */
	public GUIRenderer()
	{
		frameStack = new Stack<Vector2F>();
		frameStackSum = new Vector2F(0, 0);
	}
	
	/**
	 * Render ui.
	 *
	 * @param interpolation the interpolation
	 * @param guiCurrent the gui current
	 */
	public void renderUI(float interpolation, GUIPanel guiCurrent)
	{
		guiCurrent.renderUI(this);
	}
	
	/**
	 * Draw st background.
	 *
	 * @param bounds the bounds
	 * @param color the color
	 */
	public void drawSTBackground(RectangleF bounds, Color color)
	{
		BasicGeometry2D.setPrimaryColor(color);
		BasicGeometry2D.drawFlatUntexturedRectanglePD(bounds);
		BasicGeometry2D.setLineWidth(1f);
		BasicGeometry2D.setPrimaryColor(Color.BLACK);
		BasicGeometry2D.drawRectangleBorder(bounds);
	}
	
	/**
	 * Draw st background.
	 *
	 * @param size the size
	 * @param color the color
	 */
	public void drawSTBackground(Vector2F size, Color color)
	{
		BasicGeometry2D.setPrimaryColor(color);
		BasicGeometry2D.drawFlatUntexturedRectanglePD(size);
		BasicGeometry2D.setLineWidth(1f);
		BasicGeometry2D.setPrimaryColor(Color.BLACK);
		BasicGeometry2D.drawRectangleBorder(size);
	}
	
	/**
	 * Push frame.
	 *
	 * @param position the position
	 */
	public void pushFrame(Vector2F position)
	{
		frameStack.push(position);
		frameStackSum.add(position);
		BasicGeometry2D.setOffset(frameStackSum);
	}
	
	/**
	 * Pop frame.
	 */
	public void popFrame() {
		Vector2F position = frameStack.pop();
		frameStackSum.subtract(position);
		BasicGeometry2D.setOffset(frameStackSum);
	}

	/**
	 * Draw sring.
	 *
	 * @param string the string
	 * @param x the x
	 * @param y the y
	 * @param flags the flags
	 */
	public void drawSring(String string, float x, float y, int flags)
	{
		x += frameStackSum.x;
		y += frameStackSum.y;
		fr.bindFontMap();
		fr.drawString(string, x, y, flags);
		wp.bind();
	}
	
	/**
	 * Sets the font renderer.
	 *
	 * @param fontRenderer the new font renderer
	 */
	public void setFontRenderer(FontRenderer fontRenderer)
	{
		fr = fontRenderer;
	}

	/**
	 * Sets the white pixel.
	 *
	 * @param whitepixel the new white pixel
	 */
	public void setWhitePixel(BasicTexture whitepixel)
	{
		wp = whitepixel;
	}
	
	/**
	 * Sets the font color.
	 *
	 * @param color the new font color
	 */
	public void setFontColor(Color color)
	{
		fr.setFontColor(color.r, color.g, color.b);
	}
	
	/**
	 * Draw vertical gradient.
	 *
	 * @param size the size
	 * @param A the a
	 * @param B the b
	 */
	public void drawVerticalGradient(Vector2F size, Color A, Color B)
	{
		BasicGeometry2D.setPrimaryColor(A);
		BasicGeometry2D.setSecondaryColor(B);
		BasicGeometry2D.drawVerticalGradient(size);
	}
	
	/**
	 * Draw horizontal gradient.
	 *
	 * @param size the size
	 * @param A the a
	 * @param B the b
	 */
	public void drawHorizontalGradient(Vector2F size, Color A, Color B)
	{
		BasicGeometry2D.setPrimaryColor(A);
		BasicGeometry2D.setSecondaryColor(B);
		BasicGeometry2D.drawHorizontalGradient(size);
	}
	
}
