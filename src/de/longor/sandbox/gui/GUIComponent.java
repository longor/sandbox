/*
 * 
 */
package de.longor.sandbox.gui;

import de.longor.gcore.input.MouseEvent;
import de.longor1996.util.math.Vector2F;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIComponent.
 */
public abstract class GUIComponent
{
	
	/** The position. */
	public Vector2F position = new Vector2F(32, 32);
	
	/** The size. */
	public Vector2F size = new Vector2F(32, 32);
	
	/**
	 * Render ui.
	 *
	 * @param guiRenderer the gui renderer
	 */
	public abstract void renderUI(GUIRenderer guiRenderer);
	
	/**
	 * Sets the size.
	 *
	 * @param width the width
	 * @param height the height
	 * @return the GUI component
	 */
	public GUIComponent setSize(float width, float height)
	{
		size.setXY(width, height);
		return this;
	}
	
	/**
	 * Sets the location.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the GUI component
	 */
	public GUIComponent setLocation(float x, float y)
	{
		position.setXY(x, y);
		return this;
	}
	
	/**
	 * Mouse event.
	 *
	 * @param evt the evt
	 * @param mx the mx
	 * @param my the my
	 */
	public void mouseEvent(MouseEvent evt, float mx, float my)
	{
		
	}
	
}
