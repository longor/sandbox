/*
 * 
 */
package de.longor.sandbox.gui.components;

import java.util.ArrayList;

import de.longor.gcore.input.MouseEvent;
import de.longor.sandbox.gui.GUIComponent;
import de.longor.sandbox.gui.GUIRenderer;
import de.longor1996.util.math.Color;
import de.longor1996.util.math.MathUtility;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIButton.
 */
public class GUIButton extends GUIComponent
{
	
	/** The button text. */
	String buttonText;
	
	/** The hover state. */
	boolean hoverState;
	
	/** The text color. */
	Color textColor = Color.WHITE;
	
	/** The text hover color. */
	Color textHoverColor = Color.WHITE;
	
	/** The button color. */
	Color buttonColor = Color.BLACK_TRANSLUCENT2;
	
	/** The button hover color. */
	Color buttonHoverColor = Color.ORANGE_TRANSLUCENT2;
	
	/** The click listeners. */
	ArrayList<IActionListener> clickListeners;
	
	/**
	 * Instantiates a new GUI button.
	 *
	 * @param text the text
	 */
	public GUIButton(String text)
	{
		super();
		buttonText = text;
		clickListeners = null;
	}
	
	/**
	 * Adds the click listener.
	 *
	 * @param listener the listener
	 * @return the GUI button
	 */
	public GUIButton addClickListener(IActionListener listener)
	{
		if(clickListeners == null)
			clickListeners = new ArrayList<IActionListener>();
		
		clickListeners.add(listener);
		return this;
	}

	/**
	 * Sets the button color.
	 *
	 * @param normalColor the normal color
	 * @param hoverColor the hover color
	 * @return the GUI button
	 */
	public GUIButton setButtonColor(Color normalColor, Color hoverColor)
	{
		this.buttonColor = normalColor;
		this.buttonHoverColor = hoverColor;
		return this;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.sandbox.gui.GUIComponent#renderUI(de.longor.sandbox.gui.GUIRenderer)
	 */
	@Override
	public void renderUI(GUIRenderer guiRenderer)
	{
		guiRenderer.drawSTBackground(size, hoverState ? buttonHoverColor : buttonColor);
		guiRenderer.setFontColor(hoverState ? textHoverColor : textColor);
		guiRenderer.drawSring(buttonText, 2f, size.y/2f - 16f/2f, 0);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.sandbox.gui.GUIComponent#mouseEvent(de.longor.gcore.input.MouseEvent, float, float)
	 */
	public void mouseEvent(MouseEvent evt, float mx, float my)
	{
		if(MathUtility.isInside(0f,size.x,mx) && MathUtility.isInside(0f,size.y,my))
		{
			hoverState = true;
		}
		else
		{
			hoverState = false;
		}
		
		if(evt.isLeftClick() && hoverState && clickListeners != null)
		{
			for(IActionListener action : clickListeners)
				action.action(this);
		}
	}
	
}
