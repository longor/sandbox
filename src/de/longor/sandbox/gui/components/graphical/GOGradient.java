/*
 * 
 */
package de.longor.sandbox.gui.components.graphical;

import de.longor.sandbox.gui.GUIComponent;
import de.longor.sandbox.gui.GUIRenderer;
import de.longor1996.util.math.Color;

// TODO: Auto-generated Javadoc
/**
 * The Class GOGradient.
 */
public class GOGradient extends GUIComponent {
	
	/** The ca. */
	Color CA;
	
	/** The cb. */
	Color CB;
	
	/** The mth. */
	int MTH;
	
	/**
	 * Instantiates a new GO gradient.
	 *
	 * @param CA the ca
	 * @param CB the cb
	 * @param MTH the mth
	 */
	public GOGradient(Color CA, Color CB, int MTH)
	{
		this.CA = CA;
		this.CB = CB;
		this.MTH = MTH;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.sandbox.gui.GUIComponent#renderUI(de.longor.sandbox.gui.GUIRenderer)
	 */
	@Override
	public void renderUI(GUIRenderer guiRenderer)
	{
		switch(MTH)
		{
			case 0: guiRenderer.drawVerticalGradient(this.size, CA, CB); break;
			case 1: guiRenderer.drawHorizontalGradient(this.size, CA, CB); break;
		}
		
	}
	
}
