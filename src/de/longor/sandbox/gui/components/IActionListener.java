/*
 * 
 */
package de.longor.sandbox.gui.components;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving IAction events.
 * The class that is interested in processing a IAction
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addIActionListener<code> method. When
 * the IAction event occurs, that object's appropriate
 * method is invoked.
 *
 * @see IActionEvent
 */
public interface IActionListener
{
	
	/**
	 * Action.
	 *
	 * @param source the source
	 */
	public void action(Object source);
}
