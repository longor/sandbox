/*
 * 
 */
package de.longor.sandbox.gui;

import java.util.ArrayList;

import de.longor.gcore.input.KeyboardEvent;
import de.longor.gcore.input.MouseEvent;
import de.longor.sandbox.gui.components.IActionListener;
import de.longor1996.util.math.Color;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIPanel.
 */
public class GUIPanel extends GUIComponent
{
	
	/** The components. */
	public ArrayList<GUIComponent> components;
	
	/** The background. */
	public Color background;
	
	/** The should resize to full screen. */
	public boolean shouldResizeToFullScreen;
	
	/** The key listeners. */
	private ArrayList<IKeyEventListener> keyListeners;
	
	/** The mouse listeners. */
	private ArrayList<IMouseEventListener> mouseListeners;
	
	/** The relayout listeners. */
	private ArrayList<IActionListener> relayoutListeners;
	
	/**
	 * Instantiates a new GUI panel.
	 */
	public GUIPanel()
	{
		components = new ArrayList<GUIComponent>();
		background = Color.BLACK_TRANSLUCENT2;
		shouldResizeToFullScreen = false;
	}
	
	/**
	 * Adds the relayout listener.
	 *
	 * @param newListener the new listener
	 */
	public void addRelayoutListener(IActionListener newListener)
	{
		if(relayoutListeners == null)
			relayoutListeners = new ArrayList<IActionListener>();
		
		relayoutListeners.add(newListener);
	}
	
	/**
	 * Adds the key listener.
	 *
	 * @param newListener the new listener
	 */
	public void addKeyListener(IKeyEventListener newListener)
	{
		if(keyListeners == null)
			keyListeners = new ArrayList<IKeyEventListener>();
		
		keyListeners.add(newListener);
	}
	
	/**
	 * Adds the mouse listener.
	 *
	 * @param newListener the new listener
	 */
	public void addMouseListener(IMouseEventListener newListener)
	{
		if(mouseListeners == null)
			mouseListeners = new ArrayList<IMouseEventListener>();
		
		mouseListeners.add(newListener);
	}
	
	/**
	 * Load.
	 */
	public void load()
	{
		
	}
	
	/**
	 * Relayout.
	 */
	public void relayout()
	{
		if(relayoutListeners != null)
			for(IActionListener action : relayoutListeners)
				action.action(this);
	}
	
	/**
	 * Unload.
	 */
	public void unload()
	{
		
	}
	
	/**
	 * Should resize to fill screen.
	 *
	 * @return true, if successful
	 */
	public boolean shouldResizeToFillScreen()
	{
		return shouldResizeToFullScreen;
	}
	
	/* (non-Javadoc)
	 * @see de.longor.sandbox.gui.GUIComponent#renderUI(de.longor.sandbox.gui.GUIRenderer)
	 */
	@Override
	public void renderUI(GUIRenderer guiRenderer)
	{
		guiRenderer.drawSTBackground(size, background);
		
		for(GUIComponent component : components)
		{
			guiRenderer.pushFrame(component.position);
			component.renderUI(guiRenderer);
			guiRenderer.popFrame();
		}
	}
	
	/**
	 * Key event.
	 *
	 * @param evt the evt
	 */
	public void keyEvent(KeyboardEvent evt)
	{
		if(keyListeners != null)
			for(IKeyEventListener kl : keyListeners)
				kl.keyEvent(evt);
	}

	/**
	 * Mouse event.
	 *
	 * @param evt the evt
	 */
	public void mouseEvent(MouseEvent evt)
	{
		if(mouseListeners != null)
			for(IMouseEventListener kl : mouseListeners)
				kl.mouseEvent(evt);
		
		for(GUIComponent component : components)
		{
			component.mouseEvent(evt, evt.mouseX-component.position.x, evt.mouseY-component.position.y);
		}
	}
	
}
