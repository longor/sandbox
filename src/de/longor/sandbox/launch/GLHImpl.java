/*
 * 
 */
package de.longor.sandbox.launch;

import org.lwjgl.opengl.GL11;

import de.longor.gcore.GameLoop;
import de.longor.gcore.TimedLoopHandler;
import de.longor.gcore.graphics.IGLScreen;
import de.longor.gcore.graphics.MatrixStack;
import de.longor.gcore.graphics.projection.IProjection;
import de.longor.gcore.profiler.SystemProfiler;
import de.longor.sandbox.Sandbox;

// TODO: Auto-generated Javadoc
/**
 * The Class GLHImpl.
 */
class GLHImpl implements TimedLoopHandler
{
	
	/** The matrix stack. */
	public MatrixStack matrixStack;
	
	/** The ui projection. */
	public IProjection uiProjection;
	
	/** The profiler. */
	public SystemProfiler profiler;
	
	/** The gloop. */
	public GameLoop gloop;
	
	/** The sbox. */
	public Sandbox sbox;
	
	/** The screen. */
	public IGLScreen screen;
	
	/**
	 * Instantiates a new GLH impl.
	 */
	public GLHImpl()
	{
		matrixStack = new MatrixStack();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#loop(float)
	 */
	@Override
	public void loop(float variableDelta)
	{
		sbox.loopUpdate(variableDelta);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#tick()
	 */
	@Override
	public void tick()
	{
		sbox.tickUpdate();
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frameStart()
	 */
	@Override
	public void frameStart()
	{
		if(screen.pre_update())
		{
			profiler.setScreenPosition(
					screen.getPhysicalWidth()-profiler.getDisplayWidth()+2,
					screen.getPhysicalHeight()-profiler.getDisplayHeight()*2
			);
			sbox.screenResize(screen, screen.getPhysicalWidth(), screen.getPhysicalHeight());
		}
		
		profiler.update();
		
		if(screen.isCloseRequested())
		{
			gloop.stopLoop();
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frame(float)
	 */
	@Override
	public void frame(float interpolation)
	{
		// Scene
		{
			screen.clear(true, true, true);
			sbox.renderFrame(interpolation);
		}
		
		// GUI
		{
			screen.clear(false, true, false);
			
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			
			matrixStack.glLoadMatrix(uiProjection.getProjectionMatrix(interpolation));
			matrixStack.uploadAsProjectionMatrix();
			matrixStack.glSetIdentity();
			matrixStack.uploadAsModelViewMatrix();
			
			sbox.renderFrame2D(interpolation);
			profiler.draw();
			
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frameStop()
	 */
	@Override
	public void frameStop()
	{
		screen.post_update();
	}
}