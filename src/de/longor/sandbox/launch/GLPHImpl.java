/*
 * 
 */
package de.longor.sandbox.launch;

import java.util.concurrent.atomic.AtomicInteger;

import org.lwjgl.opengl.GL11;

import de.longor.gcore.GameLoop;
import de.longor.gcore.TimedLoopHandler;
import de.longor.gcore.graphics.IGLScreen;
import de.longor.gcore.graphics.IVertexDrawer;
import de.longor.gcore.graphics.MatrixStack;
import de.longor.gcore.graphics.VertexDrawerVA;
import de.longor.gcore.graphics.projection.IProjection;
import de.longor.gcore.profiler.SystemProfiler;
import de.longor1996.util.math.SimplexNoise;

// TODO: Auto-generated Javadoc
/**
 * The Class GLPHImpl.
 */
class GLPHImpl implements TimedLoopHandler
{
	
	/** The loop impl. */
	public GLHImpl loopImpl;
	
	/** The loaders. */
	public AtomicInteger loaders;
	
	/** The profiler. */
	public SystemProfiler profiler;
	
	/** The gloop. */
	public GameLoop gloop;
	
	/** The screen. */
	public IGLScreen screen;
	
	/** The matrix stack. */
	public MatrixStack matrixStack;
	
	/** The ui projection. */
	public IProjection uiProjection;
	
	/** The noise. */
	public SimplexNoise noise;
	
	/** The xo. */
	float xo = 0;
	
	/**
	 * Instantiates a new GLPH impl.
	 */
	public GLPHImpl()
	{
		noise = new SimplexNoise(1234567L);
		matrixStack = new MatrixStack(2);
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frameStart()
	 */
	@Override
	public void frameStart()
	{
		if(screen.pre_update())
		{
			profiler.setScreenPosition(
					screen.getPhysicalWidth()-profiler.getDisplayWidth()+2,
					screen.getPhysicalHeight()-profiler.getDisplayHeight()*2
			);
		}
		
		profiler.update();
		
		if(screen.isCloseRequested())
		{
			gloop.stopLoop();
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#tick()
	 */
	@Override
	public void tick()
	{
		xo += 0.1f;
		
		if(loaders.get() <= 0)
		{
			gloop.setLoopHandler(loopImpl);
			screen.setResized();
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frame(float)
	 */
	@Override
	public void frame(float interpolation)
	{
		screen.clear(true, true, true);
		
		// GUI
		{
			matrixStack.glLoadMatrix(uiProjection.getProjectionMatrix(interpolation));
			matrixStack.uploadAsProjectionMatrix();
			matrixStack.glSetIdentity();
			matrixStack.uploadAsModelViewMatrix();
			
			// GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
			
			IVertexDrawer va = VertexDrawerVA.drawer;
			va.startDrawing(GL11.GL_TRIANGLES, true, false, false);
			va.setVertexColor(1, 1, 1, 1);
			va.setOriginTranslation(16, 128, 0);
			float xscale = -128;
			float yscale = -128;
			
			for(int i = 0; i < LoadingModel.triangles.length; i++)
			{
				float[][] triangle = LoadingModel.triangles[i];
				float[] a = triangle[0];
				float[] b = triangle[1];
				float[] c = triangle[2];
				
				float N = (float) noise.noise(a[1] + xo, b[0], c[2]);
				float N2 = (float) noise.noise(b[0] + xo, a[1] / 512f, c[2]);
				float N3 = (float) noise.noise(c[1] + xo, a[0], b[1]);
				N += 1;
				N *= 0.5;
				N2 += 1;
				N2 *= 0.5;
				va.setVertexColor(N, N3, N2, 1);
				
				va.vertex(a[1]*xscale, a[0]*yscale, a[2]);
				va.vertex(b[1]*xscale, b[0]*yscale, b[2]);
				va.vertex(c[1]*xscale, c[0]*yscale, c[2]);
			}
			va.stopDrawing();
			
			profiler.draw();
		}
	}
	
	/* (non-Javadoc)
	 * @see de.longor.gcore.TimedLoopHandler#frameStop()
	 */
	@Override
	public void frameStop()
	{
		screen.post_update();
	}
}