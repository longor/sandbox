/*
 * 
 */
package de.longor.sandbox.launch;


// TODO: Auto-generated Javadoc
/**
 * The Class TimeWasteThread.
 */
public class TimeWasteThread extends Thread
{
	
	/**
	 * Instantiates a new time waste thread.
	 */
	public TimeWasteThread()
	{
		super.setDaemon(true);
		super.setName("TimePrecisionFixerThread");
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		try {
			Thread.sleep(Long.MAX_VALUE, 333);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
