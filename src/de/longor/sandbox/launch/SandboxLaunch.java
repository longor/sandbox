/*
 * 
 */
package de.longor.sandbox.launch;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

import de.longor.eventor.Eventor;
import de.longor.eventor.SystemCleanup;
import de.longor.eventor.SystemInitialize;
import de.longor.gcore.ArgsMap;
import de.longor.gcore.GameLoop;
import de.longor.gcore.crashreport.CrashReport;
import de.longor.gcore.crashreport.CrashWindow;
import de.longor.gcore.graphics.IGLScreen;
import de.longor.gcore.graphics.projection.IProjection;
import de.longor.gcore.graphics.projection.PhysicalScreenProjection;
import de.longor.gcore.graphics.screen.GLAWTScreen;
import de.longor.gcore.graphics.screen.GLNativeScreen;
import de.longor.gcore.graphics.screen.GLSwingScreen;
import de.longor.gcore.input.InputManager;
import de.longor.gcore.profiler.FPSDataline;
import de.longor.gcore.profiler.SystemProfiler;
import de.longor.gcore.resourcemanager.ResourceManager;
import de.longor.sandbox.Sandbox;
import de.longor1996.util.concurrent.WorkThreadPool;
import de.longor1996.util.io.FileUtil;
import de.longor1996.util.io.IniFile;

// TODO: Auto-generated Javadoc
/**
 * The Class SandboxLaunch.
 */
public class SandboxLaunch
{
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args)
	{
		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		Thread.currentThread().setName("gl-main-thread");
		
		System.out.println("[Info] SandboxLaunch.main(args)");
		FileUtil.ensureFileExists(new File("engine.ini"));
		
		// Settings/Flags/Arguments
		ArgsMap map = ArgsMap.createAndLoad(args);
		IniFile ini = IniFile.safeLoad("engine.ini");
		
		// Fetch settings
		String st_glscreen = ini.getString("screen", "class", "awt");
		boolean st_borderlessScreen = ini.getBoolean("screen", "borderless", false);
		boolean st_expandwindow = ini.getBoolean("screen", "expandwindow", false);
		boolean st_vsync = ini.getBoolean("screen", "vsync", false);
		int st_minscreenWidth = ini.getInt("screen", "minimum-width", 800);
		int st_minscreenHeight = ini.getInt("screen", "minimum-height", 500);
		int st_frameratecap = ini.getInt("screen", "frameratecap", 60);
		
		// Object Creation
		ResourceManager resmng = new ResourceManager();
		Eventor eventor = new Eventor();
		Sandbox box = new Sandbox();
		GLHImpl loopImpl = new GLHImpl();
		GLPHImpl preloopImpl = new GLPHImpl();
		GameLoop loop = new GameLoop(preloopImpl);
		WorkThreadPool threadPool = new WorkThreadPool("worker", 4);
		SystemProfiler profiler = new SystemProfiler();
		IGLScreen screen = makeScreen(st_glscreen);
		IProjection screenGuiProj = new PhysicalScreenProjection(screen);
		InputManager input = new InputManager(eventor, screen);
		TimeWasteThread timewaster = new TimeWasteThread();
		
		{
			// Screen Setup
			screen.setTitle("Sandbox");
			screen.setFramerateCap(st_frameratecap);
			screen.setBorderless(st_borderlessScreen);
			screen.setPhysicalMinimumScreenSize(st_minscreenWidth, st_minscreenHeight);
			screen.setExpandWindow(st_expandwindow);
			
			// LoopHandler Setup
			loopImpl.screen = screen;
			loopImpl.sbox = box;
			loopImpl.gloop = loop;
			loopImpl.profiler = profiler;
			loopImpl.uiProjection = screenGuiProj;
			preloopImpl.profiler = profiler;
			preloopImpl.screen = screen;
			preloopImpl.gloop = loop;
			preloopImpl.uiProjection = screenGuiProj;
			preloopImpl.loopImpl = loopImpl;
			
			// Loop Setup
			loop.setSleeping(false);
			loop.setTickRate(40);
			loop.setTimers(true, true, true, true);
			
			// System Profiler
			profiler.addDataline(new FPSDataline(loop, profiler.length()));
			profiler.setDisplayHeight(96);
			profiler.setDisplayWidthScale(3);
			
			// Eventor Registration
			eventor.register(resmng);
			eventor.register(eventor);
			eventor.register(box);
			eventor.register(loop);
			eventor.register(profiler);
			eventor.register(screen);
			eventor.register(input);
			eventor.register(loopImpl);
		}
		
		// Assignment
		box.appargs = map;
		box.screen = screen;
		box.gameloop = loop;
		box.eventor = eventor;
		box.resmanager = resmng;
		box.threadpool = threadPool;
		box.input = input;
		
		// Initialize
		try {
			// start timewaster
			timewaster.start();
			
			// init screen
			screen.show();
			
			// init input system
			input.init();
			
			if(st_vsync)
				Display.setVSyncEnabled(true);
			
		} catch (UnsatisfiedLinkError e) {
			e.printStackTrace();
			CrashWindow.showCrashWindow(CrashReport.create(e).addObject(screen).addObject(args));
			return;
		} catch (LWJGLException e) {
			e.printStackTrace();
			screen.cleanup();
			CrashWindow.showCrashWindow(CrashReport.create(e).addObject(screen).addObject(args));
			return;
		}
		
		try
		{
			AtomicInteger loaders = new AtomicInteger(0);
			SystemInitialize evt = new SystemInitialize(eventor, loaders);
			eventor.post(evt);
			preloopImpl.loaders = loaders;
			
			if(evt.errors.size() != 0)
				throw evt.errors.get(0);
		}
		catch(Throwable e)
		{
			e.printStackTrace();
			screen.cleanup();
			CrashWindow.showCrashWindow(CrashReport.create(e).addObject(screen).addObject(args));
			return;
		}
		
		System.gc();
		
		try
		{
			// Run
			loop.run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			screen.cleanup();
			CrashWindow.showCrashWindow(CrashReport.create(e).addObject(screen).addObject(args));
			return;
		}
		
		// Cleanup
		eventor.post(new SystemCleanup());
		screen.cleanup();
		
		System.out.println("[Info] System.exit(0)");
		
		// Shutdown
		System.exit(0);
	}

	/**
	 * Make screen.
	 *
	 * @param type the type
	 * @return the IGL screen
	 */
	private static IGLScreen makeScreen(String type)
	{
		if("awt".equalsIgnoreCase(type))
			return new GLAWTScreen();
		
		if("swing".equalsIgnoreCase(type))
			return new GLSwingScreen();
		
		if("native".equalsIgnoreCase(type))
			return new GLNativeScreen();
		
		throw new Error("Unsupported screen type: " + type);
	}
	
}
