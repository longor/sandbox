/*
 * 
 */
package de.longor.eventor;

import java.util.List;

import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventServiceExistsException;
import org.bushe.swing.event.EventServiceLocator;
import org.bushe.swing.event.ThreadSafeEventService;
import org.bushe.swing.event.annotation.AnnotationProcessor;

// TODO: Auto-generated Javadoc
/**
 * The Class Eventor.
 */
public class Eventor
{
	
	/** The sidedhandler. */
	SidedHandler sidedhandler;
	
	/**
	 * Instantiates a new eventor.
	 */
	public Eventor()
	{
		this.sidedhandler = new SidedHandler();
		
		try
		{
			EventServiceLocator.setEventService(EventServiceLocator.SERVICE_NAME_SWING_EVENT_SERVICE, this.sidedhandler);
		}
		catch (EventServiceExistsException e)
		{
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Registers each and every method in the given Object that has a '@EventSubscriber'-annotation to the correct EventService.
	 *
	 * @param subscriber the subscriber
	 */
	public void register(Object subscriber)
	{
		AnnotationProcessor.process(subscriber);
	}
	
	/**
	 * Publishes an Event.
	 *
	 * @param event the event
	 * @return the event
	 */
	public Event post(Event event)
	{
		EventBus.publish(event);
		return event;
	}
	
	/**
	 * The Class SidedHandler.
	 */
	private static class SidedHandler extends ThreadSafeEventService
	{
		
		/* (non-Javadoc)
		 * @see org.bushe.swing.event.ThreadSafeEventService#publish(java.lang.Object, java.lang.String, java.lang.Object, java.util.List, java.util.List, java.lang.StackTraceElement[])
		 */
		@SuppressWarnings("rawtypes")
		@Override
		protected void publish(
				final Object event, final String topic, final Object eventObj,
				final List subscribers, final List vetoSubscribers,
				final StackTraceElement[] callingStack)
		{
			super.publish(event, topic, eventObj, subscribers, vetoSubscribers, callingStack);
		}
		
	}
}
