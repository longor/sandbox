/*
 * 
 */
package de.longor.eventor;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemInitialize.
 */
public class SystemInitialize extends Event
{
	
	/** The eventor. */
	public Eventor eventor;
	
	/** The loaders. */
	public AtomicInteger loaders;
	
	/** The errors. */
	public ArrayList<Throwable> errors;
	
	/**
	 * Instantiates a new system initialize.
	 *
	 * @param eventor the eventor
	 * @param loaders the loaders
	 */
	public SystemInitialize(Eventor eventor, AtomicInteger loaders)
	{
		this.eventor = eventor;
		this.loaders = loaders;
		errors = new ArrayList<Throwable>();
	}

}
