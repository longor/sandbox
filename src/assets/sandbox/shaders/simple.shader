#SHADER_VERTEX
varying vec4 position;
varying vec3 normal;
varying vec3 lightDir;

void main()
{
	normal = normalize(gl_NormalMatrix * gl_Normal);
	lightDir =  normalize(vec3(gl_LightSource[0].position));;
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
	position = gl_Position;
	
	gl_FrontColor = gl_Color;
	gl_BackColor = vec4(0.0,0.0,0.0,1.0);
}






#SHADER_FRAGMENT
uniform sampler2D Texture0;
varying vec4 position;
varying vec3 normal;
varying vec3 lightDir;



float fogFactorLinear(const float dist, const float start, const float end)
{
	return clamp((end - dist) / (end - start), 0.0, 1.0);
}




float fogFactorExp2(const float dist, const float density)
{
	const float LOG2 = -1.442695;
	float d = density * dist;
	return clamp(exp2(d * d * LOG2), 0.0, 1.0);
}




void main(void)
{
  // Diffuse Texture
  vec4 texture_diffuse = texture2D(Texture0, gl_TexCoord[0].xy);
  vec4 diffuse_multiply = gl_FrontFacing ? gl_Color : vec4(0.0,0.0,0.0,1.0);
  vec4 diffuse_term = vec4(1.0,1.0,1.0,1.0) + gl_LightSource[0].diffuse;
  vec4 globalAmbient = gl_LightSource[0].ambient;
  float NdotL = max(dot(normal, lightDir), 0.0);
  
  // Final Diffuse Color
  vec4 diffuse_color = texture_diffuse * diffuse_multiply;
  vec4 final_diffuse_color = diffuse_color * (NdotL * diffuse_term + globalAmbient);
  
  // Fog
  const float LOG2 = 1.442695;
  float z = (gl_FragCoord.z / gl_FragCoord.w);
  float fogFactor = fogFactorExp2(z, gl_Fog.density);
  
  gl_FragColor = mix(gl_Fog.color, final_diffuse_color, fogFactor);
}
