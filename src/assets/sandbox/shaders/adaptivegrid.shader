#SHADER_VERTEX
#version 130
varying vec4 position;

void main()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
	gl_FrontColor = gl_Color;
	position = gl_Vertex;
}

#SHADER_FRAGMENT
#version 130
varying vec4 position;

void main()
{	
	
	// get the length of the texture derivatives
	// float lambda_s = length(dFdx(gl_TexCoord[0].x));
	// float lambda_t = length(dFdy(gl_TexCoord[0].y));
	float lambda_s = length(dFdx((position.x / 100.0) + gl_TexCoord[0].x));
	float lambda_t = length(dFdy((position.y / 100.0) + gl_TexCoord[0].y));
	
	// compute the sample level, based on the maximum derivative, and the interpolation factor between ceiled and floored level
	float level_s;    
	float interp_s = modf(-log2(max(lambda_s, lambda_t)), level_s);
	
	// get the new texture coordinate by scaling the texture coordinate by the floored and ceiled level
	vec2 tx_down = gl_TexCoord[0].xy * pow(2, level_s);         
	vec2 tx_up = gl_TexCoord[0].xy * pow(2, level_s + 1.0);
	
	// compute the color procedural
	vec2 highlight_down = clamp(cos(tx_down * 0.2) - 0.95, 0.0,1.0) * 10.0;
	vec2 highlight_up = clamp(cos(tx_up * 0.2) - 0.95, 0.0,1.0) * 10.0;
	
	// this might be faster, but it doesn't fade nicely...
	//float highlight_down = step(0.9, fract(tx_down * 0.04)) * 0.8;
	//float highlight_up = step(0.9, fract(tx_up * 0.04)) * 0.8;
	
	float intensity = clamp(mix(
		max(highlight_down.x, highlight_down.y), 
		max(highlight_up.x, highlight_up.y), 
		interp_s), 0.0,1.0);
	
	// lerp between the intensities
	gl_FragColor = vec4(1.0,1.0,1.0,intensity);
}