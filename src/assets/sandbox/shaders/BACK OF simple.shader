#SHADER_VERTEX
varying vec4 position;

void main()
{
	vec3 normal, lightDir, viewDir;
	vec4 diffuse;
	float NdotL;
	
	/* first transform the normal into eye space and normalize the result */
	normal = normalize(gl_NormalMatrix * gl_Normal);
	
	lightDir = normalize(vec3(gl_LightSource[0].position));
	
	NdotL = max(dot(normal, lightDir), 0.0);
	
	/* Compute the diffuse term */
	diffuse = vec4(1.0,1.0,1.0,1.0) + gl_LightSource[0].diffuse;
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
	position = gl_Position;
	
	
	// gl_FrontColor = vec4(gl_Vertex.xyz,1.0);
	
	// gl_FrontColor = vec4(normalize(gl_Normal) * 0.5 + 0.5, 1.0);
	
	vec4 globalAmbient = gl_LightSource[0].ambient;
	gl_FrontColor = (NdotL * diffuse + globalAmbient) * gl_Color;
	gl_BackColor = vec4(0.0,0.0,0.0,1.0);
}






#SHADER_FRAGMENT
uniform sampler2D Texture0;
varying vec4 position;



float fogFactorLinear(const float dist, const float start, const float end)
{
	return clamp((end - dist) / (end - start), 0.0, 1.0);
}




float fogFactorExp2(const float dist, const float density)
{
	const float LOG2 = -1.442695;
	float d = density * dist;
	return clamp(exp2(d * d * LOG2), 0.0, 1.0);
}




void main(void)
{
  // Diffuse Texture
  vec4 texture_diffuse = texture2D(Texture0, gl_TexCoord[0].xy);
  vec4 diffuse_multiply = gl_FrontFacing ? gl_Color : vec4(0.0,0.0,0.0,1.0);
  
  // Final Diffuse Color
  vec4 final_diffuse_color = texture_diffuse * diffuse_multiply;
  
  // Fog
  const float LOG2 = 1.442695;
  float z = (gl_FragCoord.z / gl_FragCoord.w);
  float fogFactor = fogFactorExp2(z, gl_Fog.density);
  
  gl_FragColor = mix(gl_Fog.color, final_diffuse_color, fogFactor);
}
